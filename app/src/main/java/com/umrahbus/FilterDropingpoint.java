package com.umrahbus;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.umrahbus.BeanClasses.DropingPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by jagadeesh on 8/5/2016.
 */
public class FilterDropingpoint extends AppCompatActivity {

    private ListView listView;
    Button Done;
    String DropingTimes;
    DropingPoint Dropingpoint;
    private DropingAdapter myAppAdapter;
    JSONArray results;
    ArrayList<String> List =new ArrayList<>();
    ArrayList<String> FilterOperaters =new ArrayList<>();

    String Dropinglocation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_operaters_list);

        Done=(Button)findViewById(R.id.OK);
        Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String listString = TextUtils.join(",",FilterOperaters);
                System.out.println(listString);

                Intent i=new Intent(FilterDropingpoint.this, FilterActivity.class);
                SharedPreferences preference = getApplication().getSharedPreferences("Filter", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preference.edit();

                editor.putString("FilterDropingId", listString);
                editor.putString("FilterDropinglocation", Dropinglocation);
                editor.commit();

                startActivity(i);
            }
        });

        listView = (ListView) findViewById(R.id.Filter_listView);
        SharedPreferences preference = getApplicationContext().getSharedPreferences("Filter", MODE_PRIVATE);
        DropingTimes=preference.getString("JsonResponce", null);

        System.out.println("BoardingTimes"+DropingTimes);

        ArrayList<DropingPoint> dropingPointArrayList = new ArrayList<>();
        JSONObject jsonObject = null;
        if(DropingTimes!=null){
            try {
                jsonObject = new JSONObject(DropingTimes);
                results = jsonObject.getJSONArray("AvailableTrips");

                for (int i = 0; i < results.length(); i++) {
                    Dropingpoint = new DropingPoint();

                    JSONObject jobj = results.getJSONObject(i);

                    //   JSONArray jsonArray = new JSONArray(BoardingTimes);
                    JSONArray jsonArray = jobj.getJSONArray("DroppingTimes");
                    System.out.println("Array****" + jsonArray);


                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject disptime = jsonArray.getJSONObject(j);
                        Dropingpoint = new DropingPoint();
                        Dropingpoint.setDroppingAddress(disptime.getString("Address"));
                        Dropingpoint.setDroppingContactNumbers(disptime.getString("ContactNumbers"));
                        Dropingpoint.setDroppingContactPersons(disptime.getString("ContactPersons"));
                        Dropingpoint.setDroppingPointId(disptime.getString("PointId"));
                        String Point=disptime.getString("PointId");
                        Dropingpoint.setDroppingLandmark(disptime.getString("Landmark"));
                        Dropingpoint.setDroppingLocation(disptime.getString("Location"));
                        Dropingpoint.setDroppingName(disptime.getString("Name"));

                        if(!(List.contains(Point))){
                            List.add(Point);
                            dropingPointArrayList.add(Dropingpoint);
                        }


                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            myAppAdapter = new DropingAdapter(dropingPointArrayList, FilterDropingpoint.this);
            listView.setAdapter(myAppAdapter);

        }else{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set title
            //  alertDialogBuilder.setTitle("Your Title");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Sorry! No Dropping points available")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity

                            dialog.cancel();
                            Intent i=new Intent(FilterDropingpoint.this, FilterActivity.class);
                            startActivity(i);
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }


    }

    public class DropingAdapter extends BaseAdapter {

        public class ViewHolder {
            TextView txtTitle,txtSubTitle;
            CheckBox check;

        }

        public List<DropingPoint> dropingList;

        public Context context;
        ArrayList<DropingPoint> arraylist;

        private DropingAdapter(List<DropingPoint> apps, Context context) {
            this.dropingList = apps;
            this.context = context;
            arraylist = new ArrayList<>();
            arraylist.addAll(dropingList);

        }

        @Override
        public int getCount() {
            return dropingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;
            ViewHolder viewHolder;

            if (rowView == null) {
                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.filter_operaters, null);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.txtTitle = (TextView) rowView.findViewById(R.id.Filter_Operater);
                viewHolder.check = (CheckBox) rowView.findViewById(R.id.Filter_CheckBox);
                rowView.setTag(viewHolder);
                viewHolder.check
                        .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton vw,
                                                         boolean isChecked) {
                                //    int getPosition = (Integer) vw.getTag();
                                arraylist.get(position).setSelected(
                                        vw.isChecked());
                        /*        if(FilterOperaters.size()!=0){
                                    if (FilterOperaters.contains(boadinglist.get(position).getId())) {
                                        System.out.println("check remove"+FilterOperaters);
                                        FilterOperaters.remove(boadinglist.get(position).getId());

                                        System.out.println("check remove"+FilterOperaters);
                                    } else {

                                        FilterOperaters.add(boadinglist.get(position).getId());

                                        System.out.println("check add"+FilterOperaters+boadinglist.get(position).getId());

                                    }
                                }else{
                                    FilterOperaters.addAll(Arrays.<String>asList(boadinglist.get(position).getId()));
                                    System.out.println("checked add"+FilterOperaters+boadinglist.get(position).getId());

                                }*/
                            }
                        });
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.txtTitle.setText(dropingList.get(position).getDroppingLocation() + "");


            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Dropinglocation = dropingList.get(position).getDroppingLocation();
                    CheckBox chk = (CheckBox) v
                            .findViewById(R.id.Filter_CheckBox);
                    if(FilterOperaters.size()!=0){
                        if (FilterOperaters.contains(dropingList.get(position).getDroppingPointId())) {
                            System.out.println("row remove"+FilterOperaters);
                            FilterOperaters.remove(dropingList.get(position).getDroppingPointId());
                            chk.setSelected(false);
                            chk.setChecked(false);
                            System.out.println("row remove"+FilterOperaters);
                        } else {

                            FilterOperaters.add(dropingList.get(position).getDroppingPointId());
                            chk.setSelected(true);
                            chk.setChecked(true);
                            System.out.println("row add"+FilterOperaters+dropingList.get(position).getDroppingPointId());

                        }
                    }else{
                        FilterOperaters.addAll(Arrays.<String>asList(dropingList.get(position).getDroppingPointId()));
                        System.out.println("row add"+FilterOperaters+dropingList.get(position).getDroppingPointId());
                        chk.setSelected(true);
                        chk.setChecked(true);
                    }



                }
            });
            return rowView;


        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            dropingList.clear();
            if (charText.length() == 0) {
                dropingList.addAll(arraylist);



            } else {
                for (DropingPoint postDetail : arraylist) {
                    if (charText.length() != 0 && postDetail.getDroppingLocation().toLowerCase(Locale.getDefault()).contains(charText)) {
                        dropingList.add(postDetail);


                    }

                   /* else if (charText.length() != 0 && postDetail.getPostSubTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                        parkingList.add(postDetail);
                    }*/
                }
            }
            notifyDataSetChanged();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(true);
        searchView.setQueryHint("Dropping Point");
        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.curser); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }
        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                myAppAdapter.filter(searchQuery.trim());
                listView.invalidate();
                return true;

            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
