package com.umrahbus;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.umrahbus.ConnectionStatus.ConnectivityReceiver;
import com.umrahbus.ConnectionStatus.MyApplication;
import com.umrahbus.ServicesClasses.ServiceHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by jagadeesh on 7/6/2016.
 */
public class SelectSeatActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    TextView Seats,Net_fares;
    Button Done,Aminites,BusImages;
    RelativeLayout RelativeLayout;
    LinearLayout LinearLayout,LinearAminites_Layout;
    ScrollView scrollView;
    Dialog dialog;
    String cancellationPolicy;
    String polcy;
    String Arrival_id,Destination_id,Journey_Date,ObiboAPIProvidrer,Provider,Operator,BusTypeID;
    String[] scripts;
    String cancalpolicy="",PartialCancellation;
    String TotalAmount;
    Button lower,upper;
    Button  myButton;
    JSONArray results;
    ProgressDialog mProgressDialog;
    int Screenheight, Screenwidth;

    Boolean selectseat=false;
    ArrayList<String> arList = new ArrayList<String>();
    ArrayList<String> netfare = new ArrayList<String>();
    ArrayList<String> ServiceTaxList = new ArrayList<String>();
    ArrayList<String> ServiceChargeList = new ArrayList<String>();
    ArrayList<String> PriceArray = new ArrayList<String>();
    String NetTotalAmount;

    String ZindexString;
    String ServiceTaxes,OperatorserviceCharge,Fares,NoOperatorserviceCharge;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String userLogin,agentLogin;
    String UserId,User_agentid;

    int x,y;
    ////*****TAb View****????
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    final Context context = this;
    String dialogboxseats="";
    double dialogboxamount;
    double dialogboxservicetax,dialogboxservicecharge;
    String dialogboxnetfare;

    String ReturnNumberOfSeats,IsReturnYes,RETURNDate;
    String HotelName;
    double Dialogfare;

    String Hotel;
    int SeatWidth;
    int SeatHeaight;
    String Point="true";
    String ISCHECKED="false";
    String isShown="false";
    String partailCancellation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seatlayout);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        Screenheight = displaymetrics.heightPixels;
        Screenwidth = displaymetrics.widthPixels;
        System.out.println("Device width :"+ Screenwidth +" Device Height :"+Screenheight);

        String f=TotalAmount;

        SharedPreferences prefer = getApplicationContext().getSharedPreferences("Filter", MODE_PRIVATE);
        prefer.edit().remove("ACSELECTED").apply();
        prefer.edit().remove("NonACSELECTED").apply();
        prefer.edit().remove("SleeperSELECTED").apply();
        prefer.edit().remove("NonSleeperSELECTED").apply();


        pref = getSharedPreferences("LoginPreference", MODE_PRIVATE);
        editor = pref.edit();
        userLogin= pref.getString("UserLOGIN", null);
        agentLogin=pref.getString("AgentLOGIN",null);
        if( userLogin!=null && userLogin.equals("Yes")){
            UserId="6";
            User_agentid=pref.getString("User_agentid", null);
        }else if(agentLogin!=null && agentLogin.equals("Yes")){
            UserId="4";
            User_agentid=pref.getString("User_agentid", null);
        }else{
            UserId="5";
            User_agentid=pref.getString("SuperAdmin", null);
        }

        LinearAminites_Layout=(LinearLayout)findViewById(R.id.Aminites_Layout);
        Aminites = (Button) findViewById(R.id.Aminites);
        Aminites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SelectSeatActivity.this, AminitesActivity.class);
                i.putExtra("id",0);
                startActivity(i);
            }
        });
        BusImages = (Button) findViewById(R.id.Bus_Images);
        BusImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SelectSeatActivity.this, AminitesActivity.class);
                i.putExtra("id",1);
                startActivity(i);
            }
        });
        scrollView=(ScrollView)findViewById(R.id.SeatScrollView);
        Seats=(TextView)findViewById(R.id.No_of_seats);
        Net_fares=(TextView)findViewById(R.id.Net_Fare);
        RelativeLayout=(RelativeLayout)findViewById(R.id.Done_layout);
        RelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // custom dialog
                dialog = new Dialog(context);
                dialog.setContentView(R.layout.custemalertdialogbox);
                dialog.setTitle("Fare Details ");
                dialog.setCancelable(false);

                // set the custom dialog components - text, image and button
                TextView noofseats = (TextView) dialog.findViewById(R.id.Noofseats);
                TextView Amount = (TextView) dialog.findViewById(R.id.amount);
                TextView serviceTax = (TextView) dialog.findViewById(R.id.servicetax);
                final TextView serviceCharge = (TextView) dialog.findViewById(R.id.servicecharge);
                final TextView total = (TextView) dialog.findViewById(R.id.total);
                final CheckBox mcheck;
                mcheck= (CheckBox) dialog.findViewById(R.id.hotelswitch);




                noofseats.setText(""+dialogboxseats+"");



                double amount = Double.parseDouble(String.valueOf(dialogboxamount));
                amount =Double.parseDouble(new DecimalFormat("##.##").format(amount));
                //    int roundValueamount = (int) Math.round(amount);
                Amount.setText(""+amount+"");


                double Servicecharge = Double.parseDouble(String.valueOf(dialogboxservicetax));

                Servicecharge =Double.parseDouble(new DecimalFormat("##.##").format(Servicecharge));
                //   int ServCharge = (int) Math.round(Servicecharge);
                serviceTax.setText(""+Servicecharge+"");



                final double[] Operatercharge = new double[1];
                final double finalAmount = amount;
                final double finalServicecharge = Servicecharge;
                final double[] NetFare = new double[1];


                mcheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                         Point="false";
                        if (mcheck.isChecked()){
                            Operatercharge[0] = Double.parseDouble(String.valueOf(dialogboxservicecharge));
                            Operatercharge[0] =Double.parseDouble(new DecimalFormat("##.##").format(Operatercharge[0]));
                            //     int Opcharge = (int) Math.round(Operatercharge);
                            serviceCharge.setText(""+ Operatercharge[0] +"");
                            NetFare[0]=(finalAmount + Operatercharge[0] + finalServicecharge);
                            total.setText(""+NetFare[0] +" SAR");
                            Dialogfare=NetFare[0];
                        }else {
                            Operatercharge[0]=0;
                            serviceCharge.setText(""+ Operatercharge[0] +"");
                            NetFare[0] =(finalAmount + Operatercharge[0] + finalServicecharge);
                            total.setText(""+ NetFare[0] +" SAR");
                            Dialogfare=NetFare[0];

                        }

                    }
                });

                if (ISCHECKED.equals("true")){
                    mcheck.setChecked(true);
                    serviceCharge.setText(""+Operatercharge[0]+"");
                    total.setText(""+NetFare[0]+" SAR");

                }else {
                    total.setText(""+dialogboxnetfare+""+" SAR");
                }


                isShown="true";

                Button dialogButton = (Button) dialog.findViewById(R.id.cancel_dialog);
                // if button is clicked, close the custom dialog
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mcheck.isChecked()){

                            mcheck.setChecked(true);
                            ISCHECKED="true";
                            OperatorserviceCharge=TextUtils.join("~",ServiceChargeList);
                            Hotel=HotelName;
                        }
                        else {
                            ISCHECKED="false";
                            if (Point.equals("true")){
                                Dialogfare= Double.parseDouble(dialogboxnetfare);
                            }
                            String Num = TextUtils.join("~", ServiceChargeList);
                            OperatorserviceCharge=Num.replaceAll("[0-9]+","0");
                            Hotel="";

                        }
                        Net_fares.setText(""+Dialogfare+" SAR");


                        dialog.dismiss();

                    }
                });

                dialog.show();
            }
        });

        LinearLayout=(LinearLayout)findViewById(R.id.UppearLayout);
        upper = (Button) findViewById(R.id.Upper);
        upper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upper.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                upper.setTextColor(getResources().getColor(R.color.colorWhite));
                lower.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                lower.setTextColor(getResources().getColor(R.color.colorPrimary));
                try {
                    ZindexString="1";
                    seatCreation();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        lower = (Button) findViewById(R.id.Lower);
        lower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lower.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                lower.setTextColor(getResources().getColor(R.color.colorWhite));
                upper.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                upper.setTextColor(getResources().getColor(R.color.colorPrimary));
                try {
                    ZindexString="0";
                    seatCreation();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        SharedPreferences preference = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
        cancellationPolicy=preference.getString("CancellationPolicy", null);
        BusTypeID=preference.getString("BusTypeID", null);
        Arrival_id=preference.getString("ARRIVAL_ID", null);
        Destination_id=preference.getString("DESTINATION_ID", null);
        Journey_Date=preference.getString("JourneyDate", null);
        PartialCancellation=preference.getString("PartialCancellationAllowed",null );

        ObiboAPIProvidrer=preference.getString("ObiboAPIProvider", null);
        Provider=preference.getString("Provider", null);
        Operator=preference.getString("Operator", null);

        IsReturnYes = preference.getString("IsReturnYes", null);
        RETURNDate = preference.getString("PassengerRETURN_DATE", null);
        ReturnNumberOfSeats = preference.getString("ReturnNumberOfSeats", null);
        HotelName=preference.getString("HotelName",null);

        System.out.println("Date :"+Journey_Date +"JourneyDate" +"JourneyDetails");
        checkConnection();


        Done=(Button)findViewById(R.id.Done);
        Done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  new BlockTicket().execute();
                String listString ;
               /* for (int i=0; i<arList.size(); i++)
                {
                    String s= arList.get(i);
                    if(i==arList.size()-1){

                        listString += s ;
                    }
                    else{
                        listString += s + "~";
                    }

                }*/

                if (IsReturnYes.equals("Yes")){
                    String numberOfSeats=""+arList.size();
                    if (numberOfSeats.equals(ReturnNumberOfSeats)){

                        listString=TextUtils.join("~",arList);
                        ServiceTaxes= TextUtils.join("~",ServiceTaxList);
                        Fares=TextUtils.join("~",PriceArray);

                        System.out.println("STax"+ServiceTaxes+"OperatorserviceCharge"+OperatorserviceCharge+"Fares"+Fares);
                        System.out.println("numberof seats"+NetTotalAmount);

                        Intent i=new Intent(SelectSeatActivity.this, BoardingPointActivity.class);
                        SharedPreferences preference = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preference.edit();
                        editor.putString("NumberOfSeats", numberOfSeats);
                        editor.putString("SelectedSeats", listString);
                        editor.putString("HotelName",Hotel);

                        String P=Net_fares.getText().toString();
                        String Q=P.replace(" SAR","");
                        editor.putString("NetFare", Q);
                        editor.putString("ServiceTaxes", ServiceTaxes);
                        editor.putString("OperatorserviceCharge",OperatorserviceCharge);
                        editor.putString("Fares", Fares);
                        editor.apply();
                        startActivity(i);
                    }else {
                      Toast.makeText(getApplicationContext(),"No of seats should be same for forward and return journeys",Toast.LENGTH_LONG).show();
                    }
                }else {
                    listString=TextUtils.join("~",arList);
                    ServiceTaxes= TextUtils.join("~",ServiceTaxList);
                    String numberOfSeats=""+arList.size();
                    Fares=TextUtils.join("~",PriceArray);

                    System.out.println("STax"+ServiceTaxes+"OperatorserviceCharge"+OperatorserviceCharge+"Fares"+Fares);
                    System.out.println("numberof seats"+NetTotalAmount);

                    Intent i=new Intent(SelectSeatActivity.this, BoardingPointActivity.class);
                    SharedPreferences preference = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
                    SharedPreferences.Editor editor = preference.edit();
                    editor.putString("NumberOfSeats", numberOfSeats);
                    editor.putString("SelectedSeats", listString);
                    editor.putString("HotelName",Hotel);

                    String P=Net_fares.getText().toString();
                    String Q=P.replace(" SAR","");
                    editor.putString("NetFare", Q);
                    editor.putString("ServiceTaxes", ServiceTaxes);
                    editor.putString("OperatorserviceCharge",OperatorserviceCharge);
                    editor.putString("Fares", Fares);
                    editor.apply();
                    startActivity(i);
                }





            }
        });

        String cancelArray[] = cancellationPolicy.split(";");
        System.out.println("split"+cancelArray[0]);

        if(!cancelArray[0].equals("")) {
            for (int i = 0; i < cancelArray.length; i++) {
                String fromCancelArray[] = cancelArray[i].split(":");
                if (fromCancelArray.length > 1) {
//
                    String cancelTimeStr;
                    if (fromCancelArray[1].equals("-1")) {
                        cancelTimeStr = (fromCancelArray[0] + "   hours before journey time          ");
                    } else {
                        cancelTimeStr = (fromCancelArray[1] + "   hours before journey time          " + fromCancelArray[0]);
                    }
                    String percentStr;
                    if(fromCancelArray[3].equals("1")){
                        percentStr = (fromCancelArray[2] + "  SAR" );
                    }else{
                        percentStr = (fromCancelArray[2] + " .0 %" );
                    }



                    polcy = cancelTimeStr + percentStr;


                    //   cancelDaytextArray addObject:cancelTimeStr;
                    //  percentageArray addObject:percentStr;
                    System.out.println("data" + cancelTimeStr + percentStr);
                }

                cancalpolicy =""+ cancalpolicy + "" + polcy + "\n";


            }
            if (PartialCancellation.equals("true")){
                partailCancellation="Partial Cancellation is Allowed";
            }else {
                partailCancellation="Partial Cancellation is not Allowed";
            }
        }else{
            cancalpolicy="No cancellation Policy Found";
        }
        System.out.println("data*****"+scripts);
    }


    //////////Service Calling///////
    public class Availablebuses extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {


        //    mProgressDialog = ProgressDialog.show(SelectSeatActivity.this, "","Loading...");
        //    mProgressDialog.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... params) {
            String url;


            if(IsReturnYes.equals("Yes")){
                url = Constants.BUSAPI + Constants.TripDetails+"?tripId="+BusTypeID+"&sourceId="+Arrival_id+"&destinationId="+Destination_id+"&journeyDate="+Journey_Date+"&tripType=2&provider="+Provider+"&travelOperator="+Operator+"&userType="+UserId+"&user="+User_agentid+"&returnDate="+RETURNDate+"&obiboProvider="+ObiboAPIProvidrer;
                SharedPreferences preference = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
                SharedPreferences.Editor editor = preference.edit();
                editor.putString("PassengerIsReturnselected","ReturnSelected");
                editor.apply();
            }else{
                url = Constants.BUSAPI + Constants.TripDetails+"?tripId="+BusTypeID+"&sourceId="+Arrival_id+"&destinationId="+Destination_id+"&journeyDate="+Journey_Date+"&tripType=1&provider="+Provider+"&travelOperator="+Operator+"&userType="+UserId+"&user="+User_agentid+"&returnDate="+RETURNDate+"&obiboProvider="+ObiboAPIProvidrer;
            }

          //  }

            System.out.println("url" + url);
            ServiceHandler sh = new ServiceHandler();
            String jsonstr = sh.makeServiceCall(url, ServiceHandler.GET);
            if (jsonstr != null) {

                //    postArrayList.clear();
                try {


                    JSONObject jsonObject = new JSONObject(jsonstr);

                    String tripid = jsonObject.getString("TripId");
                    results = jsonObject.getJSONArray("Seats");

                    System.out.println("Seats   "+results);


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                ZindexString="0";
            //    mProgressDialog.dismiss();
                if(results.length()!=0){
                    seatCreation();
                }else{
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SelectSeatActivity.
                            this);

                    // set title
                    alertDialogBuilder.setTitle("");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("No Seats Available!")
                            .setCancelable(false)
                            .setPositiveButton("Search Again",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                    Intent i=new Intent(SelectSeatActivity.this, SearchActivity.class);
                                    startActivity(i);

                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



    public void seatCreation() throws JSONException {
        boolean isavailableseat=true;
        List<String> jsonValues = new ArrayList<String>();
        for (int i = 0; i < results.length(); i++) {
            JSONObject seatObject=results.getJSONObject(i);
            jsonValues.add(seatObject.getString("Row"));

        }
        Collections.sort(jsonValues);

        JSONArray sortedJsonArray = new JSONArray(jsonValues);
        System.out.println("RowArray"+sortedJsonArray);
        List<String> jsonValueswidth = new ArrayList<String>();
        for (int i = 0; i < results.length(); i++) {
            JSONObject seatObject=results.getJSONObject(i);
            jsonValues.add(seatObject.getString("Column"));

        }
        Collections.sort(jsonValues);

        JSONArray sortedJsonArraywidth = new JSONArray(jsonValues);
        System.out.println("RowArray"+sortedJsonArray);

        for(int i=0; i<results.length(); i++) {
            JSONObject seatObject = results.getJSONObject(i);
            String Zindex = seatObject.getString("Zindex");
            if (seatObject.getString("Zindex") .equals(ZindexString) ) {
                int xPosition, yPosition;
                yPosition = Integer.parseInt(seatObject.getString("Column"));
                xPosition = Integer.parseInt(seatObject.getString("Row"));
                final String number = seatObject.getString("Number");
                final String NetFare = seatObject.getString("Fare");
                final String fare = seatObject.getString("NetFare");
                final String IsAvailableSeat = seatObject.getString("IsAvailableSeat");
                final String IsLadiesSeat = seatObject.getString("IsLadiesSeat");
                final int Length = Integer.parseInt(seatObject.getString("Length"));
                int Width = Integer.parseInt(seatObject.getString("Width"));
                final String Servicetax = seatObject.getString("Servicetax");
                final String IsFamily=seatObject.getString("IsFamilyQuota");
                final String OperatorServiceCharge = seatObject.getString("OperatorServiceCharge");

                System.out.println("Device width :"+ Screenwidth +" Device Height :"+Screenheight);

                if((String.valueOf(Screenwidth).equals("320") )){
                    System.out.println("Device width 320:"+ Screenwidth +" Device Height :"+Screenheight);
                    x = Screenwidth - xPosition * 53;
                    //  x = x - 230;
                    y = yPosition * 100 - 70;
                    SeatWidth= 40 * Width;
                    SeatHeaight=60 * Length;
                }
                else if((String.valueOf(Screenwidth).equals("360") )){
                    System.out.println("Device width 360 :"+ Screenwidth +" Device Height :"+Screenheight);
                    x = Screenwidth - xPosition * 70;
                    //  x = x - 230;
                    y = yPosition * 100 - 60;
                    SeatWidth= 70 * Width;
                    SeatHeaight=70 * Length;
                }else if((String.valueOf(Screenwidth).equals("480") )){
                    System.out.println("Device width 480 :"+ Screenwidth +" Device Height :"+Screenheight);
                    x = Screenwidth - xPosition * 80;
                    //  x = x - 230;
                    y = yPosition * 100 - 60;
                    SeatWidth= 70 * Width;
                    SeatHeaight=70 * Length;
                }else if((String.valueOf(Screenwidth).equals("540") )){
                    System.out.println("Device width 540 :"+ Screenwidth +" Device Height :"+Screenheight);
                    x = Screenwidth - xPosition * 90;
                    //  x = x - 230;
                    y = yPosition * 100 - 60;
                    SeatWidth= 70 * Width;
                    SeatHeaight=70 * Length;
                }else if((String.valueOf(Screenwidth).equals("720") )){
                    System.out.println("Device width 720 :"+ Screenwidth +" Device Height :"+Screenheight);
                    x = Screenwidth - xPosition * 120;
                    //  x = x - 230;
                    y = yPosition * 100 - 60;
                    SeatWidth= 70 * Width;
                    SeatHeaight=80 * Length;
                }else if((String.valueOf(Screenwidth).equals("768") )){
                    System.out.println("Device width 768 :"+ Screenwidth +" Device Height :"+Screenheight);
                    x = Screenwidth - xPosition * 120;
                    //  x = x - 230;
                    y = yPosition * 100 - 60;
                    SeatWidth= 70 * Width;
                    SeatHeaight=70 * Length;
                }else if((String.valueOf(Screenwidth).equals("800") )){
                    System.out.println("Device width 800 :"+ Screenwidth +" Device Height :"+Screenheight);
                    x = Screenwidth - xPosition * 155;
                    //  x = x - 230;
                    y = yPosition * 100 - 60;
                    SeatWidth= 80 * Width;
                    SeatHeaight=80 * Length;
                }else if((String.valueOf(Screenwidth).equals("1080") )){
                    System.out.println("Device width 1080 :"+ Screenwidth +" Device Height :"+Screenheight);
                    x = Screenwidth - xPosition * 215;
                    // x = x - 230;
                    y = yPosition * 160 - 10;

                    SeatWidth= 120 * Width;
                    SeatHeaight=140 * Length;
                }else if((String.valueOf(Screenwidth).equals("1440") )){
                    System.out.println("Device width 1440 :"+ Screenwidth +" Device Height :"+Screenheight);
                    x = Screenwidth - xPosition * 240;
                    //  x = x - 230;
                    y = yPosition * 160 - 10;

                    SeatWidth= 120 * Width;
                    SeatHeaight=140 * Length;
                }else{

                    x = Screenwidth - xPosition * 90;
                    //  x = x - 230;
                    y = yPosition * 100 - 60;
                    SeatWidth= 70 * Width;
                    SeatHeaight=70 * Length;
                }


                RelativeLayout ll = (RelativeLayout) findViewById(R.id.setlayout);
                myButton = new Button(this);
                myButton.setTag(number);
                System.out.println("*****" + number);

                if (Length == 2) {
                    LinearLayout.setVisibility(View.VISIBLE);

                    if (IsAvailableSeat.equals("False")) {

                        if (IsLadiesSeat.equals("True")) {
                            myButton.setBackgroundResource(R.drawable.notavailableleadiessleeperseat);
                            myButton.setClickable(false);
                        } else {
                            myButton.setBackgroundResource(R.drawable.notavailblesleeperseat);
                            myButton.setClickable(false);
                        }

                        } else {
                        if (IsLadiesSeat.equals("True")){
                            myButton.setBackgroundResource(R.drawable.availbleleadiessleeperseat);
                        }
                        else{
                            myButton.setBackgroundResource(R.drawable.availblesleeperseat);
                        }


                    }
                    if (arList.contains(String.valueOf(number))) {
                        myButton.setBackgroundResource(R.drawable.selectedsleeper);

                    }

                } else if (IsAvailableSeat.equals("False")) {

                    if (IsLadiesSeat.equals("True")) {
                        myButton.setBackgroundResource(R.drawable.notavailableladiesseat);
                        myButton.setClickable(false);
                    } else
                        myButton.setBackgroundResource(R.drawable.notavailble_seat_icon);
                        myButton.setClickable(false);
                     if (IsFamily.equals("TRUE")) {
                         myButton.setBackgroundResource(R.drawable.notavailble_seat_icon);
                         myButton.setClickable(false);
                     }else
                         myButton.setBackgroundResource(R.drawable.notavailble_seat_icon);
                         myButton.setClickable(false);

                    } else{
                    if (IsLadiesSeat.equals("True"))
                        myButton.setBackgroundResource(R.drawable.ladiesseat);
                    else
                        myButton.setBackgroundResource(R.drawable.availble_seat_icon);

                    if (IsFamily.equals("TRUE"))
                        myButton.setBackgroundResource(R.drawable.familyseats);
                    else
                        myButton.setBackgroundResource(R.drawable.availble_seat_icon);

                }
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(50, 50);
                lp.addRule(RelativeLayout.ALIGN_RIGHT, 0);

                lp.width = SeatWidth;
                lp.height = SeatHeaight;
                lp.leftMargin = x;
                lp.topMargin = y;


                myButton.setLayoutParams(lp);
                ll.addView(myButton, lp);
                myButton.setText(""+number);
                System.out.println("seat" + "" + x + "" + y + "number" + number);

                myButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Double d = Double.parseDouble(NetFare);
                        //    int roundValue = (int) Math.round(d);
                        double roundValue =Double.parseDouble(NetFare);
                        System.out.println("Math.round(" + d + ")=" + Math.round(d) + roundValue);


                        String seatno = String.valueOf(number);
                        String price = String.valueOf(roundValue);
                        String serviceTax = String.valueOf(Servicetax);
                        String serviceCharge = String.valueOf(OperatorServiceCharge);

                        float FareString = 0;
                        String Fare = "";
                        String STax = "";
                        System.out.println("seats***" + seatno);

                        if (IsAvailableSeat.equals("False")) {
                            myButton.setClickable(false);
                        } else if (arList.contains(seatno)) {

                            arList.remove(seatno);
                            netfare.remove(price);
                            PriceArray.remove(price);
                            ServiceTaxList.remove(serviceTax);
                            ServiceChargeList.remove(serviceCharge);
                            //Net_fares.setText("" + FareString +" SAR");

                            Seats.setText("" + Fare+"");
                            if (Length == 2)
                                v.setBackgroundResource(R.drawable.availblesleeperseat);
                            else
                                v.setBackgroundResource(R.drawable.availble_seat_icon);
                            if (IsFamily.equals("TRUE")){
                                v.setBackgroundResource(R.drawable.familyseats);
                            }

                            if (arList.size() == 0) {
                                RelativeLayout.setVisibility(View.GONE);
                                LinearAminites_Layout.setVisibility(View.VISIBLE);
                                isShown="false";
                                ISCHECKED="false";
                            }
                            for (int i = 0; i < arList.size(); i++) {
                                String s = arList.get(i);
                                if (i == arList.size() - 1) {

                                    Fare += s;
                                } else {
                                    Fare += s + ",";
                                }

                            }
                            Seats.setText("" + Fare+"");


                            for (int i = 0; i < netfare.size(); i++) {
                                float s= Float.parseFloat(netfare.get(i));

                                FareString += s;

                            }
                            //    Net_fares.setText("" + FareString+" CFA");


                            ////******dialog box details *****//////
                            //  ServiceTaxList.addAll(Arrays.<String>asList(String.valueOf(Servicetax)));
                            System.out.println("service Tax :***" + ServiceTaxList);
                            float servicetax=0;
                            for (int i = 0; i < ServiceTaxList.size(); i++) {
                                float s = Float.parseFloat((ServiceTaxList.get(i)));

                                servicetax += s;

                            }
                            //   ServiceChargeList.addAll(Arrays.<String>asList(String.valueOf(OperatorServiceCharge)));
                            System.out.println("OperatorServiceCharge***" + ServiceChargeList);


                            float ServiceCharge=0;
                            for (int i = 0; i < ServiceChargeList.size(); i++) {
                                float s = Float.parseFloat(((ServiceChargeList.get(i))));
                                ServiceCharge += s;

                            }


                            dialogboxseats=Fare;
                            dialogboxamount=FareString;
                            dialogboxservicetax=(servicetax);
                            dialogboxservicecharge= (ServiceCharge);
                            float SeatFare=(FareString+servicetax);
                            float Checked=(FareString+servicetax+ServiceCharge);

                            if (isShown.equals("true")){
                                if (ISCHECKED.equals("true")){
                                    Net_fares.setText(""+Checked+" SAR");
                                    NetTotalAmount = String.valueOf(Checked);
                                    dialogboxnetfare=String.valueOf(NetTotalAmount);
                                    dialogboxservicecharge=ServiceCharge;


                                }else {
                                    if (ISCHECKED.equals("false")){
                                        Net_fares.setText(""+SeatFare+" SAR");
                                        NetTotalAmount = String.valueOf(SeatFare);
                                        dialogboxnetfare=String.valueOf(NetTotalAmount);
                                        String Num=TextUtils.join("~",ServiceChargeList);
                                        OperatorserviceCharge=Num.replaceAll("[0-9]+","0");
                                }


                                }
                            }else if (isShown.equals("false")){
                                Net_fares.setText(""+SeatFare+" SAR");
                                NetTotalAmount = String.valueOf(SeatFare);
                                dialogboxnetfare=String.valueOf(NetTotalAmount);
                                String Num=TextUtils.join("~",ServiceChargeList);
                                OperatorserviceCharge=Num.replaceAll("[0-9]+","0");
                            }

                        } else {
                            if (arList.size() < 6) {

                                if(ReturnNumberOfSeats!=null){
                                    int ReturnSeats= Integer.parseInt(ReturnNumberOfSeats);
                                    if(arList.size()< ReturnSeats){
                                        RelativeLayout.setVisibility(View.VISIBLE);
                                        if (Length == 2)
                                            v.setBackgroundResource(R.drawable.selectedsleeper);
                                        else
                                            v.setBackgroundResource(R.drawable.selected_seat);
                                        String select = String.valueOf(number);

                                        arList.addAll(Arrays.<String>asList(String.valueOf(number)));
                                        System.out.println("seats***" + arList);
                                        for (int i = 0; i < arList.size(); i++) {
                                            String s = arList.get(i);
                                            if (i == arList.size() - 1) {

                                                Fare += s;
                                            } else {
                                                Fare += s + ",";
                                            }

                                        }
                                        Seats.setText("" + Fare+"");
                                        System.out.println(Fare);
                                        netfare.addAll(Arrays.<String>asList(String.valueOf(roundValue)));
                                        System.out.println("NetFare" + netfare);

                                        for (int i = 0; i < netfare.size(); i++) {
                                            float s = Float.parseFloat(netfare.get(i));

                                            FareString += s;

                                        }

                                        //    Net_fares.setText("" + FareString);
                                        System.out.println(FareString);
                                        NetTotalAmount = String.valueOf(FareString);
                                        PriceArray.addAll(Arrays.<String>asList(String.valueOf(roundValue)));
                                        ServiceTaxList.addAll(Arrays.<String>asList(String.valueOf(Servicetax)));
                                        System.out.println("seats***" + ServiceTaxList);
                                        float servicetax=0;
                                        for (int i = 0; i < ServiceTaxList.size(); i++) {
                                            float s = Float.parseFloat((ServiceTaxList.get(i)));

                                            servicetax += s;

                                        }

                                        ServiceChargeList.addAll(Arrays.<String>asList(String.valueOf(OperatorServiceCharge)));
                                        System.out.println("OperatorServiceCharge***" + ServiceChargeList);
                                        float ServiceCharge=0;
                                        for (int i = 0; i < ServiceChargeList.size(); i++) {

                                            float s = Float.parseFloat(((ServiceChargeList.get(i))));

                                            ServiceCharge += s;

                                        }


                                        dialogboxseats = Fare;
                                        dialogboxamount = FareString;
                                        dialogboxservicetax = (servicetax);
                                        dialogboxservicecharge = (ServiceCharge);
                                        float SeatFare = (FareString + servicetax);
                                        float Checked = (FareString + servicetax + ServiceCharge);
                                        if (isShown.equals("true")) {
                                            if (ISCHECKED.equals("true")) {
                                                Net_fares.setText("" + Checked + " SAR");
                                                NetTotalAmount = String.valueOf(Checked);
                                                dialogboxnetfare = String.valueOf(NetTotalAmount);
                                                dialogboxservicecharge = ServiceCharge;

                                            } else {
                                                if (ISCHECKED.equals("false")) {
                                                    Net_fares.setText("" + SeatFare + " SAR");
                                                    NetTotalAmount = String.valueOf(SeatFare);
                                                    dialogboxnetfare = String.valueOf(NetTotalAmount);
                                                    String Num = TextUtils.join("~", ServiceChargeList);
                                                    OperatorserviceCharge = Num.replaceAll("[0-9]+", "0");
                                                    Hotel = "";
                                                }
                                            }

                                        } else if (isShown.equals("false")) {
                                            Net_fares.setText("" + SeatFare + " SAR");
                                            NetTotalAmount = String.valueOf(SeatFare);
                                            dialogboxnetfare = String.valueOf(NetTotalAmount);
                                            String Num = TextUtils.join("~", ServiceChargeList);
                                            OperatorserviceCharge = Num.replaceAll("[0-9]+", "0");
                                            Hotel = "";
                                        }
                                    }else {
                                        Toast.makeText(getApplicationContext(), "No of seats should be same for forward and return journey", Toast.LENGTH_SHORT).show();
                                    }

                                }else {
                                    RelativeLayout.setVisibility(View.VISIBLE);
                                    if (Length == 2)
                                        v.setBackgroundResource(R.drawable.selectedsleeper);
                                    else
                                        v.setBackgroundResource(R.drawable.selected_seat);

                                    if (IsFamily.equals("TRUE")) {
                                        v.setBackgroundResource(R.drawable.familyseatavialable);
                                    }

                                    String select = String.valueOf(number);

                                    arList.addAll(Arrays.<String>asList(String.valueOf(number)));

                                    System.out.println("seats***" + arList);
                                    for (int i = 0; i < arList.size(); i++) {
                                        String s = arList.get(i);
                                        if (i == arList.size() - 1) {

                                            Fare += s;
                                        } else {
                                            Fare += s + ",";
                                        }

                                    }
                                    Seats.setText("" + Fare + "");
                                    System.out.println(Fare);
                                    netfare.addAll(Arrays.<String>asList(String.valueOf(roundValue)));
                                    System.out.println("NetFare" + netfare);

                                    for (int i = 0; i < netfare.size(); i++) {
                                        float s = Float.parseFloat(netfare.get(i));

                                        FareString += s;

                                    }

                                    System.out.println(FareString);

                                    PriceArray.addAll(Arrays.<String>asList(String.valueOf(roundValue)));
                                    ServiceTaxList.addAll(Arrays.<String>asList(String.valueOf(Servicetax)));
                                    System.out.println("seats***" + ServiceTaxList);
                                    float servicetax = 0;
                                    for (int i = 0; i < ServiceTaxList.size(); i++) {
                                        float s = Float.parseFloat((ServiceTaxList.get(i)));

                                        servicetax += s;

                                    }


                                    ServiceChargeList.addAll(Arrays.<String>asList(String.valueOf(OperatorServiceCharge)));
                                    System.out.println("OperatorServiceCharge***" + ServiceChargeList);

                                    float ServiceCharge = 0;
                                    for (int i = 0; i < ServiceChargeList.size(); i++) {

                                        float s = Float.parseFloat(((ServiceChargeList.get(i))));

                                        ServiceCharge += s;
                                    }

                                    dialogboxseats = Fare;
                                    dialogboxamount = FareString;
                                    dialogboxservicetax = (servicetax);
                                    dialogboxservicecharge = (ServiceCharge);
                                    float SeatFare = (FareString + servicetax);
                                    float Checked = (FareString + servicetax + ServiceCharge);
                                    if (isShown.equals("true")) {
                                        if (ISCHECKED.equals("true")) {
                                            Net_fares.setText("" + Checked + " SAR");
                                            NetTotalAmount = String.valueOf(Checked);
                                            dialogboxnetfare = String.valueOf(NetTotalAmount);
                                            dialogboxservicecharge = ServiceCharge;

                                        } else {
                                            if (ISCHECKED.equals("false")) {
                                                Net_fares.setText("" + SeatFare + " SAR");
                                                NetTotalAmount = String.valueOf(SeatFare);
                                                dialogboxnetfare = String.valueOf(NetTotalAmount);
                                                String Num = TextUtils.join("~", ServiceChargeList);
                                                OperatorserviceCharge = Num.replaceAll("[0-9]+", "0");
                                                Hotel = "";
                                            }
                                        }

                                    } else if (isShown.equals("false")) {
                                        Net_fares.setText("" + SeatFare + " SAR");
                                        NetTotalAmount = String.valueOf(SeatFare);
                                        dialogboxnetfare = String.valueOf(NetTotalAmount);
                                        String Num = TextUtils.join("~", ServiceChargeList);
                                        OperatorserviceCharge = Num.replaceAll("[0-9]+", "0");
                                        Hotel = "";
                                    }


                                }
                                LinearAminites_Layout.setVisibility(View.INVISIBLE);
                            } else {
                                Toast.makeText(getApplicationContext(), "Cant select more than 6 seats", Toast.LENGTH_SHORT).show();
                            }


                            }




                    }
                });
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.cancel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_policy:

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

                // set title
                alertDialogBuilder.setTitle("Cancellation policy");

                // set dialog message
                alertDialogBuilder
                        .setMessage(""+cancalpolicy+"  "+"\n"+partailCancellation)
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, close
                                // current activity

                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            new Availablebuses().execute();
        } else {


            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set title
            //  alertDialogBuilder.setTitle("Your Title");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Sorry! Not connected to internet")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity

                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
    //  finish();


}