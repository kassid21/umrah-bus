package com.umrahbus.BeanClasses;

/**
 * Created by jagadeesh on 8/18/2016.
 */
public class Getreports {

    public  String BookingDate;
    public  String BookingRefNo;
    public  String OperatorNamey;
    public  String PassengerName;
    public  String SourceName;
    public  String DestinationName;
    public  String JourneyDate;
    public  String NoofSeats;
    public String BookingStatus;
    public String Fares;
    public String Servicetax;
    public String ServiceCharge;
    public String DepartureTime;
    public String Returndate;
    public String ArravialTime;

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getHotelName() {
        return HotelName;
    }

    public void setHotelName(String hotelName) {
        HotelName = hotelName;
    }

    public String EmailID;
    public String HotelName;

    private boolean selected;

    public String getBookingDate() {
        return BookingDate;
    }

    public void setBookingDate(String bookingDate) {
        BookingDate = bookingDate;
    }

    public String getBookingRefNo() {
        return BookingRefNo;
    }

    public void setBookingRefNo(String bookingRefNo) {
        BookingRefNo = bookingRefNo;
    }

    public String getOperatorNamey() {
        return OperatorNamey;
    }

    public void setOperatorNamey(String operatorNamey) {
        OperatorNamey = operatorNamey;
    }

    public String getPassengerName() {
        return PassengerName;
    }

    public void setPassengerName(String passengerName) {
        PassengerName = passengerName;
    }

    public String getSourceName() {
        return SourceName;
    }

    public void setSourceName(String sourceName) {
        SourceName = sourceName;
    }

    public String getDestinationName() {
        return DestinationName;
    }

    public void setDestinationName(String destinationName) {
        DestinationName = destinationName;
    }

    public String getJourneyDate() {
        return JourneyDate;
    }

    public void setJourneyDate(String journeyDate) {
        JourneyDate = journeyDate;
    }

    public String getNoofSeats() {
        return NoofSeats;
    }

    public void setNoofSeats(String noofSeats) {
        NoofSeats = noofSeats;
    }

    public String getBookingStatus() {
        return BookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        BookingStatus = bookingStatus;
    }

    public String getFares() {
        return Fares;
    }

    public void setFares(String fares) {
        Fares = fares;
    }

    public String getServicetax() {
        return Servicetax;
    }

    public void setServicetax(String servicetax) {
        Servicetax = servicetax;
    }

    public String getsetServiceCharge() {
        return ServiceCharge;
    }

    public void setServiceCharge(String serviceCharge) {
        ServiceCharge = serviceCharge;
    }

    public String getDepartureTime() {
        return DepartureTime;
    }

    public void setDepartureTime(String departureTime) {
        DepartureTime = departureTime;
    }




    public String getArravialTime() {
        return ArravialTime;
    }

    public void setArravialTime(String arravialTime) {
        ArravialTime = arravialTime;
    }

    public String getReturndate() {
        return Returndate;
    }

    public void setReturndate(String Returnnndate) {
        Returndate = Returnnndate;
    }
}

