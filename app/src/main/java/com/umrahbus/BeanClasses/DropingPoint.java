package com.umrahbus.BeanClasses;

/**
 * Created by jagadeesh on 7/12/2016.
 */
public class DropingPoint {

    public  String DroppingAddress;
    public  String DroppingContactNumbers;
    public  String DroppingContactPersons;
    public  String DroppingPointId;
    public  String DroppingLandmark;
    public  String DroppingLocation;
    public  String DroppingName;
    public  String DroppingTime;

    private boolean selected;

    public String getDroppingAddress() {
        return DroppingAddress;
    }

    public void setDroppingAddress(String droppingAddress) {
        DroppingAddress = droppingAddress;
    }

    public String getDroppingContactNumbers() {
        return DroppingContactNumbers;
    }

    public void setDroppingContactNumbers(String droppingContactNumbers) {
        DroppingContactNumbers = droppingContactNumbers;
    }

    public String getDroppingContactPersons() {
        return DroppingContactPersons;
    }

    public void setDroppingContactPersons(String droppingContactPersons) {
        DroppingContactPersons = droppingContactPersons;
    }

    public String getDroppingPointId() {
        return DroppingPointId;
    }

    public void setDroppingPointId(String droppingPointId) {
        DroppingPointId = droppingPointId;
    }

    public String getDroppingLandmark() {
        return DroppingLandmark;
    }

    public void setDroppingLandmark(String droppingLandmark) {
        DroppingLandmark = droppingLandmark;
    }

    public String getDroppingLocation() {
        return DroppingLocation;
    }

    public void setDroppingLocation(String droppingLocation) {
        DroppingLocation = droppingLocation;
    }

    public String getDroppingName() {
        return DroppingName;
    }

    public void setDroppingName(String droppingName) {
        DroppingName = droppingName;
    }

    public String getDroppingTime() {
        return DroppingTime;
    }

    public void setDroppingTime(String droppingTime) {
        DroppingTime = droppingTime;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
