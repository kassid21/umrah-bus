/**
 * Copyright (c) 2016, Serge NTONG
 * All rights reserved.
 * <p/>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * <p/>
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * <p/>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * <p/>
 * /**
 *
 * @author Serge NTONG <sergentong@gmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @category Libraries
 * @version 1.0
 */

package com.umrahbus.Paymentgateways.http;

import android.os.AsyncTask;
import android.os.Bundle;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

public class Request extends AsyncTask<Void, Integer, Void> {
    public final static String METHOD_POST = "POST";
    public final static String METHOD_GET = "GET";

    protected Object o;
    protected RequestCallback callback = null;
    protected String method;
    protected String url;
    protected String response = "";
    protected boolean useCache = true;

    protected HashMap<String, String> params = new HashMap<>();

    Boolean unknownHostException = false;

    public Request() {
    }

    public RequestCallback getCallback() {
        return callback;
    }

    public void setCallback(Object o, RequestCallback callback) {
        this.o = o;
        this.callback = callback;
    }

    public void setCallback(RequestCallback callback) {
        this.callback = callback;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean addParam(String name, String value) {
        params.put(name, value);
        return true;
    }

    public boolean addParam(String name, int value) {
        params.put(name, Integer.toString(value));
        return true;
    }

    public boolean addParam(String name, float value) {
        params.put(name, Float.toString(value));
        return true;
    }

    public boolean addParam(String name, double value) {
        params.put(name, Double.toString(value));
        return true;
    }

    public boolean addParam(String name, long value) {
        params.put(name, Long.toString(value));
        return true;
    }

    private String getPostDataString() throws UnsupportedEncodingException {

        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public boolean isUseCache() {
        return useCache;
    }

    public Request setUseCache(boolean useCache) {
        this.useCache = useCache;
        return this;
    }

    protected InputStream openConnection() {
        HttpURLConnection connection = null;
        String url = this.getUrl();
        String method = this.getMethod();
        InputStream inputStream = null;

        try {
            // Create connection
            URL httpUrl = new URL(url);

            connection = (HttpURLConnection) httpUrl.openConnection();

            connection.setRequestMethod(method);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "utf-8");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);

            // Send request
            DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
            dStream.writeBytes(this.getPostDataString());
            dStream.flush();
            dStream.close();

            int responseCode = connection.getResponseCode();

            if (responseCode != HttpURLConnection.HTTP_OK) {
                return null;
            }

            // Get Response
            inputStream = connection.getInputStream();

            response = inputStreamToString(inputStream);

        } catch (UnknownHostException e) {
            this.unknownHostException = true;
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        // Return Response
        return inputStream;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Void doInBackground(Void... params) {
        // Return Response
        this.openConnection();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {

        Bundle bundle = new Bundle();
        bundle.putBoolean("unknownHostException", unknownHostException);

        if (null != callback) {

            callback.setResponse(response);
            callback.setBundle(bundle);
            callback.run();

        }

        super.onPostExecute(aVoid);
    }

    public static String inputStreamToString(InputStream inputStream) {

        if (null == inputStream) {
            return "";
        }

        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            StringBuffer response = new StringBuffer();

            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }

            rd.close();

            String reponseString = response.toString();

            return reponseString;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

}
