package com.umrahbus.Paymentgateways;

import android.content.Intent;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.umrahbus.MainActivity;
import com.umrahbus.draweritems.TicketFormatActivity;
import com.umrahbus.R;


/**
 * Created by Administrator on 08-03-2016.
 */
public class ResponseActivity extends AppCompatActivity implements View.OnClickListener {

    View success,failed,pending;
    String newString,refno,stringname,stringmessage,stringReturnReferencenumber;
    TextView referenceno,message,name;
    Button clickhere;
    protected  void onCreate(Bundle savedInstanstate){
        super.onCreate(savedInstanstate);
        setContentView(R.layout.booking_status);

        if (savedInstanstate == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                newString= null;
                refno=null;
                stringmessage=null;
                stringname=null;
            } else {
                newString= extras.getString("status");
                refno=extras.getString("Onwardrefno");
                stringname=extras.getString("name");
                stringmessage=extras.getString("msg");
                stringReturnReferencenumber=extras.getString("Returnrefno");

            }
        } else {
            newString= (String) savedInstanstate.getSerializable("status");
            refno=(String)savedInstanstate.getSerializable("Onwardrefno");
            stringname=(String)savedInstanstate.getSerializable("name");
            stringmessage=(String)savedInstanstate.getSerializable("msg");
             stringReturnReferencenumber=(String)savedInstanstate.getSerializable("Returnrefno");
        }
        success=(View)findViewById(R.id.success);
        failed=(View)findViewById(R.id.failed);
        pending=(View)findViewById(R.id.pendingrec);
        name=(TextView)findViewById(R.id.message);
        clickhere=(Button)findViewById(R.id.clickhere);
        clickhere.setOnClickListener(this);
        if(newString.equals("1")){
            success.setVisibility(View.GONE);
            failed.setVisibility(View.VISIBLE);
            pending.setVisibility(View.GONE);

         clickhere.setText("Click Here For\nAnother Booking");



            referenceno=(TextView)success.findViewById(R.id.srefnotv);
            referenceno.setText("Your Reference Number is "+refno);

            name.setText("Hotel Room(s) booking is Failed");
           // message.setText(stringmessage);
        }else if(newString.equals("4")){
            success.setVisibility(View.GONE);
            failed.setVisibility(View.VISIBLE);
            pending.setVisibility(View.GONE);




            referenceno=(TextView)failed.findViewById(R.id.frefnotv);

            referenceno.setText("Your Reference Number is "+refno);
            name.setText("Bus seat booking is Failed");
            clickhere.setText("Click Here For\nAnother Seat");
          //  message.setText(stringmessage);
        }else if(newString.equals("3")){
            success.setVisibility(View.VISIBLE);
            failed.setVisibility(View.GONE);
            pending.setVisibility(View.GONE);

            referenceno=(TextView)pending.findViewById(R.id.prefnotv);
            referenceno.setText("Your Reference Number is "+refno);
            name.setText("Bus seat booking is successful");
            clickhere.setText("Click Here to\nCheck Ticket Details");
           // message.setText(stringmessage);
        }else if (newString.equals("5")){
            success.setVisibility(View.GONE);
            failed.setVisibility(View.GONE);
            pending.setVisibility(View.VISIBLE);

            referenceno=(TextView)pending.findViewById(R.id.prefnotv);
            referenceno.setText("Your Booking Request is received by Admin - Your Booking Reference Number = "+refno);
            name.setText("Booking is subject to Payment. On non-payment booking will get unblocked automatically");
            clickhere.setText("Click Here to\nCheck Ticket Details");
        }

    }

    @Override
    public void onClick(View v) {
        if(v==clickhere){
            if(newString.equals("1")||newString.equals("4")||newString.equals("3")||newString.equals("5")){
                Intent i=new Intent(ResponseActivity.this,TicketFormatActivity.class);
                i.putExtra("status",newString);
                i.putExtra("Onwardrefno", refno);
                i.putExtra("Returnrefno",stringReturnReferencenumber);
                startActivity(i);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(this,MainActivity.class));
    }
}
