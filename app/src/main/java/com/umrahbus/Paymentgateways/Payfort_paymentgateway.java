package com.umrahbus.Paymentgateways;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.umrahbus.ServicesClasses.ServiceHandler;
import com.google.gson.Gson;
import com.payfort.fort.android.sdk.base.FortSdk;
import com.payfort.fort.android.sdk.base.callbacks.FortCallBackManager;
import com.payfort.sdk.android.dependancies.base.FortInterfaces;
import com.payfort.sdk.android.dependancies.models.FortRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jagadeesh on 3/13/2017.
 */

public class Payfort_paymentgateway  {


    private final static String KEY_MERCHANT_IDENTIFIER = "merchant_identifier";
    private final static String KEY_SERVICE_COMMAND = "service_command";
    private final static String KEY_DEVICE_ID = "device_id";
    private final static String KEY_LANGUAGE = "language";
    private final static String KEY_ACCESS_CODE = "access_code";
    private final static String KEY_SIGNATURE = "signature";
    private final static String SDK_TOKEN = "SDK_TOKEN";

    public static final int RESPONSE_GET_TOKEN = 111;
    public static final int RESPONSE_PURCHASE = 222;
    public static final int RESPONSE_PURCHASE_CANCEL = 333;
    public static final int RESPONSE_PURCHASE_SUCCESS = 444;
    public static final int RESPONSE_PURCHASE_FAILURE = 555;
    public final static String CURRENCY_TYPE = "SAR";
    public final static String LANGUAGE_TYPE = "en";

    private Activity context;

    String PayfortAPI ="https://paymentservices.payfort.com/FortAPI/paymentApi?";

    String TestPayfort="https://sbpaymentservices.payfort.com/FortAPI/paymentApi";

    //LIVE***Payfort Keys
  /*  String PayFortEnviroment = "LIVE";
    String AccessCode = "TDVBXRcsvAtpgmSuCewU";
    String PayfortMarchantID = "JKOgMFgp";
    private final static String SHA_REQUEST_PHRASE = "umrahbusreq";*/

    String language = "en";
    String signature;
    String PayfortTokenurl = "http://webumrahbus.i2space.co.in/Admin/GetPayfortSDKToken?";
    String sdktoken;

    //TEST****Payfort Keys
    String PayFortEnviroment = "TEST";
    String AccessCode = "mCYTSSsCOKWZWx7MLs81";
    String PayfortMarchantID ="JkvfTgBr";
    private final static String SHA_REQUEST_PHRASE = "sadsddfeses";

/*    String PayFortEnviroment = "TEST";
    String AccessCode = "mCYTSSsCOKWZWx7MLs81";
    String PayfortMarchantID ="sqliMovc";
    private final static String SHA_REQUEST_PHRASE = "cvbdfg35";*/

    /*JkvfTgBr
     mCYTSSsCOKWZWx7MLs81
    sadsddfeses*/


    private final static String SHA_TYPE = "SHA-256";
    InputStream inputStream;
    String data, responce;
    String deviceid;
    private FortCallBackManager fortCallback = null;
    private IPaymentRequestCallBack iPaymentRequestCallBack;
    private ProgressDialog progressDialog;
    private PayFortData payFortData;
    private  Gson gson;
    JSONObject obj;
    public final static String PURCHASE = "PURCHASE";


    public Payfort_paymentgateway(Activity context, FortCallBackManager fortCallback, IPaymentRequestCallBack iPaymentRequestCallBack) {
        this.context = context;
        this.fortCallback = fortCallback;
        this.iPaymentRequestCallBack = iPaymentRequestCallBack;

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Processing your payment\nPlease wait...");
        progressDialog.setCancelable(false);
        gson=new Gson();

    }
    public void requestForPayment(PayFortData payFortData) {
        this.payFortData = payFortData;
        new GetTokenFromServer().execute();
    }
    private class GetTokenFromServer extends AsyncTask<String, String, String> {

        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(context, "", "Processing your payment\nPlease Wait");

        }

        @Override
        protected String doInBackground(String... params) {


   /*         DefaultHttpClient httpClient=new DefaultHttpClient();
            HttpPost httpPost=new HttpPost("https://sbpaymentservices.payfort.com/FortAPI/paymentApi");

            httpPost.setHeader("Content-type", "application/json");

            String str=getTokenParams();

            StringEntity stringEntity;

            try {
                stringEntity=new StringEntity(str,"UTF_8");
                httpPost.setEntity(stringEntity);
                HttpResponse httpResponse;
                httpResponse=httpClient.execute(httpPost);
                Log.v("jsonresponce", "" + str);
                inputStream = httpResponse.getEntity().getContent();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream));
            String line = "";
            data = "";
            try {
                while ((line = bufferedReader.readLine()) != null)
                    data += line;
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                inputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.d("TAG_LOCATIONS", str);

            //   Log.v("rsponse", "" + JsonWriter.formatJson(data.toString()).toString());
            System.out.println("Status" + data);
            responce = "" + str;*/
            try {

                String url;
                getTokenParams();
                url=PayfortTokenurl+"&mode="+PayFortEnviroment+"&service_command="+SDK_TOKEN+
                        "&merchant_identifier="+PayfortMarchantID+"&access_code="+AccessCode+"&signature="+signature+
                        "&language="+language+"&device_id="+deviceid;

                System.out.println("url" + url);
                ServiceHandler sh = new ServiceHandler();
                String jsonstr=sh.makeServiceCall(url, ServiceHandler.GET);
                if (jsonstr!=null){


                    obj=new JSONObject(jsonstr);

                    String message=obj.getString("Message");
                    JSONObject msgobject=new JSONObject(message);
                    payFortData.sdkToken=msgobject.getString("sdk_token");
                    String responcemsg=obj.getString("StatusCode");
                    if (responcemsg.equals("400")){
                        requestPurchase();
                    }else {
                        Toast.makeText(context,""+responcemsg,Toast.LENGTH_LONG).show();
                    }
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.cancel();


        }
    }

    private void requestPurchase() {
        try {
            FortSdk.getInstance().registerCallback(context,getPurchaseFortRequest(),FortSdk.ENVIRONMENT.PRODUCTION, RESPONSE_PURCHASE,
                   fortCallback, new FortInterfaces.OnTnxProcessed() {
                        @Override
                        public void onCancel(Map<String, String> map, Map<String, String> responseMap) {
                            JSONObject response = new JSONObject(responseMap);
                            PayFortData payFortData=gson.fromJson(response.toString(),PayFortData.class);
                            payFortData.paymentResponse = response.toString();
                            Log.e("Cancel Response", response.toString());
                            if (iPaymentRequestCallBack != null) {
                                iPaymentRequestCallBack.onPaymentRequestResponse(RESPONSE_PURCHASE_CANCEL, payFortData);

                            }
                        }

                        @Override
                        public void onSuccess(Map<String, String> map, Map<String, String> fortResponseMap) {
                            JSONObject response = new JSONObject(fortResponseMap);

                            PayFortData payFortData = gson.fromJson(response.toString(), PayFortData.class);
                            payFortData.paymentResponse = response.toString();
                            Log.e("Success Response", response.toString());
                            if (iPaymentRequestCallBack != null) {
                                iPaymentRequestCallBack.onPaymentRequestResponse(RESPONSE_PURCHASE_SUCCESS, payFortData);
                            }
                        }

                        @Override
                        public void onFailure(Map<String, String> map, Map<String, String> fortResponseMap) {
                            JSONObject response = new JSONObject(fortResponseMap);
                            PayFortData payFortData = gson.fromJson(response.toString(), PayFortData.class);
                            payFortData.paymentResponse = response.toString();
                            Log.e("Failure Response", response.toString());
                            if (iPaymentRequestCallBack != null) {
                                iPaymentRequestCallBack.onPaymentRequestResponse(RESPONSE_PURCHASE_FAILURE, payFortData);
                            }
                        }
                    });
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    private FortRequest getPurchaseFortRequest() {
        FortRequest fortRequest = new FortRequest();
        if ( obj!= null) {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("command",payFortData.command);
            parameters.put("merchant_reference",payFortData.merchantReference);
            parameters.put("amount",payFortData.amount);
            parameters.put("currency",payFortData.currency);
            parameters.put("language",payFortData.language);
            parameters.put("customer_email",payFortData.customerEmail);
            parameters.put("sdk_token",payFortData.sdkToken);
            fortRequest.setRequestMap(parameters);

            Log.e("code",parameters.toString());
        }
        return fortRequest;
    }


    public String getTokenParams() {
        JSONObject object=new JSONObject();

        deviceid= FortSdk.getDeviceId(context);
        deviceid= FortSdk.getDeviceId(context);
        System.out.print("Deviceid"+deviceid);

        String concatenatedstring=SHA_REQUEST_PHRASE+
                KEY_ACCESS_CODE + "=" +AccessCode+
                KEY_DEVICE_ID + "=" +deviceid+
                KEY_LANGUAGE + "=" +language+
                KEY_MERCHANT_IDENTIFIER + "=" +PayfortMarchantID+
                KEY_SERVICE_COMMAND +"=" + SDK_TOKEN+
                SHA_REQUEST_PHRASE;

        try {
            object.put(KEY_SERVICE_COMMAND,SDK_TOKEN);
            object.put(KEY_MERCHANT_IDENTIFIER,PayfortMarchantID);
            object.put(KEY_ACCESS_CODE,AccessCode);
            signature = getSignatureSHA256(concatenatedstring);
            object.put(KEY_SIGNATURE,signature);
            object.put(KEY_DEVICE_ID,deviceid);
            object.put(KEY_LANGUAGE,language);

            Log.e("concatenatedString", concatenatedstring);
            Log.e("signature", signature);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Jsonstring",String.valueOf(object));
        return String.valueOf(object);
    }
    private static String getSignatureSHA256(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(SHA_TYPE);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            return String.format("%0" + (messageDigest.length * 2) + 'x', new BigInteger(1, messageDigest));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }



}
