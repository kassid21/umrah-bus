package com.umrahbus.Paymentgateways;

/**
 * Created by jagadeesh on 3/14/2017.
 */
public interface IPaymentRequestCallBack {
    void onPaymentRequestResponse(int responseType, PayFortData responseData);
}
