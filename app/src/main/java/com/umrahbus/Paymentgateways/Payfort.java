package com.umrahbus.Paymentgateways;

/**
 * Created by jagadeesh on 3/14/2017.
 */

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.umrahbus.Constants;
import com.umrahbus.MainActivity;
import com.payfort.fort.android.sdk.base.callbacks.FortCallBackManager;
import com.payfort.fort.android.sdk.base.callbacks.FortCallback;
import com.umrahbus.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Payfort extends AppCompatActivity implements IPaymentRequestCallBack {

    private Button tvPurchage;
    private TextView pay;
    public FortCallBackManager fortCallback = null;
    String amt;
    InputStream inputStream;
    private String mfirstname; // From Previous Activity
    private String memail; // From Previous Activity
    String OnwardReferenceNumber;
    private String mphone;
    String data;
    String refno,ReturnTicketrefno;
    PayFortData payFortData;
    String status;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payfort_payment_gateway);
        initilizePayFortSDK();
        pay= (TextView) findViewById(R.id.Amount);
        tvPurchage = (Button) findViewById(R.id.tvPurchage);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            mfirstname = bundle.getString("name");
            memail = bundle.getString("email");
            amt = bundle.getString("amount");
            mphone = bundle.getString("phone");
            refno = bundle.getString("referencenumber");
            ReturnTicketrefno=bundle.getString("ReturnTicketrReferenceNumber");


            Log.i(amt, "" + mfirstname + " : " + memail + " : " + mphone + " : "  + refno );
        }
        pay.setText("You have to pay"+" "+amt+" "+"SAR");

        ActivityCompat.requestPermissions(Payfort.this,
                new String[]{Manifest.permission.READ_PHONE_STATE},
                1);

        tvPurchage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestForPayfortPayment();
            }
        });
    }

    private void initilizePayFortSDK() {
        fortCallback = FortCallback.Factory.create();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Payfort_paymentgateway.RESPONSE_PURCHASE) {
            fortCallback.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void requestForPayfortPayment() {
        PayFortData payFortData = new PayFortData();

            payFortData.amount =String.valueOf((int) (Float.parseFloat(amt))*100);
            payFortData.command = Payfort_paymentgateway.PURCHASE;
            payFortData.currency = Payfort_paymentgateway.CURRENCY_TYPE;
            payFortData.customerEmail =memail;
            payFortData.language = Payfort_paymentgateway.LANGUAGE_TYPE;
            payFortData.merchantReference =refno;

            Payfort_paymentgateway payFortPayment = new Payfort_paymentgateway(this, this.fortCallback, this);
            payFortPayment.requestForPayment(payFortData);

    }

    @Override
    public void onPaymentRequestResponse(int responseType, final PayFortData responseData) {
        if (responseType == Payfort_paymentgateway.RESPONSE_GET_TOKEN) {
            Toast.makeText(this, "Token not generated", Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Token not generated");
        } else if (responseType == Payfort_paymentgateway.RESPONSE_PURCHASE_CANCEL) {
            Toast.makeText(this, "Payment cancelled", Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Payment cancelled");
            Intent ip=new Intent(Payfort.this,MainActivity.class);
            startActivity(ip);
        } else if (responseType == Payfort_paymentgateway.RESPONSE_PURCHASE_FAILURE) {
            Toast.makeText(this, "Payment failed", Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Payment failed");
            new PaymentFailed().execute();
        } else {
            Toast.makeText(this, "Payment successful", Toast.LENGTH_SHORT).show();
            Log.e("onPaymentResponse", "Payment successful");
            new Busbooking().execute();
        }
    }
    public class  PaymentFailed extends AsyncTask<String,String,String> {

        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog=ProgressDialog.show(Payfort.this,"","Please Wait ...");
        }

        @Override
        protected String doInBackground(String... params) {


            // List<NameValuePair> bcparam=new ArrayList<NameValuePair>();
            JSONObject bcparam = new JSONObject();
            // List<NameValuePair> parm=new ArrayList<NameValuePair>();
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Constants.BUSAPI+ Constants.SavePaymentDetails);
            httpPost.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpPost.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            try {
                bcparam.put("PaymentGatewayId",payFortData);
                bcparam.put("Amount", amt);

                bcparam.put("PaymentStatus", "Failure");
                bcparam.put("ResponseString", "PayFort");

                bcparam.put("IPAddress", "::1");
                bcparam.put("ReferenceNumber",refno);
                bcparam.put("DeviceModel","");
                bcparam.put("DeviceOS","Android");
                bcparam.put("DeviceOSVersion","");
                bcparam.put("DeviceToken","");
                bcparam.put("ApplicationType","Mobile");


                // httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                // parm.add(new BasicNameValuePair("res",bcparam.toString()));
                try {
                    StringEntity seEntity;
                    // String data1 = null;
				/*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                    seEntity = new StringEntity(bcparam.toString(), "UTF_8");
                    seEntity.setContentType("application/json");
                    httpPost.setEntity(seEntity);
                    HttpResponse httpResponse;

                    httpResponse = httpClient.execute(httpPost);

                    inputStream = httpResponse.getEntity().getContent();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //json=bcparam.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream));
            String line = "";
            data = "";
            try {
                while ((line = bufferedReader.readLine()) != null)
                    data += line;
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                inputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.v("Json Object",""+bcparam);
            Log.v("Save Data", "" + data);


            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            try{

                JSONObject jsonObject=new JSONObject(data);
                String strstatus=   jsonObject.getString("StatusCode");
                if(strstatus.contains("200")){

                    Intent intent = new Intent(Payfort.this, MainActivity.class);
                    startActivity(intent);
                }else {

                }
            }catch (Exception e){
                e.printStackTrace();
            }




        }
    }

    public  class Busbooking extends AsyncTask<String,String,String> {

        ProgressDialog dialog;
        String res;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog=ProgressDialog.show(Payfort.this,"","Please Wait...");
        }

        @Override
        protected String doInBackground(String... params) {

            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(Constants.BUSAPI+ Constants.BookBusTicket+refno);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpGet.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            try {
                res = httpClient.execute(httpGet, responseHandler);
            } catch (IOException e) {

                e.printStackTrace();
            }

            Log.v("Busbooking", "" + res);
            return res;


        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            String sts,msg,ref,PNRNUMBER;
            JSONObject jsonObject = null;
            try {



                if ((!res.equals("")) && (!res.equals(null)) ) {
                    //  Toast.makeText(getApplicationContext(),"hiii welcome to splash Screen",2000).show();
                    jsonObject = new JSONObject(res);
                    // encryptclientid=jsonObject.getString("ClientId")
                    PNRNUMBER=jsonObject.getString("OperatorPNR");
                    OnwardReferenceNumber=jsonObject.getString("ReferenceNo");
                    jsonObject.getString("APIReferenceNo");
                    msg= jsonObject.getString("Message");
                    sts=  jsonObject.getString("BookingStatus");
                    jsonObject.getString("ResponseStatus");
                    jsonObject.getString("RefundResponse");
                    jsonObject.getString("Provider");



                    //    Toast.makeText(Paymentinfo.this,""+data,3000).show();

                    if(ReturnTicketrefno!=null) {
                        new Payfort.ReturnTicketBusbooking().execute();
                    }else if(sts.equals("1")){
                        Intent i = new Intent(Payfort.this, ResponseActivity.class);
                        i.putExtra("name", mfirstname);
                        i.putExtra("email", memail);
                        i.putExtra("phone", mphone);
                        i.putExtra("amount", String.valueOf(amt));
                        i.putExtra("Pnrnumber", PNRNUMBER);
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", refno);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);
                    } else if(sts.equals("4")){
                        Intent i = new Intent(Payfort.this, ResponseActivity.class);
                        i.putExtra("name", mfirstname);
                        i.putExtra("email", memail);
                        i.putExtra("phone", mphone);
                        i.putExtra("amount", String.valueOf(amt));
                        i.putExtra("Pnrnumber", PNRNUMBER);
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", refno);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);

                    }else if(sts.equals("3")){
                        Intent i = new Intent(Payfort.this, ResponseActivity.class);
                        i.putExtra("name", mfirstname);
                        i.putExtra("email", memail);
                        i.putExtra("phone", mphone);
                        i.putExtra("amount", String.valueOf(amt));
                        i.putExtra("Pnrnumber", PNRNUMBER);
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", refno);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);

                    }
                    else {
                        new Payment().execute();
                        Toast.makeText(Payfort.this,"Some Problem Occured.\n Please Check your credentials",Toast.LENGTH_LONG).show();
                    }
                }else {
                    new Payment().execute();
                    Toast.makeText(Payfort.this,"Some Problem Occured with Provider\n Please Try later",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {
                new Payment().execute();
                Toast.makeText(Payfort.this,"Some Problem Occured with Provider\n Please Try later",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
           /* Intent intent = new Intent(PayUmoney.this, MainActivity.class);
            startActivity(intent);*/

        }

    }
    public  class ReturnTicketBusbooking extends AsyncTask<String,String,String> {

        ProgressDialog dialog;
        String res;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog=ProgressDialog.show(Payfort.this,"","Please Wait..\nGood things take time :)");
        }

        @Override
        protected String doInBackground(String... params) {

            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(Constants.BUSAPI+ Constants.BookBusTicket+ReturnTicketrefno);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpGet.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            try {
                res = httpClient.execute(httpGet, responseHandler);
            } catch (IOException e) {

                e.printStackTrace();
            }

            Log.v("Busbooking", "" + res);
            return res;


        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            String sts,msg,ref;
            JSONObject jsonObject = null;
            try {



                if ((!res.equals("")) && (!res.equals(null)) ) {
                    //  Toast.makeText(getApplicationContext(),"hiii welcome to splash Screen",2000).show();
                    jsonObject = new JSONObject(res);
                    // encryptclientid=jsonObject.getString("ClientId")
                    jsonObject.getString("OperatorPNR");
                    ref=jsonObject.getString("ReferenceNo");
                    jsonObject.getString("APIReferenceNo");
                    msg= jsonObject.getString("Message");
                    sts=  jsonObject.getString("BookingStatus");
                    jsonObject.getString("ResponseStatus");
                    jsonObject.getString("RefundResponse");
                    jsonObject.getString("Provider");



                    //    Toast.makeText(Paymentinfo.this,""+data,3000).show();
                    if (sts.equals("1")) {


                        Intent i = new Intent(Payfort.this, ResponseActivity.class);
                        i.putExtra("name", mfirstname);
                        i.putExtra("email", memail);
                        i.putExtra("phone", mphone);
                        i.putExtra("amount", String.valueOf(amt));
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", OnwardReferenceNumber);
                        i.putExtra("Returnrefno", ref);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);

                    } else if(sts.equals("4")){
                        Intent i = new Intent(Payfort.this, ResponseActivity.class);
                        i.putExtra("name", mfirstname);
                        i.putExtra("email", memail);
                        i.putExtra("phone", mphone);
                        i.putExtra("amount", String.valueOf(amt));
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", OnwardReferenceNumber);
                        i.putExtra("Returnrefno", ref);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);

                    }else if(sts.equals("3")){
                        Intent i = new Intent(Payfort.this, ResponseActivity.class);
                        i.putExtra("name", mfirstname);
                        i.putExtra("email", memail);
                        i.putExtra("phone", mphone);
                        i.putExtra("amount", String.valueOf(amt));
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", OnwardReferenceNumber);
                        i.putExtra("Returnrefno", ref);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);

                    }
                    else {

                        Toast.makeText(Payfort.this,"Some Problem Occured.\n Please Check your credentials",Toast.LENGTH_LONG).show();
                    }
                }else {

                    Toast.makeText(Payfort.this,"Some Problem Occured with Provider\n Please Try later",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                Toast.makeText(Payfort.this,"Some Problem Occured with Provider\n Please Try later",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
           /* Intent intent = new Intent(PayUmoney.this, MainActivity.class);
            startActivity(intent);*/

        }

    }

    public class  Payment extends AsyncTask<String,String,String>{

        ProgressDialog dialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog=ProgressDialog.show(Payfort.this,"","Please Wait ...");
        }

        @Override
        protected String doInBackground(String... params) {


            // List<NameValuePair> bcparam=new ArrayList<NameValuePair>();
            JSONObject bcparam = new JSONObject();
            // List<NameValuePair> parm=new ArrayList<NameValuePair>();
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Constants.BUSAPI+ Constants.SavePaymentDetails);
            httpPost.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpPost.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            try {
                bcparam.put("PaymentGatewayId",payFortData.authorizationCode);
                bcparam.put("Amount", amt);
                if(status.equals("1")) {
                    bcparam.put("PaymentStatus", "Success");
                    bcparam.put("ResponseString", payFortData.responseMessage);
                }else {
                    bcparam.put("PaymentStatus", "Failure");
                    bcparam.put("ResponseString", payFortData.responseMessage);
                }
                bcparam.put("IPAddress", "::1");
                bcparam.put("ReferenceNumber",refno);
                bcparam.put("DeviceModel","");
                bcparam.put("DeviceOS","Android");
                bcparam.put("DeviceOSVersion","");
                bcparam.put("DeviceToken","");
                bcparam.put("ApplicationType","Mobile");


                // httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                // parm.add(new BasicNameValuePair("res",bcparam.toString()));
                try {
                    StringEntity seEntity;
                    // String data1 = null;
				/*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                    seEntity = new StringEntity(bcparam.toString(), "UTF_8");
                    seEntity.setContentType("application/json");
                    httpPost.setEntity(seEntity);
                    HttpResponse httpResponse;

                    httpResponse = httpClient.execute(httpPost);

                    inputStream = httpResponse.getEntity().getContent();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //json=bcparam.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream));
            String line = "";
            data = "";
            try {
                while ((line = bufferedReader.readLine()) != null)
                    data += line;
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                inputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.v("Json Object",""+bcparam);
            Log.v("Save Data", "" + data);


            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            try{

                JSONObject jsonObject=new JSONObject(data);
                String strstatus=   jsonObject.getString("StatusCode");
                if(strstatus.contains("200")){

                    Intent intent = new Intent(Payfort.this, MainActivity.class);
                    startActivity(intent);
                }else {

                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }



}

