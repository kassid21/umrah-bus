package com.umrahbus.loginpackage;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.umrahbus.Constants;
import com.umrahbus.ServicesClasses.ServiceHandler;
import com.umrahbus.draweritems.TermsAndConditions;
import com.umrahbus.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.UUID;

/**
 * Created by jagadeesh on 7/14/2016.
 */
public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    EditText firstname,lastname,email,password,mobilenum;
    String fname,lname,mails,pwd,mobile;
    Button signup,agent,user;
    Boolean user_agent=true;
    String result,data;
    String otpstring,parentid;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    InputStream inputStream;
    SharedPreferences sharedPreferences;
    TextView mTc,mPrivacy;
    CheckBox checkbox;
    ProgressDialog mProgressDialog;
    String sRole="6";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        signup=(Button)findViewById(R.id.donereg);

        pref = getSharedPreferences("LoginPreference", 0);
        parentid = pref.getString("SuperAdmin",null);
        editor = pref.edit();
        firstname=(EditText)findViewById(R.id.regfirstname);
        lastname=(EditText)findViewById(R.id.reglastname);
        email=(EditText)findViewById(R.id.regmail);
        mobilenum=(EditText)findViewById(R.id.regmobile);
        agent=(Button)findViewById(R.id.agentsignup);
        user=(Button)findViewById(R.id.usersignup);
        mPrivacy= (TextView) findViewById(R.id.policy);
        mTc= (TextView) findViewById(R.id.terms);

        user.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        user.setTextColor(getResources().getColor(R.color.colorWhite));
        agent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        agent.setTextColor(getResources().getColor(R.color.colorPrimary));
        user_agent = true;
        checkbox= (CheckBox) findViewById(R.id.checkBox);
        signup.setOnClickListener(this);
        user.setOnClickListener(this);
        agent.setOnClickListener(this);

        mTc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ip=new Intent(RegisterActivity.this,TermsAndConditions.class);
                startActivity(ip);
            }
        });
        mPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lp=new Intent(RegisterActivity.this,Privacy.class);
                startActivity(lp);
            }
        });
    }
    @Override
    public void onClick(View v) {

            if (v == signup) {
                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                pwd = uuid.substring(0, 6);
                fname = firstname.getText().toString();
                lname = lastname.getText().toString();
                mails = email.getText().toString();

                mobile = mobilenum.getText().toString();
                int length = mobile.length();
                if (!fname.equals("") && !lname.equals("") && !mails.equals("") && !mobile.equals("") && length == 9 && mails.matches(Constants.emailPattern)&&checkbox.isChecked()==true) {

                    if (sRole.equals("6")) {
                        new Getparentid().execute();
                    } else {
                        new GetOtp().execute();
                    }
                } else {
                    if (fname.equals("")) {
                        firstname.setError("Enter FirstName");
                    }
                    if (lname.equals("")) {
                        lastname.setError("Enter LastName");
                    }
                    if (mobile.equals("")) {
                        mobilenum.setError("Enter Mobile Number");
                    }
                    if (mobile.length() < 9) {
                        mobilenum.setError("Enter Valid Mobile Number");
                    }
                    if (mails.equals("")) {
                        email.setError("Enter Valid Email");
                    }
                    if (!mails.matches(Constants.emailPattern)) {
                        email.setError("Enter Valid Email");
                    }
                    if(checkbox.isChecked()==false){
                        Toast.makeText(getApplicationContext(),"Please agree to terms and conditions, Privacy Policy",Toast.LENGTH_LONG).show();
                    }

                }
            }



        if(v==user){

            user.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            user.setTextColor(getResources().getColor(R.color.colorWhite));
            agent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            agent.setTextColor(getResources().getColor(R.color.colorPrimary));
            //user_agent = false;
            sRole="6";
        }
        if(v==agent){

            agent.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            agent.setTextColor(getResources().getColor(R.color.colorWhite));
            user.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            user.setTextColor(getResources().getColor(R.color.colorPrimary));
            //user_agent = true;
            sRole="4";
        }



    }

    public  class  GetOtp extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... params) {
            ServiceHandler sh= new ServiceHandler();
            String jsonstr= sh.makeServiceCall(Constants.BUSAPI+ Constants.OTP+mobile+"&emailId="+mails, ServiceHandler.GET);
            if (jsonstr!=null && !jsonstr.equals("")){
                try {

                    JSONObject jsonObject=new JSONObject(jsonstr);
                    result=jsonObject.getString("Message");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("otp"+result);
            dialog();

        }
    }


    public void dialog(){
        final EditText otpet;
        Button ok,cancel;
        final Dialog otp=new Dialog(RegisterActivity.this);
        otp.requestWindowFeature(Window.FEATURE_NO_TITLE);
        otp.setContentView(R.layout.otpdialog);

        otpet=(EditText)otp.findViewById(R.id.otpet);

        ok=(Button)otp.findViewById(R.id.otpok);
        cancel=(Button)otp.findViewById(R.id.otpcancel);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otpstring=otpet.getText().toString();
                if(result!=null && result.equals(otpstring)){
                    if(sRole.equals("6")){
                        new UserRegistration().execute();
                    }else {
                        new  AgentRegistration().execute();
                    }
                    //    new  AgentRegistration().execute();
                    //    new UserRegistration().execute();

                    otp.dismiss();

                }else{
                    otpet.setError("OTP Incorrect");
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp.dismiss();
            }
        });
        otp.show();
    }



    public class UserRegistration extends AsyncTask<String,String,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog("Please Wait",RegisterActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {


            JSONObject bcparam = new JSONObject();
            // List<NameValuePair> parm=new ArrayList<NameValuePair>();
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Constants.BUSAPI+ Constants.userRegistration);
            httpPost.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpPost.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            try {
                bcparam.put("AlternateEmail", "");
                bcparam.put("DeviceModel", "LG Nexus 5");
                bcparam.put("Pincode", "500000");
                bcparam.put("ParentId",parentid );
                bcparam.put("AlternateMobile", "");
                bcparam.put("LoginType", "Website");
                bcparam.put("Password", pwd);
                bcparam.put("ActivationCode", "");
                bcparam.put("City", "hyd");
                bcparam.put("FirstName", fname);
                bcparam.put("LastName", lname);
                bcparam.put("Gender", "1");
                bcparam.put("Address", "hyd");
                bcparam.put("ApplicationType", "Mobile");
                bcparam.put("Mobile", mobile);
                bcparam.put("Status", "1");
                bcparam.put("Email", mails);
                bcparam.put("IsSIteAdminAgent", "0");
                bcparam.put("DeviceOS", "Android");
                bcparam.put("DeviceToken", "21455566g565g656hghhghhs");
                bcparam.put("State", "1");
                bcparam.put("DOB", "1-1-1990");
                bcparam.put("Telephone", "");
                bcparam.put("DeviceOSVersion", "1.1.1");

                System.out.println("Json Object :"+bcparam);
                try {
                    StringEntity seEntity;
                    // String data1 = null;
				/*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                    seEntity = new StringEntity(bcparam.toString(), "UTF_8");
                    seEntity.setContentType("application/json");
                    httpPost.setEntity(seEntity);
                    HttpResponse httpResponse;

                    httpResponse = httpClient.execute(httpPost);

                    inputStream = httpResponse.getEntity().getContent();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream));
            String line = "";
            data = "";
            try {
                while ((line = bufferedReader.readLine()) != null)
                    data += line;
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                inputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

        System.out.println("Responce :"+data);

            }
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgressDialog();
            if(data.contains("Success")){
                // new  GetOtp().execute();
                editor.putString("Name", fname);
                editor.putString("Email",mails);

                editor.commit();
                Toast.makeText(RegisterActivity.this,"Good News \nSuccessfully Registerd",Toast.LENGTH_SHORT).show();
                Intent i=new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(i);
                System.out.println("Useremail"+mails+"pasword"+pwd);
                System.out.println("Userregestration1"+data);
            }else {
                System.out.println("Userpsw"+pwd);
                System.out.println("Userregestration"+data);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterActivity.this);

                alertDialogBuilder
                        .setMessage("This Email has been already registered \n  ")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, close
                                // current activity

                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            //    Toast.makeText(RegisterActivity.this,""+data,Toast.LENGTH_SHORT).show();
            }
        }
    }



    public class AgentRegistration extends AsyncTask<String,String,String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog("Please Wait",RegisterActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {


            JSONObject bcparam = new JSONObject();
            // List<NameValuePair> parm=new ArrayList<NameValuePair>();
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Constants.BUSAPI+ Constants.AgentRegistration);
            httpPost.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpPost.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            try {
                bcparam.put("IsSIteAdminAgent", "0");
                bcparam.put("FirstName", fname);
                bcparam.put("LastName", lname);
                bcparam.put("Password", pwd);
                bcparam.put("Email", mails);
                bcparam.put("Mobile", mobile);
                bcparam.put("Gender", "1");
                bcparam.put("DOB", "1-1-1990");
                bcparam.put("AlternateEmail", "");
                bcparam.put("AlternateMobile", "");
                bcparam.put("Telephone", "");
                bcparam.put("Address", "hyd");
                bcparam.put("City", "hyd");
                bcparam.put("State", "1");
                bcparam.put("Pincode", "500000");
                bcparam.put("Status", "0");
                bcparam.put("ActivationCode", "");
                bcparam.put("DeviceModel", "LG Nexus 5");
                bcparam.put("DeviceOS", "Android");
                bcparam.put("DeviceOSVersion", "1.1.1");
                bcparam.put("DeviceToken", "");
                bcparam.put("IsAgent", "4");
                bcparam.put("ParentId", parentid);
                bcparam.put("CompanyName", "Unknown");


                try {
                    StringEntity seEntity;
                    // String data1 = null;
				/*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                    seEntity = new StringEntity(bcparam.toString(), "UTF_8");
                    seEntity.setContentType("application/json");
                    httpPost.setEntity(seEntity);
                    HttpResponse httpResponse;

                    httpResponse = httpClient.execute(httpPost);

                    inputStream = httpResponse.getEntity().getContent();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream));
            String line = "";
            data = "";
            try {
                while ((line = bufferedReader.readLine()) != null)
                    data += line;
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                inputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            hideProgressDialog();
            if(data.contains("Success")){
                //     new  GetOtp().execute();
                Intent i=new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(i);
                System.out.println("Agentemail"+mails+"password"+pwd);
                System.out.println("Agentregestration1"+data);
            }else {
                System.out.println("Agentpwd"+pwd);
                System.out.println("Agentregestration"+data);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterActivity.this);

                // set title
                //  alertDialogBuilder.setTitle("Your Title");

                // set dialog message
                alertDialogBuilder
                        .setMessage("This Email has been already registered \n  ")
                        .setCancelable(false)
                        .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, close
                                // current activity

                                dialog.cancel();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
              //  Toast.makeText(RegisterActivity.this,""+data,Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class Getparentid extends AsyncTask<String,String,String> {

        @Override
        protected String doInBackground(String... params) {
            ServiceHandler sh = new ServiceHandler();
            String jsonstr = sh.makeServiceCall(Constants.BUSAPI + Constants.AgentList, ServiceHandler.GET);
            if (jsonstr != "") {
                try {

                    JSONArray jarray = new JSONArray(jsonstr);
                    for (int i = 0; i < jarray.length(); i++) {
                        JSONObject jsonObject = jarray.getJSONObject(i);
                        parentid = jsonObject.getString("UserId");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("parentid" + parentid);
            new GetOtp().execute();
            //  new AgentRegistration().execute();
        }
    }
    private  void showProgressDialog(String msg,Context context) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setMessage(msg);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }
}
