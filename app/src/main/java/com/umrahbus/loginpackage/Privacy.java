package com.umrahbus.loginpackage;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.umrahbus.ConnectionStatus.ConnectivityReceiver;
import com.umrahbus.Constants;
import com.umrahbus.ServicesClasses.ServiceHandler;
import com.umrahbus.R;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by jagadeesh on 3/23/2017.
 */
public class Privacy extends AppCompatActivity {

    WebView mwebview;
    String data;
    String Content;
    ProgressDialog progressdialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_privacy);
        checkConnection();
        new GetPrivacy().execute();
        mwebview= (WebView) findViewById(R.id.webView);
        mwebview.getSettings().setJavaScriptEnabled(true);

    }



    private class GetPrivacy extends AsyncTask<String,String,String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressdialog=ProgressDialog.show(Privacy.this," ","Please Wait");

        }

        @Override
        protected String doInBackground(String... params) {

            String strurl = Constants.BUSAPI+ Constants.Accounts;
            DefaultHttpClient httpClient = new DefaultHttpClient();

            ServiceHandler sh = new ServiceHandler();
            data = sh.makeServiceCall(strurl, ServiceHandler.GET);
            System.out.println("JSON LENGTH" + strurl);

            try {
                JSONArray array=new JSONArray(data);

                for (int i=0;i<array.length();i++){

                    JSONObject object=array.getJSONObject(i);

                    String page=object.getString("Page");

                    if (page.equals("4")){
                        Content=object.getString("ContentDescription");

                    }
                }





            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;


        }



        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressdialog.dismiss();

            mwebview.loadDataWithBaseURL("",Content,"text/html", "UTF-8", "");

        }
    }
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set title
            //  alertDialogBuilder.setTitle("Your Title");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Sorry! Not connected to internet")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity

                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }
}
