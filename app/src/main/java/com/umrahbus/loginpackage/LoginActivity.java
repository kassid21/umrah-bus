package com.umrahbus.loginpackage;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.umrahbus.BeanClasses.AgentDetailsBean;
import com.umrahbus.Constants;
import com.umrahbus.MainActivity;
import com.umrahbus.ServicesClasses.ServiceHandler;
import com.umrahbus.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.UUID;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity  {

    Button  user, agent, loginbutton;
    TextView  newuser, forgotpwd;
    EditText username, password;
    EditText regmail,regmob;
    Button fsubmit,forgotagent,forgotuser;
    String usernameString, passwordString, data,forgotemail,forgotmobile,parentid;
    View  logininc,view;
    String opname, srole="", agentid="",newpasswor="";
    Dialog agentpaymentDialog,forgotpassDialog,logindialog;
    Boolean user_agent = true,dialogwallet=false,paymentinfo=false;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    InputStream inputStream;
    NavigationView navigationView;

    String agentbal,User_agentid;
    TextView clientname,clientbal;

    AgentDetailsBean agent_userbean;
    ArrayList<AgentDetailsBean> beanArrayList;


    ProgressDialog progressDialog;
    String user_Login ,Agent_Login;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_login);


        pref = getSharedPreferences("LoginPreference", MODE_PRIVATE);
        editor = pref.edit();

        user = (Button) findViewById(R.id.User_log);
        agent = (Button) findViewById(R.id.Agent_log);
        newuser = (TextView) findViewById(R.id.Register);

        username = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);
        // Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();

        loginbutton = (Button)findViewById(R.id.email_sign_in_button);
        forgotpwd=(TextView)findViewById(R.id.Forgot);


        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {

                    usernameString = username.getText().toString();
                    passwordString = password.getText().toString();
                    if (user_agent == true) {
                        srole = "6";
                        if ((!usernameString.equals("")) && (!passwordString.equals(""))) {
                            new Userlogin().execute();
                        } else {
                            if ((usernameString.equals("")) || (passwordString.equals(""))) {
                                if (usernameString.equals("")) {
                                    username.setError("Enter Username");
                                }
                                if (passwordString.equals("")) {
                                    password.setError("Enter Password");
                                }
                            } else {
                                new Userlogin().execute();
                            }
                        }
                    } else if (user_agent == false) {

                        srole = "4";
                        if ((!usernameString.equals("")) && (!passwordString.equals(""))) {
                            new AgentLogin().execute();
                        } else {
                            if ((usernameString.equals("")) || (passwordString.equals(""))) {
                                if (usernameString.equals("")) {
                                    username.setError("Enter Username");
                                }
                                if (passwordString.equals("")) {
                                    password.setError("Enter Password");
                                }
                            } else {
                                new AgentLogin().execute();
                            }
                        }


                    }
                }

            }
        });
        //   logininc.setVisibility(View.GONE);
        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                user.setTextColor(getResources().getColor(R.color.colorWhite));
                agent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                agent.setTextColor(getResources().getColor(R.color.colorPrimary));

                username.setText("");
                password.setText("");
                user_agent = true;
            }
        });
        agent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agent.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                agent.setTextColor(getResources().getColor(R.color.colorWhite));
                user.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                user.setTextColor(getResources().getColor(R.color.colorPrimary));

                username.setText("");
                password.setText("");
                user_agent = false;
            }
        });
        newuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });
        forgotpwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uuid = UUID.randomUUID().toString().replaceAll("-", "");
                newpasswor=uuid.substring(0,6);
                Log.e("NewPassword",newpasswor);
                forgotpassDialog=new Dialog(LoginActivity.this);
                forgotpassDialog.setTitle("Please Enter Credentials ");
                forgotpassDialog.setContentView(R.layout.forgotpassword);

                regmail=(EditText)forgotpassDialog.findViewById(R.id.fetemail);
                regmob=(EditText)forgotpassDialog.findViewById(R.id.fetmobile);
                fsubmit=(Button)forgotpassDialog.findViewById(R.id.fbsub);
                forgotagent=(Button)forgotpassDialog.findViewById(R.id.agentforgotdialog);
                forgotuser=(Button)forgotpassDialog.findViewById(R.id.userforgotdialog);

                forgotagent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                forgotagent.setTextColor(getResources().getColor(R.color.colorPrimary));
                forgotuser.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                forgotuser.setTextColor(getResources().getColor(R.color.colorWhite));
                user_agent = false;
                forgotuser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        forgotuser.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        forgotuser.setTextColor(getResources().getColor(R.color.colorWhite));
                        forgotagent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                        forgotagent.setTextColor(getResources().getColor(R.color.colorPrimary));
                        user_agent = true;
                    }
                });
                forgotagent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        forgotagent.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        forgotagent.setTextColor(getResources().getColor(R.color.colorWhite));
                        forgotuser.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                        forgotuser.setTextColor(getResources().getColor(R.color.colorPrimary));
                        user_agent = false;
                    }
                });




                fsubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        forgotemail = regmail.getText().toString();
                        forgotmobile = regmob.getText().toString();

                        int length=forgotmobile.length();

                        if (!forgotemail.equals("") && !forgotmobile.equals("")&& length == 9) {

                            new ForgotPasswordasy().execute();
                        } else {
                            try {
                                if (forgotmobile.equals("")) {
                                    regmob.setError("Enter Registered Mobile");
                                }
                                if (forgotemail.equals("")) {
                                    regmail.setError("Please Enter Registered Mail");
                                } if (forgotmobile.length() < 9) {
                                    regmob.setError("Enter Valid Mobile Number");
                                }
                                else {
                                    new ForgotPasswordasy().execute();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                forgotpassDialog.show();

            }
        });
    }
    ////User Login Functionality////////////////
    public class Userlogin extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog=ProgressDialog.show(LoginActivity.this,"","Please Wait..\nGood things take time :)");
        }
        @Override
        protected String doInBackground(String... params) {
            {


                // List<NameValuePair> bcparam=new ArrayList<NameValuePair>();
                JSONObject bcparam = new JSONObject();
                // List<NameValuePair> parm=new ArrayList<NameValuePair>();
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.BUSAPI+ Constants.userLogin);
                httpPost.setHeader("ConsumerKey", Constants.ConsumerKey);
                httpPost.setHeader("ConsumerSecret", Constants.ConsumerSecret);
                try {
                    bcparam.put("Username", usernameString);
                    bcparam.put("Password", passwordString);
                    bcparam.put("Client", null);

                    bcparam.put("IsAgentfromPSALogin", false);
                    bcparam.put("IsWebApiAgentLogin", false);


                    // httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                    // parm.add(new BasicNameValuePair("res",bcparam.toString()));
                    try {
                        StringEntity seEntity;
                        // String data1 = null;
				/*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                        seEntity = new StringEntity(bcparam.toString(), "UTF_8");
                        seEntity.setContentType("application/json");
                        httpPost.setEntity(seEntity);
                        HttpResponse httpResponse;

                        httpResponse = httpClient.execute(httpPost);

                        inputStream = httpResponse.getEntity().getContent();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //json=bcparam.toString();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream));
                String line = "";
                data = "";
                try {
                    while ((line = bufferedReader.readLine()) != null)
                        data += line;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    inputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                Log.v("rsponse", "" + data);


                return data;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(data);
                String message = jsonObject.getString("Message");
                if (message.contains("Success")) {
                    editor.putString("username", usernameString);
                    editor.putString("passward", passwordString);

                    editor.commit();

                    new UserDetails().execute();
                    //updateSpinner();
                  /*  try {
                        navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
                        navigationView.getMenu().findItem(R.id.nav_Login).setVisible(false);
                        MenuItem item = navigationView.getMenu().getItem(2);
                        item.setVisible(false);
                        MenuItem item1 = navigationView.getMenu().getItem(7);
                        item1.setVisible(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/
                /*    view.setVisibility(View.VISIBLE);
                    //  view1.setVisibility(View.VISIBLE);
                    logininc.setVisibility(View.GONE);*/

                }else{
                    error(message);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class UserDetails extends AsyncTask<String, String, String> {


        String result;

        @Override
        protected String doInBackground(String... params) {


            ServiceHandler sh = new ServiceHandler();
            String jsonstr = sh.makeServiceCall(Constants.BUSAPI+ Constants.userDetails+usernameString, ServiceHandler.GET);
            if (jsonstr != "") {

                try {


                    agent_userbean = new AgentDetailsBean();
                    beanArrayList = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(jsonstr);

                    agent_userbean.setIsSIteAdminAgent(jsonObject.getString("IsSIteAdminAgent"));
                    agent_userbean.setUserId(jsonObject.getString("UserId"));
                    agent_userbean.setClientId(jsonObject.getString("ClientId"));
                    agent_userbean.setParentId(jsonObject.getString("ParentId"));
                    agent_userbean.setClientName(jsonObject.getString("ClientName"));
                    agent_userbean.setFirstName(jsonObject.getString("FirstName"));
                    agent_userbean.setLastName(jsonObject.getString("LastName"));
                    agent_userbean.setPassword(jsonObject.getString("Password"));
                    agent_userbean.setGender(jsonObject.getString("Gender"));
                    agent_userbean.setDOB(jsonObject.getString("DOB"));
                    agent_userbean.setEmail(jsonObject.getString("Email"));
                    agent_userbean.setAlternateEmail(jsonObject.getString("AlternateEmail"));
                    agent_userbean.setMobile(jsonObject.getString("Mobile"));
                    agent_userbean.setAlternateMobile(jsonObject.getString("AlternateMobile"));
                    agent_userbean.setTelephone(jsonObject.getString("Telephone"));
                    agent_userbean.setAddress(jsonObject.getString("Address"));
                    agent_userbean.setCity(jsonObject.getString("City"));
                    agent_userbean.setState(jsonObject.getString("State"));
                    agent_userbean.setPincode(jsonObject.getString("Pincode"));
                    agent_userbean.setStatus(jsonObject.getString("Status"));
                    agent_userbean.setActivationCode(jsonObject.getString("ActivationCode"));
                    agent_userbean.setDeviceModel(jsonObject.getString("DeviceModel"));
                    agent_userbean.setDeviceOS(jsonObject.getString("DeviceOS"));
                    agent_userbean.setDeviceToken(jsonObject.getString("LoginType"));
                    agent_userbean.setDeviceOSVersion(jsonObject.getString("DeviceOSVersion"));
                    agent_userbean.setDeviceToken(jsonObject.getString("DeviceToken"));



                    beanArrayList.add(agent_userbean);
                    User_agentid = agent_userbean.getUserId();
                    editor.putString("Clientname",agent_userbean.getFirstName()+agent_userbean.getLastName());
                    editor.putString("Emailid",agent_userbean.getEmail());
                    editor.commit();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            user_Login="Yes";
            editor.putString("UserLOGIN",user_Login);
            editor.putString("User_agentid",User_agentid);
            editor.commit();


            Intent j=new Intent(LoginActivity.this,MainActivity.class);
            Toast.makeText(getApplicationContext(), "Logged in successfully", Toast.LENGTH_LONG).show();
            startActivity(j);
        }
    }


    ///////AgentLogin functionality///////////////////////
    public class AgentLogin extends AsyncTask<String, String, String> {
        ProgressDialog agentdialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            agentdialog=ProgressDialog.show(LoginActivity.this,"","Please Wait");
        }

        @Override
        protected String doInBackground(String... params) {
            {


                // List<NameValuePair> bcparam=new ArrayList<NameValuePair>();
                JSONObject bcparam = new JSONObject();
                // List<NameValuePair> parm=new ArrayList<NameValuePair>();
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.BUSAPI+ Constants.AgentLogin);
                httpPost.setHeader("ConsumerKey", Constants.ConsumerKey);
                httpPost.setHeader("ConsumerSecret", Constants.ConsumerSecret);
                try {
                    bcparam.put("Username", usernameString);
                    bcparam.put("Password", passwordString);


                    // httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                    // parm.add(new BasicNameValuePair("res",bcparam.toString()));
                    try {
                        StringEntity seEntity;
                        // String data1 = null;
				/*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                        seEntity = new StringEntity(bcparam.toString(), "UTF_8");
                        seEntity.setContentType("application/json");
                        httpPost.setEntity(seEntity);
                        HttpResponse httpResponse;

                        httpResponse = httpClient.execute(httpPost);

                        inputStream = httpResponse.getEntity().getContent();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //json=bcparam.toString();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream));
                String line = "";
                data = "";
                try {
                    while ((line = bufferedReader.readLine()) != null)
                        data += line;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    inputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                Log.v("rsponse", "" + data);


                return data;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            agentdialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(data);
                String message = jsonObject.getString("Message");
                if (message.contains("Success")&&!data.equals("")) {
                    editor.putString("username", usernameString);
                    editor.putString("passward", passwordString);
                    editor.putBoolean("ISLOGIN",true);
                    editor.commit();
                    new AgentDetails().execute();

                    //  updateSpinner();
                   /* try {
                        MenuItem item = navigationView.getMenu().getItem(1);
                        item.setVisible(false);
                        MenuItem item1 = navigationView.getMenu().getItem(5);
                        item1.setVisible(true);
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/
                   /* view.setVisibility(View.VISIBLE);
                    //  view1.setVisibility(View.VISIBLE);
                    logininc.setVisibility(View.GONE);*/
                }
                else {
                    Toast.makeText(LoginActivity.this,"Please try again - "+message,Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                Toast.makeText(LoginActivity.this,"The Response from the service is null",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }
    public class AgentDetails extends AsyncTask<String, String, String> {


        String result;

        @Override
        protected String doInBackground(String... params) {


            ServiceHandler sh = new ServiceHandler();
            String jsonstr = sh.makeServiceCall(Constants.BUSAPI+ Constants.AgentDetails+usernameString, ServiceHandler.GET);
            if (jsonstr != "") {

                try {

                    agent_userbean = new AgentDetailsBean();
                    beanArrayList = new ArrayList<AgentDetailsBean>();
                    JSONObject jsonObject = new JSONObject(jsonstr);

                    agent_userbean.setStatus(jsonObject.getString("Status"));
                    agent_userbean.setBalance(jsonObject.getString("Balance"));
                    agent_userbean.setVirtualBalance(jsonObject.getString("VirtualBalance"));
                    agent_userbean.setBalanceAlert(jsonObject.getString("BalanceAlert"));
                    agent_userbean.setIsAgent(jsonObject.getString("IsAgent"));
                    agent_userbean.setCompanyName(jsonObject.getString("CompanyName"));
                    agent_userbean.setPAN(jsonObject.getString("PAN"));
                    agent_userbean.setClientType(jsonObject.getString("ClientType"));
                    agent_userbean.setIsSIteAdminAgent(jsonObject.getString("IsSIteAdminAgent"));
                    agent_userbean.setUserId(jsonObject.getString("UserId"));
                    agent_userbean.setClientId(jsonObject.getString("ClientId"));
                    agent_userbean.setFirstName(jsonObject.getString("FirstName"));
                    agent_userbean.setLastName(jsonObject.getString("LastName"));
                    agent_userbean.setPassword(jsonObject.getString("Password"));
                    agent_userbean.setGender(jsonObject.getString("Gender"));
                    agent_userbean.setDOB(jsonObject.getString("DOB"));
                    agent_userbean.setEmail(jsonObject.getString("Email"));
                    agent_userbean.setAlternateEmail(jsonObject.getString("AlternateEmail"));
                    agent_userbean.setTelephone(jsonObject.getString("Telephone"));
                    agent_userbean.setMobile(jsonObject.getString("Mobile"));
                    agent_userbean.setAlternateMobile(jsonObject.getString("AlternateMobile"));
                    agent_userbean.setAddress(jsonObject.getString("Address"));
                    agent_userbean.setCity(jsonObject.getString("City"));
                    agent_userbean.setState(jsonObject.getString("State"));
                    agent_userbean.setPincode(jsonObject.getString("Pincode"));
                    agent_userbean.setActivationCode(jsonObject.getString("ActivationCode"));
                    agent_userbean.setDeviceModel(jsonObject.getString("DeviceModel"));
                    agent_userbean.setDeviceOS(jsonObject.getString("DeviceOS"));
                    agent_userbean.setDeviceOSVersion(jsonObject.getString("DeviceOSVersion"));
                    agent_userbean.setDeviceToken(jsonObject.getString("DeviceToken"));
                    JSONArray jarry = jsonObject.getJSONArray("Services");
                    String[] arr = new String[jarry.length()];

                    for (int k = 0; k < jarry.length(); k++) {
                        arr[k] = jarry.getString(k);
                        agent_userbean.setServices(arr);

                    }

                    beanArrayList.add(agent_userbean);
                    agentid = agent_userbean.getUserId();


                    editor.putString("Clientname",agent_userbean.getBalance());
                    editor.putString("Emailid",agent_userbean.getEmail());
                    editor.commit();


                    // clientname.setText(pref.getString("Clientname",""));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Agent_Login="Yes";
            editor.putString("AgentLOGIN",Agent_Login);
            editor.putString("User_agentid",agentid);
            editor.commit();

            agentbal=agent_userbean.getBalance();
            /*if(pref.getBoolean("ISLOGIN",false==true)) {
                agentbal();
            }*/
            new Decrypt().execute();

            Toast.makeText(getApplicationContext(), "Logged in successfully", Toast.LENGTH_LONG).show();
        }
    }

    public class Decrypt extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            ServiceHandler sh = new ServiceHandler();
            String jsonstr1 = sh.makeServiceCall(Constants.BUSAPI+ Constants.DecriptCode+agentid, ServiceHandler.GET);
            if (jsonstr1 != "") {
                try {
                    JSONObject joj = new JSONObject(jsonstr1);

                    String decryptcode = joj.getString("StatusCode");
                    agent_userbean.setUserId(joj.getString("Message"));
                    if (decryptcode.equals("200")) {

                        beanArrayList.add(agent_userbean);
                        agentid = agent_userbean.getUserId();
                        editor.putBoolean("ISLOGIN", true);
                        editor.commit();

                        if(pref.getBoolean("ISLOGIN", false)==true){
                            editor.putString("AgentId",agentid);
                            editor.commit();
/*
                            Intent i = getBaseContext().getPackageManager()
                                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);*/
                            Intent j=new Intent(LoginActivity.this,MainActivity.class);

                            startActivity(j);
                        }

                    }




                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return null;
        }
    }
    public  void agentbal(){
        clientname.setText(pref.getString("Clientname",""));
        clientbal.setText(" balance : "+agentbal);
    }
    ////Forgot Password in Login scree///////////////////
    public  class  ForgotPasswordasy extends AsyncTask<String,String,String> {
        ProgressDialog dilog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dilog=ProgressDialog.show(LoginActivity.this,"","Please wait we are generating your new password");
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject obj = new JSONObject();

            try {
                if(user_agent==true){
                    obj.put("UserType", "User");
                }else {
                    obj.put("UserType", "Agent");
                }


                obj.put("Email", forgotemail);
                obj.put("Mobile", forgotmobile);
                obj.put("NewPassword", newpasswor);


                HttpClient httpclient = new DefaultHttpClient();

                HttpPost method = new HttpPost(Constants.BUSAPI+ Constants.ForgotPassword);
                method.setHeader("ConsumerKey", Constants.ConsumerKey);
                method.setHeader("ConsumerSecret", Constants.ConsumerSecret);


                try {
                    StringEntity seEntity;
                    // String data1 = null;
				/*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                    seEntity = new StringEntity(obj.toString(), "UTF_8");
                    seEntity.setContentType("application/json");
                    method.setEntity(seEntity);
                    HttpResponse httpResponse;

                    httpResponse = httpclient.execute(method);

                    inputStream = httpResponse.getEntity().getContent();

                }catch (Exception e){
                    e.printStackTrace();
                }
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream));
                String line = "";
                data = "";
                while ((line = bufferedReader.readLine()) != null)
                    data += line;

                try {
                    inputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                Log.v("rsponse", "" + data);
                // HttpResponse response = httpclient.execute(method);
                // HttpEntity entity = response.getEntity();
                // if(entity != null){
                // return EntityUtils.toString(entity);
                // }
                // else{
                // return "No string.";


            }catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dilog.dismiss();
            try{
                JSONObject object=new JSONObject(data);
                String strr=object.getString("Message");
                if(strr.contains("Success")){
                    forgotpassDialog.dismiss();
                    newpassword();
                    Toast.makeText(LoginActivity.this,"Your New Password has been sent to your Registered MailId",Toast.LENGTH_LONG).show();
                }else {

                    Toast.makeText(LoginActivity.this,"Please Enter Registered Credentials ",Toast.LENGTH_LONG).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }
    public void  newpassword(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("We have sent your new password to your registered Email Thank You :)")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    public void  error(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Sorry Please Try Again :(" +message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}

