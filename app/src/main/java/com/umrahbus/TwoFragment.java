package com.umrahbus;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.umrahbus.AdapterClasses.CustomGridtAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jagadeesh on 8/4/2016.
 */
public class TwoFragment extends Fragment {

    public TwoFragment() {
        // Required empty public constructor
    }
    private CustomGridtAdapter mWorkerListAdapter;
    ProgressDialog pDialog;
    AlertDialog alert;
    Fragment fragment = null;
    String lati,longi,userid,rr;
    String BusImages;
    private GridView gridView;
    ArrayList<HashMap<String, String>> categoriesList = new ArrayList<HashMap<String, String>>();
    private static final String URL                   = "http://70.35.199.65/api/category";

    public static final String TAG = TwoFragment.class.getSimpleName();
    int sucess,workerid=0,i=0;
    /*
     * Define a request code to send to Google Play services
     * This code is returned in Activity.onActivityResult
     */
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;




    SharedPreferences preference;
    public static final String mypreference = "mypref";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView         = inflater.inflate(R.layout.categories, container, false);
        gridView              = (GridView) rootView.findViewById(R.id.gridcategory);
        mWorkerListAdapter    = new CustomGridtAdapter(getActivity(), categoriesList);

        gridView.setAdapter(mWorkerListAdapter);


        try {
            ConnectivityManager conMgr = (ConnectivityManager) getActivity()
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

            if (netInfo == null || !netInfo.isConnected()
                    || !netInfo.isAvailable()) {

                AlertDialog.Builder dialogs = new AlertDialog.Builder(getActivity());
                dialogs.setMessage("No Internet Found");
                dialogs.setCancelable(false);
                dialogs.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        alert.cancel();
                    }
                });
                alert = dialogs.create();
                alert.show();


            } else {
                try {
                     preference = getActivity().getSharedPreferences("JourneyDetails", Context.MODE_PRIVATE);

                    new GetWorkers().execute();
                } catch (Exception e) {
                    Log.d("Tag" + "Exception", e.toString());
                }

            }
        } catch (Exception e) {
            Log.d("Tag", e.toString());
        }
        return rootView;
    }
    private class GetWorkers extends AsyncTask<Void, Void, String>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(Void... arg0)
        {
            BusImages=preference.getString("BusImages", null);
            System.out.println("Service :"+BusImages);
            if (BusImages != null) {
                try {

                    JSONArray jsonarray = new JSONArray(BusImages);
                    for (int j = 0; j < jsonarray.length(); j++) {
                        JSONObject jsonobject = jsonarray.getJSONObject(j);
                        HashMap<String, String> map = new HashMap<String, String>();
                        // adding each child node to HashMap key => value
                        map.put("Catimage", jsonobject.getString("Imagepath"));

                        categoriesList.add(map);
                        mWorkerListAdapter.updateResults(categoriesList);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            pDialog.dismiss();




            }
        }
    }
