package com.umrahbus;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.umrahbus.BeanClasses.AvailableBusDetailsBean;
import com.umrahbus.ConnectionStatus.ConnectivityReceiver;
import com.umrahbus.ConnectionStatus.MyApplication;
import com.umrahbus.Enums.Months;
import com.umrahbus.ServicesClasses.ServiceHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class SearchActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    ProgressDialog mProgressDialog;
    RelativeLayout DateRelativelayout;
    private ListView AvailableListView;
    private MyAppAdapter AvailableBusAdapter;
    private ArrayList<AvailableBusDetailsBean> postArrayList;

    Button operator, time, price;
    TextView no_buses_found, Date_of_Journey;
    Boolean Operator = true, Time = true, Price = true;

    AvailableBusDetailsBean BusDetailsBean;
    JSONArray results;
    String Arrival_id, Destination_id, Journey_Date, DATE_OF_JOURNEY;
    String ProvidersCount, ResponceStatus, Message;

    ImageButton Increse_Date, Decrese_Date;
    String str;
    String Title_name,ReturnDateisTrue,IsReturnYes,ReturnTitle_name;
    String ISCLICKED="false";

    //OPERATOR ASCENDING?///////////
    private boolean mAscendingOrder[] = {true, true, true};
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String userLogin, agentLogin, SuperAdmin;
    String UserId, User_agentid;

    //////Filter//////
    String AC_slected, NonACSELECTED, SleeperSELECTED,NonSleeperSELECTED;
    String OperaterId;
    private ArrayList<String> BoardingArray = new ArrayList<>();
    ArrayList<String>DropingArray=new ArrayList<>();
    ArrayList<String>Operater=new ArrayList<>();

    List<String> Traveler;
    List<String> FilterBoadingId;
    List<String> FilterDropingId;
    String FROMNAME,TONAME;
    String Filter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seachlist);

        no_buses_found = (TextView) findViewById(R.id.No_buses_found);
        Date_of_Journey = (TextView) findViewById(R.id.date_of_journey);
        AvailableListView = (ListView) findViewById(R.id.search_list);
        DateRelativelayout =(RelativeLayout)findViewById(R.id.DateShowingRelativelayout);
        postArrayList = new ArrayList<>();


        pref = getSharedPreferences("LoginPreference", MODE_PRIVATE);
        editor = pref.edit();
        userLogin = pref.getString("UserLOGIN", null);
        agentLogin = pref.getString("AgentLOGIN", null);


        if (userLogin != null && userLogin.equals("Yes")) {
            UserId = "6";
            User_agentid = pref.getString("User_agentid", null);
        } else if (agentLogin != null && agentLogin.equals("Yes")) {
            UserId = "4";
            User_agentid = pref.getString("User_agentid", null);
        } else {
            UserId = "5";
            User_agentid = pref.getString("SuperAdmin", null);
        }
        SharedPreferences preference = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
        Arrival_id = preference.getString("ARRIVAL_ID", null);
        Destination_id = preference.getString("DESTINATION_ID", null);
        Journey_Date = preference.getString("JOURNEY_DATE", null);
        DATE_OF_JOURNEY = preference.getString("DAY_OF_JOURNEY", null);
        Title_name = preference.getString("TITLE_BAR", null);
        ReturnTitle_name = preference.getString("ReturnTripTITLE_BAR", null);
        ReturnDateisTrue = preference.getString("PassengerRETURN_DATE", null);
        IsReturnYes = preference.getString("IsReturnYes", null);
        FROMNAME = preference.getString("FROMNAME", null);
        TONAME = preference.getString("TONAME", null);


        SharedPreferences pref = getApplicationContext().getSharedPreferences("Filter", MODE_PRIVATE);
        Filter=pref.getString("FILTER",null);
        AC_slected = pref.getString("ACSELECTED", null);
        NonACSELECTED = pref.getString("NonACSELECTED", null);
        SleeperSELECTED = pref.getString("SleeperSELECTED", null);
        NonSleeperSELECTED = pref.getString("NonSleeperSELECTED", null);
        OperaterId = pref.getString("OperaterId", null);
        String Fboading = pref.getString("FilterBoadingId", null);
        String Fdroping = pref.getString("FilterDropingId", null);
        String oper = pref.getString("Filter_OperaterId", null);
        System.out.println("Boading" + Fboading + FilterBoadingId + FilterDropingId);

        if(oper!=null ||Fboading!=null ||Fdroping!=null){
            if(oper!=null){
                Traveler= Arrays.asList(oper.split(","));
            }if(Fboading!=null){
                FilterBoadingId=Arrays.asList(Fboading.split(","));
            }if(Fdroping!=null){
                FilterDropingId=Arrays.asList(Fdroping.split(","));
            }



            System.out.println("RESULT*****" + Traveler + FilterBoadingId + FilterDropingId);
        }



      //  new Availablebuses().execute();

        if(IsReturnYes.equals("Yes")){

            getSupportActionBar().setTitle(TONAME+" TO "+FROMNAME);

        }else{
            getSupportActionBar().setTitle(FROMNAME +" TO "+TONAME);

        }

        //   myAppAdapter = new MyAppAdapter(postArrayList,SearchActivity.this);
       //   new Availablebuses().execute();
      //  Toast.makeText(getApplicationContext(),"",Toast.LENGTH_SHORT).show();
        Date_of_Journey.setText(DATE_OF_JOURNEY);


        Increse_Date = (ImageButton) findViewById(R.id.arrow_right_date);
        Increse_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dt = Journey_Date;  // Start date
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Calendar c = Calendar.getInstance();
                try {
                    c.setTime(sdf.parse(dt));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c.add(Calendar.DATE, 1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
                Journey_Date = sdf1.format(c.getTime());
                int date = c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);
                String s = Journey_Date;
                StringTokenizer st = new StringTokenizer(s, "-");
                String Date = st.nextToken();


                for (Months dir : Months.values()) {
                    // do what you want
                    str = "" + Months.values()[month];
                }
                Date_of_Journey.setText("" + Date + " " + str + " " + year);
                Decrese_Date.setVisibility(View.VISIBLE);
                checkConnection();
                System.out.println("Date_increase" + Journey_Date + date + month + year + str);
            }
        });

        Decrese_Date = (ImageButton) findViewById(R.id.arrow_left_date);
        Calendar cl = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(cl.getTime());
        if(formattedDate.equals(Journey_Date)){
            Decrese_Date.setVisibility(View.INVISIBLE);
        }

        Decrese_Date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cl = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                String formattedDate = df.format(cl.getTime());
                System.out.println("Crrent_Date" + Journey_Date + formattedDate);
                if (formattedDate.equals(Journey_Date)) {
                    Decrese_Date.setVisibility(View.INVISIBLE);
                } else {
                    String dt = Journey_Date;  // Start date
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Calendar c = Calendar.getInstance();
                    try {
                        c.setTime(sdf.parse(dt));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    c.add(Calendar.DATE, -1);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
                    Journey_Date = sdf1.format(c.getTime());
                    int date = c.get(Calendar.DAY_OF_MONTH);
                    int month = c.get(Calendar.MONTH);
                    int year = c.get(Calendar.YEAR);
                    String s = Journey_Date;
                    StringTokenizer st = new StringTokenizer(s, "-");
                    String Date = st.nextToken();


                    for (Months dir : Months.values()) {
                        // do what you want
                        str = "" + Months.values()[month];
                    }
                    Date_of_Journey.setText("" + Date + " " + str + " " + year);
                    checkConnection();
                    System.out.println("Date_increase" + Journey_Date + date + month + year + str);


                    if (formattedDate.equals(Journey_Date)) {
                        Decrese_Date.setVisibility(View.INVISIBLE);
                    }
                }

            }
        });
        checkConnection();

      /*  operator = (Button) findViewById(R.id.Operator);
        operator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                price.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                // Change the order of items based on name
                if (mAscendingOrder[0]) {
                    // Show items descending
                    mAscendingOrder[0] = false;
                    AvailableBusAdapter.sortByNameAsc();
                    operator.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_top, 0);
                } else {
                    // Show items ascending
                    mAscendingOrder[0] = true;
                    AvailableBusAdapter.sortByNameDesc();
                    operator.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_bottom, 0);
                }

                mAscendingOrder[1] = true;
                mAscendingOrder[2] = true;


            }
        });
        time = (Button) findViewById(R.id.Time);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operator.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                price.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                // Change the order of items based on quantity
                if (mAscendingOrder[2]) {
                    // Show items descending
                    mAscendingOrder[2] = false;
                    AvailableBusAdapter.sortByTimeDesc();
                    time.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_top, 0);
                } else {
                    // Show items ascending
                    mAscendingOrder[2] = true;
                    AvailableBusAdapter.sortByTimeAsc();
                    time.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_bottom, 0);
                }

                mAscendingOrder[0] = false;
                mAscendingOrder[1] = true;


            }
        });
        price = (Button) findViewById(R.id.Price);
        price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                operator.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                if (mAscendingOrder[1]) {
                    // Show items descending
                    mAscendingOrder[1] = false;
                    AvailableBusAdapter.sortByPriceDesc();
                    price.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_top, 0);

                } else {
                    // Show items ascending
                    mAscendingOrder[1] = true;
                    AvailableBusAdapter.sortByPriceAsc();
                    price.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_bottom, 0);
                }

                mAscendingOrder[0] = false;
                mAscendingOrder[2] = true;

            }
        });*/



    }

    @Override
    public void onStart() {
        super.onStart();



    }


    public class MyAppAdapter extends BaseAdapter {

        private LayoutInflater vi;
        private ArrayList<AvailableBusDetailsBean> parkingList = new ArrayList<>();
        public Context context;

        /**
         * Add white line
         */
        public void addItem(AvailableBusDetailsBean item) {
            parkingList.add(item);
            //   notifyDataSetChanged();
        }

        /**
         * Sort Available Bus Details list by name ascending
         */
        public void sortByNameAsc() {
            Comparator<AvailableBusDetailsBean> comparator = new Comparator<AvailableBusDetailsBean>() {

                @Override
                public int compare(AvailableBusDetailsBean object1, AvailableBusDetailsBean object2) {
                    return object1.getDisplayName().compareToIgnoreCase(object2.getDisplayName());
                }
            };
            Collections.sort(parkingList, comparator);
            notifyDataSetChanged();
        }

        /**
         * Sort Available Bus Details list by name descending
         */
        public void sortByNameDesc() {
            Comparator<AvailableBusDetailsBean> comparator = new Comparator<AvailableBusDetailsBean>() {

                @Override
                public int compare(AvailableBusDetailsBean object1, AvailableBusDetailsBean object2) {
                    return object2.getDisplayName().compareToIgnoreCase(object1.getDisplayName());
                }
            };
            Collections.sort(parkingList, comparator);
            notifyDataSetChanged();
        }

        /**
         * Sort Available Bus Details list by Time ascending
         */
  /*      public void sortByTimeAsc() {
            Comparator<AvailableBusDetailsBean> comparator = new Comparator<AvailableBusDetailsBean>() {

                @Override
                public int compare(AvailableBusDetailsBean object1, AvailableBusDetailsBean object2) {
                    return object1.getDepartureTime().compareToIgnoreCase(object2.getDepartureTime());
                }
            };
            Collections.sort(parkingList, comparator);
            notifyDataSetChanged();
        }
*/


        public void sortByTimeAsc() {
            Comparator<AvailableBusDetailsBean> comparator = new Comparator<AvailableBusDetailsBean>() {

                @SuppressLint("SimpleDateFormat")
                @Override
                public int compare(AvailableBusDetailsBean object1, AvailableBusDetailsBean object2) {
                    try {
                        //  SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
                        //  System.out.println("Date :"+sdf.parse(object1.getDepartureTime()));
                        //   System.out.println(sdf.parse(object1.getDepartureTime()).before(sdf.parse(object2.getDepartureTime())));

                        return new SimpleDateFormat("hh:mm aa", Locale.ENGLISH).parse(object1.getDepartureTime()).compareTo(new SimpleDateFormat("hh:mm aa",Locale.ENGLISH).parse(object2.getDepartureTime()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        return 0;
                    }
                }
            };
            Collections.sort(parkingList, comparator);
            notifyDataSetChanged();


        }
        /**
         * Sort Available Bus Details list by Time descending
         */
        public void sortByTimeDesc() {
            Comparator<AvailableBusDetailsBean> comparator = new Comparator<AvailableBusDetailsBean>() {

                @Override
                public int compare(AvailableBusDetailsBean object1, AvailableBusDetailsBean object2) {
                    return object2.getDepartureTime().compareToIgnoreCase(object1.getDepartureTime());
                }
            };
            Collections.sort(parkingList, comparator);
            notifyDataSetChanged();
        }

        /**
         * Sort Available Bus Details list by price ascending
         */
        public void sortByPriceAsc() {
            Comparator<AvailableBusDetailsBean> comparator = new Comparator<AvailableBusDetailsBean>() {

                @Override
                public int compare(AvailableBusDetailsBean object1, AvailableBusDetailsBean object2) {
                    return object1.getNetFares().compareToIgnoreCase(object2.getNetFares());
                }
            };
            Collections.sort(parkingList, comparator);
            notifyDataSetChanged();
        }

        /**
         * Sort Available Bus Details list by price descending
         */
        public void sortByPriceDesc() {
            Comparator<AvailableBusDetailsBean> comparator = new Comparator<AvailableBusDetailsBean>() {

                @Override
                public int compare(AvailableBusDetailsBean object1, AvailableBusDetailsBean object2) {
                    return object2.getNetFares().compareToIgnoreCase(object1.getNetFares());
                }
            };
            Collections.sort(parkingList, comparator);
            notifyDataSetChanged();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;
            ViewHolder viewHolder;

            if (rowView == null) {
                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.listview_item, null);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.travels = (TextView) rowView.findViewById(R.id.Travels);
                viewHolder.busType = (TextView) rowView.findViewById(R.id.BusType);
                viewHolder.departureTime = (TextView) rowView.findViewById(R.id.DepartureTime);
                viewHolder.arrivalTime = (TextView) rowView.findViewById(R.id.ArrivalTime);
                viewHolder.available = (TextView) rowView.findViewById(R.id.BusSeats);
                viewHolder.netFares = (TextView) rowView.findViewById(R.id.NetFares);
                viewHolder.hotelName =(TextView) rowView.findViewById(R.id.UmrahHotel);
                viewHolder.MticketImage=(ImageView)rowView.findViewById(R.id.M_ticket_Icon);

                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            String fare=parkingList.get(position).getFares();
            String[] parts = fare.split("/");
            ArrayList<String>Fair=new ArrayList<>();
            for(int i=0; i<parts.length; i++){
                Double d = Double.parseDouble(parts[i]);
                int roundValue = (int) Math.round(d);
                Fair.add(String.valueOf(roundValue));
            }
          String  listString= TextUtils.join("/",Fair);

            viewHolder.travels.setText("" + parkingList.get(position).getDisplayName());
            viewHolder.busType.setText("" + parkingList.get(position).getBusType());
            viewHolder.departureTime.setText("" + parkingList.get(position).getDepartureTime());
            viewHolder.arrivalTime.setText("" + parkingList.get(position).getArrivalTime());
            viewHolder.available.setText(" " + parkingList.get(position).getAvailableSeats() + " seats ");
            viewHolder.netFares.setText(listString+ " SAR"+"");
            viewHolder.hotelName.setText("Hotel : "+parkingList.get(position).getHotelName());

            if(parkingList.get(position).getMticket().equals("True")){
                viewHolder.MticketImage.setVisibility(View.VISIBLE);

            }

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //    int postin =position;
                    JSONArray Boardingtimes = parkingList.get(position).getBoardingTimes();
                    JSONArray Dropingtimes = parkingList.get(position).getDropingTimes();
                    JSONArray BusImage =parkingList.get(position).getBusImages();
                    String BoardTimes = Boardingtimes.toString();
                    String DropingTimes = Dropingtimes.toString();
                    String Images =BusImage.toString();
                    String Operator = parkingList.get(position).getDisplayName();
                    String operater = Operator.replaceAll("\\s+", "");
                    String BusType = parkingList.get(position).getBusType();
                    String BusTypeId = parkingList.get(position).getId();
                    String departureTime = parkingList.get(position).getDepartureTime();
                    Intent i = new Intent(context, SelectSeatActivity.class);
                    SharedPreferences preference = context.getSharedPreferences("JourneyDetails", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preference.edit();
                    editor.putString("BoardingTimes", BoardTimes);
                    editor.putString("DroppingTimes", DropingTimes);
                    editor.putString("BusImages",Images);
                    editor.putString("Operator", operater);
                    editor.putString("DisplayName",Operator);
                    editor.putString("BusType", BusType);
                    editor.putString("BusTypeID", BusTypeId);
                    editor.putString("Main_Departure_time",departureTime);
                    editor.putString("departureTime", departureTime);
                    editor.putString("CancellationPolicy", parkingList.get(position).getCancellationPolicy());
                    editor.putString("PartialCancellationAllowed", parkingList.get(position).getPartialCancellationAllowed());
                    editor.putString("Provider", parkingList.get(position).getProvider());
                    editor.putString("JourneyDate", parkingList.get(position).getJourneydate());
                    editor.putString("ObiboAPIProvider", parkingList.get(position).getObiboAPIProvider());
                    editor.putString("Amenities", String.valueOf(parkingList.get(position).getAmenities()));
                    editor.putString("HotelName",String.valueOf(parkingList.get(position).getHotelName()));
                    editor.putString("IsPackage",String.valueOf(parkingList.get(position).getIspackage()));
                    editor.putBoolean("IsCOD",parkingList.get(position).isCOD());
                    System.out.println("ISCOD "+parkingList.get(position).isCOD());


                    editor.apply();
                    i.putExtra("BoardingTimes", BoardTimes);
                    i.putExtra("DropingTimes", DropingTimes);
                    System.out.println("Boarding" + parkingList.get(position).getObiboAPIProvider());


                    context.startActivity(i);
                }
            });
            return rowView;


        }

        private MyAppAdapter(ArrayList<AvailableBusDetailsBean> apps, Context context) {
            this.parkingList = apps;
            this.context = context;

        }

        @Override
        public int getCount() {
            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return parkingList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


    }

    public class ViewHolder {
        TextView travels, busType, departureTime, arrivalTime, available, netFares,hotelName;
        ImageView MticketImage;


    }


    public class Availablebuses extends AsyncTask<String, String, String> {

        String jsonstr;

        @Override
        protected void onPreExecute() {


            mProgressDialog = ProgressDialog.show(SearchActivity.this, "",
                    getResources().getString(R.string.app_name));
            mProgressDialog.setCancelable(false);
        }



        @Override
        protected String doInBackground(String... params) {

            String url;
            if(IsReturnYes.equals("Yes")){
                DateRelativelayout.setVisibility(View.GONE);
                getSupportActionBar().setTitle(ReturnTitle_name);
                url = Constants.BUSAPI + Constants.AvailableBuses + "sourceId=" +Arrival_id   + "&destinationId=" + Destination_id+ "&journeyDate=" + Journey_Date + "&tripType=2&userType=" + UserId + "&user=" + User_agentid+"&returnDate="+ReturnDateisTrue;

            }else{
                url = Constants.BUSAPI + Constants.AvailableBuses + "sourceId=" + Arrival_id + "&destinationId=" + Destination_id + "&journeyDate=" + Journey_Date + "&tripType=1&userType=" + UserId + "&user=" + User_agentid+"&returnDate="+ReturnDateisTrue;

            }


            System.out.println("url" + url);
            ServiceHandler sh = new ServiceHandler();
            jsonstr = sh.makeServiceCall(url, ServiceHandler.GET);
            System.out.println("JSON LENGTH" + jsonstr);

            if (jsonstr.length() != 0) {
                postArrayList.clear();

                try {


                    JSONObject jsonObject = new JSONObject(jsonstr);

                    results = jsonObject.getJSONArray("AvailableTrips");
                    if (results != null) {


                        //  final int numberOfItemsInResp = results.length();
                        for (int i = 0; i < results.length(); i++) {
                            BusDetailsBean = new AvailableBusDetailsBean();

                            JSONObject jobj = results.getJSONObject(i);

                            BusDetailsBean.setAmenities(jobj.getJSONArray("Amenities"));
                            BusDetailsBean.setBoardingTimes(jobj.getJSONArray("BoardingTimes"));
                            JSONArray jarry = jobj.getJSONArray("BoardingTimes");

                            System.out.println("BoardingTimes" + jarry);
                            for (int j = 0; j < jarry.length(); j++) {
                                JSONObject boardtime = jarry.getJSONObject(j);

                                BusDetailsBean.setBoardingAddress(boardtime.getString("Address"));
                                BusDetailsBean.setBoardingContactNumbers(boardtime.getString("ContactNumbers"));
                                BusDetailsBean.setBoardingContactPersons(boardtime.getString("ContactPersons"));
                                BusDetailsBean.setBoardingPointId(boardtime.getString("PointId"));
                                BusDetailsBean.setBoardingLandmark(boardtime.getString("Landmark"));
                                BusDetailsBean.setBoardingLocation(boardtime.getString("Location"));
                                BusDetailsBean.setBoardingName(boardtime.getString("Name"));
                                BusDetailsBean.setBoardingTime(boardtime.getString("Time"));

                                BoardingArray.add(boardtime.getString("PointId"));

                            }

                            BusDetailsBean.setDropingTimes(jobj.getJSONArray("DroppingTimes"));
                            JSONArray dispatch = jobj.getJSONArray("DroppingTimes");
                            System.out.println("DroppingTimes" + dispatch);
                            for (int k = 0; k < dispatch.length(); k++) {
                                JSONObject disptime = dispatch.getJSONObject(k);

                                BusDetailsBean.setDroppingAddress(disptime.getString("Address"));
                                BusDetailsBean.setDroppingContactNumbers(disptime.getString("ContactNumbers"));
                                BusDetailsBean.setDroppingContactPersons(disptime.getString("ContactPersons"));
                                BusDetailsBean.setDroppingPointId(disptime.getString("PointId"));
                                BusDetailsBean.setDroppingLandmark(disptime.getString("Landmark"));
                                BusDetailsBean.setDroppingLocation(disptime.getString("Location"));
                                BusDetailsBean.setDroppingName(disptime.getString("Name"));
                                BusDetailsBean.setDroppingTime(disptime.getString("Time"));

                                DropingArray.add(disptime.getString("PointId"));
                            }
                            BusDetailsBean.setAvailableSeats(jobj.getString("AvailableSeats"));
                            BusDetailsBean.setId(jobj.getString("Id"));
                            BusDetailsBean.setIspackage(jobj.getString("IsPackage"));
                            BusDetailsBean.setCOD(jobj.getBoolean("IsCOD"));
                            Operater.add(jobj.getString("Id"));
                            BusDetailsBean.setTravels(jobj.getString("Travels"));
                            BusDetailsBean.setDisplayName(jobj.getString("DisplayName"));
                            BusDetailsBean.setHotelName(jobj.getString("HotelName"));
                            BusDetailsBean.setBusType(jobj.getString("BusType"));
                            //  BusDetailsBean.setDepartureTime(jobj.getString("DepartureTime"));
                            String Dtime = jobj.getString("DepartureTime");
                            BusDetailsBean.setDepartureTime(Dtime);
                            String time = jobj.getString("ArrivalTime");
                            BusDetailsBean.setArrivalTime(time);
                            BusDetailsBean.setDuration(jobj.getString("Duration"));
                            BusDetailsBean.setCancellationPolicy(jobj.getString("CancellationPolicy"));
                            BusDetailsBean.setSourceId(jobj.getString("SourceId"));
                            BusDetailsBean.setDestinationId(jobj.getString("DestinationId"));
                            BusDetailsBean.setJourneydate(jobj.getString("Journeydate"));
                            BusDetailsBean.setFares(jobj.getString("Fares"));
                            BusDetailsBean.setServiceTax(jobj.getString("ServiceTax"));
                            BusDetailsBean.setOperatorServiceCharge(jobj.getString("OperatorServiceCharge"));
                            BusDetailsBean.setConvenienceFee(jobj.getString("ConvenienceFee"));
                            BusDetailsBean.setNetFares(jobj.getString("NetFares"));
                            BusDetailsBean.setProvider(jobj.getString("Provider"));
                            BusDetailsBean.setObiboAPIProvider(jobj.getString("ObiboAPIProvider"));
                            BusDetailsBean.setPartialCancellationAllowed(jobj.getString("PartialCancellationAllowed"));
                            BusDetailsBean.setSeatType(jobj.getString("SeatType"));
                            BusDetailsBean.setMticket(jobj.getString("Mticket"));
                            BusDetailsBean.setBusImages(jobj.getJSONArray("BusImages"));
                            //  BusDetailsBean.setAffiliateId(jobj.getString("AffiliateId"));
                            //here Filter is true
                            if(Filter!=null&&Filter.equals("True")) {

                                //here Traveller not equal to null
                                if(Traveler!=null&&FilterBoadingId!=null){
                                    if(Traveler.contains(jobj.getString("Travels"))){
                                        //here Boading point not equal to null
                                        if(FilterBoadingId!=null){
                                            /*for (int j = 0; j < jarry.length(); j++) {
                                                JSONObject boardtime = jarry.getJSONObject(j);*/
                                                if(FilterBoadingId.contains(BusDetailsBean.BoardingPointId)) {
                                                    // here droing point not equal to null
                                                    if (FilterBoadingId != null) {
                                                    /*    for (int k = 0; k < dispatch.length(); k++) {
                                                            JSONObject disptime = dispatch.getJSONObject(k);*/

                                                            //if (FilterBoadingId.contains(BusDetailsBean.BoardingPointId)) ;
                                                            if (AC_slected != null && AC_slected.equals("true")) {
                                                                if (jobj.getString("SeatType") .equals("1")||jobj.getString("SeatType").equals("2")) {
                                                                    postArrayList.add(BusDetailsBean);
                                                                }
                                                            } else if (NonACSELECTED != null && NonACSELECTED.equals("true")) {
                                                                if (jobj.getString("SeatType") .equals("3")||jobj.getString("SeatType").equals("4")) {
                                                                    postArrayList.add(BusDetailsBean);
                                                                }
                                                            } else if (SleeperSELECTED != null && SleeperSELECTED.equals("true")) {
                                                                if (jobj.getString("SeatType") .equals("2")||jobj.getString("SeatType") .equals("4")) {
                                                                    postArrayList.add(BusDetailsBean);
                                                                }
                                                            } else if (NonSleeperSELECTED != null && NonSleeperSELECTED.equals("true")) {
                                                                if (jobj.getString("SeatType") .equals("3")||jobj.getString("SeatType") .equals("1")) {
                                                                    postArrayList.add(BusDetailsBean);
                                                                }
                                                            }else{
                                                                postArrayList.add(BusDetailsBean);

                                                            }

                                                        }
                                                    }else{
                                                        postArrayList.add(BusDetailsBean);

                                                    }
                                               // }


                                            //}
                                        }else{
                                            postArrayList.add(BusDetailsBean);

                                        }
                                    }
                                }else  if(FilterBoadingId!=null){
                            /*        for (int j = 0; j < jarry.length(); j++) {
                                        JSONObject boardtime = jarry.getJSONObject(j);*/


                                        //here you can write the conditina for droping points checking
                                        if(FilterBoadingId.contains(BusDetailsBean.BoardingPointId)) {

                                            // here droing point not equal to null
                                            if (FilterBoadingId != null) {
                                               /* for (int k = 0; k < dispatch.length(); k++) {
                                                    JSONObject disptime = dispatch.getJSONObject(k);*/

                                                   /* if (FilterBoadingId.contains(disptime.getString("PointId"))) ;*/
                                                    if (AC_slected != null && AC_slected.equals("true")) {
                                                        if (jobj.getString("SeatType") .equals("1")||jobj.getString("SeatType").equals("2")) {
                                                            postArrayList.add(BusDetailsBean);
                                                        }
                                                    } else if (NonACSELECTED != null && NonACSELECTED.equals("true")) {
                                                        if (jobj.getString("SeatType") .equals("3")||jobj.getString("SeatType").equals("4")) {
                                                            postArrayList.add(BusDetailsBean);
                                                        }
                                                    } else if (SleeperSELECTED != null && SleeperSELECTED.equals("true")) {
                                                        if (jobj.getString("SeatType") .equals("2")||jobj.getString("SeatType") .equals("4")) {
                                                            postArrayList.add(BusDetailsBean);
                                                        }
                                                    } else if (NonSleeperSELECTED != null && NonSleeperSELECTED.equals("true")) {
                                                        if (jobj.getString("SeatType") .equals("3")||jobj.getString("SeatType") .equals("1")) {
                                                            postArrayList.add(BusDetailsBean);
                                                        }
                                                    }else{
                                                        postArrayList.add(BusDetailsBean);
                                                    }

                                                //}
                                            }else{
                                                postArrayList.add(BusDetailsBean);
                                            }
                                        //}

                                    }
                                }else  if(Traveler!=null){
                                   /* for (int j = 0; j <= jarry.length(); j++) {
                                        JSONObject boardtime = jarry.getJSONObject(j);*/

                                        //here you can write the conditina for droping points checking
                                        if(Traveler.contains(jobj.getString("Travels"))) {

                                            // here droing point not equal to null
                                            if (Traveler != null) {
                                            /*    for (int k = 0; k < dispatch.length(); k++) {
                                                    JSONObject disptime = dispatch.getJSONObject(k);*/

                                                    if (Traveler.contains(jobj.getString("Travels"))) ;
                                                    if (AC_slected != null && AC_slected.equals("true")) {
                                                        if (jobj.getString("SeatType") .equals("1")||jobj.getString("SeatType").equals("2")) {
                                                            postArrayList.add(BusDetailsBean);
                                                        }
                                                    } else if (NonACSELECTED != null && NonACSELECTED.equals("true")) {
                                                        if (jobj.getString("SeatType") .equals("3")||jobj.getString("SeatType").equals("4")) {
                                                            postArrayList.add(BusDetailsBean);
                                                        }
                                                    } else if (SleeperSELECTED != null && SleeperSELECTED.equals("true")) {
                                                        if (jobj.getString("SeatType") .equals("2")||jobj.getString("SeatType") .equals("4")) {
                                                            postArrayList.add(BusDetailsBean);
                                                        }
                                                    } else if (NonSleeperSELECTED != null && NonSleeperSELECTED.equals("true")) {
                                                        if (jobj.getString("SeatType") .equals("3")||jobj.getString("SeatType") .equals("1")) {
                                                            postArrayList.add(BusDetailsBean);
                                                        }
                                                    }else{
                                                        postArrayList.add(BusDetailsBean);
                                                    }

                                                //}
                                            }else{
                                                postArrayList.add(BusDetailsBean);
                                            }
                                        }

                                    //}
                                }
                                else  if(FilterDropingId!=null) {
                                    for (int k = 0; k < dispatch.length(); k++) {
                                        JSONObject disptime = dispatch.getJSONObject(k);

                                        if (FilterDropingId.contains(disptime.getString("PointId"))) {
                                            if (AC_slected != null && AC_slected.equals("true")) {
                                                if (jobj.getString("SeatType") .equals("1")||jobj.getString("SeatType").equals("2")) {
                                                    postArrayList.add(BusDetailsBean);
                                                }
                                            } else if (NonACSELECTED != null && NonACSELECTED.equals("true")) {
                                                if (jobj.getString("SeatType") .equals("3")||jobj.getString("SeatType").equals("4")) {
                                                    postArrayList.add(BusDetailsBean);
                                                }
                                            } else if (SleeperSELECTED != null && SleeperSELECTED.equals("true")) {
                                                if (jobj.getString("SeatType") .equals("2")||jobj.getString("SeatType") .equals("4")) {
                                                    postArrayList.add(BusDetailsBean);
                                                }
                                            } else if (NonSleeperSELECTED != null && NonSleeperSELECTED.equals("true")) {
                                                if (jobj.getString("SeatType") .equals("3")||jobj.getString("SeatType") .equals("1")) {
                                                    postArrayList.add(BusDetailsBean);
                                                }
                                            }
                                        }
                                    }
                                }else{
                                    if (AC_slected != null && AC_slected.equals("true")) {
                                            if (jobj.getString("SeatType") .equals("1")||jobj.getString("SeatType").equals("2")) {
                                                postArrayList.add(BusDetailsBean);
                                            }
                                        } else if (NonACSELECTED != null && NonACSELECTED.equals("true")) {
                                            if (jobj.getString("SeatType") .equals("3")||jobj.getString("SeatType").equals("4")) {
                                                postArrayList.add(BusDetailsBean);
                                            }
                                        } else if (SleeperSELECTED != null && SleeperSELECTED.equals("true")) {
                                            if (jobj.getString("SeatType") .equals("2")||jobj.getString("SeatType") .equals("4")) {
                                                postArrayList.add(BusDetailsBean);
                                            }
                                        } else if (NonSleeperSELECTED != null && NonSleeperSELECTED.equals("true")) {
                                            if (jobj.getString("SeatType") .equals("3")||jobj.getString("SeatType") .equals("1")) {
                                                postArrayList.add(BusDetailsBean);
                                            }
                                        }else{
                                            postArrayList.add(BusDetailsBean);
                                        }

                                }


                            }
                            else {
                                postArrayList.add(BusDetailsBean);
                            }




                            SharedPreferences preference = getApplicationContext().getSharedPreferences("Filter", MODE_PRIVATE);
                            SharedPreferences.Editor editor = preference.edit();
                            editor.putString("JsonResponce", jsonstr);
                            editor.commit();
                        }
                        ProvidersCount = jsonObject.getString("ProvidersCount");
                        ResponceStatus = jsonObject.getString("ResponseStatus");
                        Message = jsonObject.getString("Message");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }


            } else {
                no_buses_found.setVisibility(View.VISIBLE);
                mProgressDialog.dismiss();

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            mProgressDialog.dismiss();

            if (jsonstr.length() == 0) {
                no_buses_found.setVisibility(View.VISIBLE);
                mProgressDialog.dismiss();
                System.out.println("1111");

            } else if(Filter!=null&&Filter.equals("True")) {

                if (postArrayList.size() == 0) {
                    mProgressDialog.dismiss();
                    no_buses_found.setVisibility(View.VISIBLE);
                }else{
                    AvailableBusAdapter = new MyAppAdapter(postArrayList, SearchActivity.this);
                    // setListAdapter(myAppAdapter);
                    AvailableListView.setAdapter(AvailableBusAdapter);
                    AvailableBusAdapter.sortByTimeAsc();
                    mProgressDialog.dismiss();
                }
            }else if (results.length() != 0) {


                AvailableBusAdapter = new MyAppAdapter(postArrayList, SearchActivity.this);
                // setListAdapter(myAppAdapter);
                AvailableListView.setAdapter(AvailableBusAdapter);
                AvailableBusAdapter.sortByTimeAsc();
                mProgressDialog.dismiss();

            }else {
                mProgressDialog.dismiss();
                postArrayList.clear();
                AvailableListView.setAdapter(null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SearchActivity.
                        this);

                // set title
                alertDialogBuilder.setTitle("No Travels found!");

                // set dialog message
                alertDialogBuilder
                        .setMessage("This could have happened due to:\n" +
                                "1. No bus operating on this route or \n" +
                                "2. All seats for this route being sold out.\n" +
                                "Here is what you can do to get back on track...")
                        .setCancelable(false)
                        .setPositiveButton("Search Again",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                                Intent i=new Intent(SearchActivity.this, MainActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            Intent nextActivity = new Intent(this, FilterActivity.class);
            nextActivity.putExtra("JourneyDate",Journey_Date);
            startActivity(nextActivity);
            ISCLICKED="true";
            //slide from right to left
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*@Override
    public void onBackPressed() {
        SharedPreferences preference = getApplicationContext().getSharedPreferences("Filter", MODE_PRIVATE);
        preference.edit().remove("ACSELECTED").commit();
        preference.edit().remove("NonACSELECTED").commit();
        preference.edit().remove("SleeperSELECTED").commit();

    }*/
    // Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            new Availablebuses().execute();

        } else {


            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set title
            //  alertDialogBuilder.setTitle("Your Title");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Sorry! Not connected to internet")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity

                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected)
    {
        showSnack(isConnected);
    }
    //  finish();


}
