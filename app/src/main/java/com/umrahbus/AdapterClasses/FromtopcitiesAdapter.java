package com.umrahbus.AdapterClasses;

/**
 * Created by jagadeesh on 7/6/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.umrahbus.MainActivity;
import com.umrahbus.R;

import java.util.ArrayList;
import java.util.List;

public class FromtopcitiesAdapter extends BaseAdapter {

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<com.umrahbus.BeanClasses.TopCities> TopCities = null;
    private ArrayList<com.umrahbus.BeanClasses.TopCities> arraylist;

    public FromtopcitiesAdapter(Context context, List<com.umrahbus.BeanClasses.TopCities> topcity) {
        mContext = context;
        this.TopCities = topcity;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<com.umrahbus.BeanClasses.TopCities>();
        this.arraylist.addAll(topcity);
    }

    public class ViewHolder {
        TextView rank;
        TextView country;
        TextView population;
    }

    @Override
    public int getCount() {
        return TopCities.size();
    }

    @Override
    public com.umrahbus.BeanClasses.TopCities getItem(int position) {
        return TopCities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.availablelist_item, null);
            // Locate the TextViews in listview_item.xml
            holder.rank = (TextView) view.findViewById(R.id.location);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.rank.setText(TopCities.get(position).getTopCitiesName());


        // Listen for ListView Item Click
        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(mContext, MainActivity.class);
                // Pass all data rank
                intent.putExtra("rank", (TopCities.get(position).getTopCitiesId()));
                // Pass all data country
                SharedPreferences preference = mContext.getSharedPreferences("Sources", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preference.edit();
                editor.putString("FROMNAME",TopCities.get(position).getTopCitiesName());
                editor.putString("FROMID",TopCities.get(position).getTopCitiesId());
                editor.commit();

                // Start SingleItemView Class
                mContext.startActivity(intent);
            }
        });

        return view;
    }
}
