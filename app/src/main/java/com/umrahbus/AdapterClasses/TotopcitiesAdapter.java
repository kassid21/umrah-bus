package com.umrahbus.AdapterClasses;

/**
 * Created by jagadeesh on 7/6/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.umrahbus.BeanClasses.TopCities;
import com.umrahbus.MainActivity;
import com.umrahbus.R;

import java.util.ArrayList;
import java.util.List;

public class TotopcitiesAdapter extends BaseAdapter {

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<TopCities> TOPCITIES = null;
    private ArrayList<TopCities> arraylist;

    public TotopcitiesAdapter(Context context, List<TopCities> Topcity) {
        mContext = context;
        this.TOPCITIES = Topcity;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<TopCities>();
        this.arraylist.addAll(Topcity);
    }

    public class ViewHolder {
        TextView rank;

    }

    @Override
    public int getCount() {
        return TOPCITIES.size();
    }

    @Override
    public TopCities getItem(int position) {
        return TOPCITIES.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.availablelist_item, null);
            // Locate the TextViews in listview_item.xml
            holder.rank = (TextView) view.findViewById(R.id.location);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        // Set the results into TextViews
        holder.rank.setText(TOPCITIES.get(position).getTopCitiesName());


        // Listen for ListView Item Click
        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Send single item click data to SingleItemView Class
                Intent intent = new Intent(mContext, MainActivity.class);
                // Pass all data rank
                intent.putExtra("rank", (TOPCITIES.get(position).getTopCitiesId()));
                // Pass all data country
                SharedPreferences preference = mContext.getSharedPreferences("Sources", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preference.edit();
                editor.putString("TONAME",TOPCITIES.get(position).getTopCitiesName());
                editor.putString("TOID",TOPCITIES.get(position).getTopCitiesId());
                editor.commit();

                // Start SingleItemView Class
                mContext.startActivity(intent);
            }
        });

        return view;
    }
}