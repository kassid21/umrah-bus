package com.umrahbus.AdapterClasses;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.umrahbus.BeanClasses.GetPromoCodesDTO;
import com.umrahbus.draweritems.Offers;
import com.bumptech.glide.Glide;
import com.umrahbus.R;


/**
 * Created by garjunaNa on 07-Jun-17.
 */

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.MyViewHolder> {

    private GetPromoCodesDTO[] promoCodesList;
    private Context context;
    public OffersAdapter(GetPromoCodesDTO[] promoCodesList, Offers offersActivity) {
        this.promoCodesList=promoCodesList;
        context=offersActivity;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView promoNameTv, discountTv, validTv,promoTv;
        ImageView serviceIv;

        public MyViewHolder(View view) {
            super(view);
            promoNameTv = (TextView) view.findViewById(R.id.promoNameTv);
            discountTv = (TextView) view.findViewById(R.id.discountTv);
            validTv = (TextView) view.findViewById(R.id.validTv);
            promoTv = (TextView) view.findViewById(R.id.promoTv);
            serviceIv=(ImageView)view.findViewById(R.id.serviceIv);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offer_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        GetPromoCodesDTO offer = promoCodesList[position];

        int disType=offer.getDiscountType();


        if(disType==0){
            holder.discountTv.setText("Discount - "+RoundOff(Double.parseDouble(offer.getDiscount()))+" % off on Total fare");
        }else{
            holder.discountTv.setText("Discount - Flat "+RoundOff(Double.parseDouble(offer.getDiscount()))+" SAR off on Total fare");
        }
        holder.promoNameTv.setText(offer.getPromoName());
        holder.validTv.setText("Valid From "+offer.getValidFrom().split("T")[0]+" till "+offer.getValidTill().split("T")[0]);

        holder.promoTv.setText(offer.getCode());
        int featureImage = R.drawable.umrah_logo;
        int featureItem = offer.getServiceType();
        if(featureItem==1){
            featureImage = R.drawable.umrah_logo;
        }

        Glide.with(context)
                .load(featureImage)
                .placeholder(R.drawable.umrah_logo)
                .error(R.drawable.umrah_logo)
                .into(holder.serviceIv);
    }


    public Integer RoundOff(double TotalRate){
        double dAbs = Math.abs(TotalRate);
        int amt2 = (int) dAbs;
        double result = dAbs - (double) amt2;
        int finalrate;
        if (result<0.5){
            finalrate= (int) Math.floor(TotalRate);
        }else {
            finalrate= (int) Math.ceil(TotalRate);
        }

        return finalrate;
    }

    @Override
    public int getItemCount() {
        return promoCodesList.length;
    }
}
