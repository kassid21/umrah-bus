package com.umrahbus;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.umrahbus.BeanClasses.DropingPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class BoardingPointActivity extends AppCompatActivity {

    private ListView listView;
    String BoardingTimes;
    DropingPoint Dropingpoint;
    private DropingAdapter myAppAdapter;
    String Time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        listView = (ListView) findViewById(R.id.listView);
        SharedPreferences preference = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
        BoardingTimes=preference.getString("BoardingTimes", null);

        System.out.println("BoardingTimes"+BoardingTimes);

        ArrayList<DropingPoint> dropingPointArrayList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(BoardingTimes);
            System.out.println("Array****"+jsonArray);

            for (int j = 0; j < jsonArray.length(); j++) {
                JSONObject disptime = jsonArray.getJSONObject(j);
                Dropingpoint = new DropingPoint();
                Dropingpoint.setDroppingAddress(disptime.getString("Address"));
                Dropingpoint.setDroppingContactNumbers(disptime.getString("ContactNumbers"));
                Dropingpoint.setDroppingContactPersons(disptime.getString("ContactPersons"));
                Dropingpoint.setDroppingPointId(disptime.getString("PointId"));
                Dropingpoint.setDroppingLandmark(disptime.getString("Landmark"));
                Dropingpoint.setDroppingLocation(disptime.getString("Location"));
                Dropingpoint.setDroppingName(disptime.getString("Name"));

                int time= Integer.parseInt(disptime.getString("Time"));
                double  time2=time/60;
                DecimalFormat df = new DecimalFormat("##.#");
                int minuts = time % 60;
                String timestring= df.format(time2)+":"+minuts;
                final String Btime = timestring;

                try {
                    String _24HourTime = Btime;
                    SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
                    SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                    Date _24HourDt = _24HourSDF.parse(_24HourTime);
                    System.out.println(_24HourDt);
                    Time=_12HourSDF.format(_24HourDt);
                    Dropingpoint.setDroppingTime(Time);
                    System.out.println(Time);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dropingPointArrayList.add(Dropingpoint);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        myAppAdapter = new DropingAdapter(dropingPointArrayList, BoardingPointActivity.this);
        listView.setAdapter(myAppAdapter);


    }

    public class DropingAdapter extends BaseAdapter {

        public class ViewHolder {
            TextView txtTitle,txtSubTitle;


        }

        public List<DropingPoint> dropingList;

        public Context context;
        ArrayList<DropingPoint> arraylist;

        private DropingAdapter(List<DropingPoint> apps, Context context) {
            this.dropingList = apps;
            this.context = context;
            arraylist = new ArrayList<>();
            arraylist.addAll(dropingList);

        }

        @Override
        public int getCount() {
            return dropingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;
            ViewHolder viewHolder;

            if (rowView == null) {
                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.boadingpoint_list_item, null);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.txtTitle = (TextView) rowView.findViewById(R.id.boading_point);
                viewHolder.txtSubTitle = (TextView) rowView.findViewById(R.id.boading_time);
                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.txtTitle.setText(dropingList.get(position).getDroppingLocation() + "");
            viewHolder.txtSubTitle.setText(dropingList.get(position).getDroppingTime() + "");

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int postin =position;

                    Intent i = new Intent(context, DropinngPointActivity.class);
                    SharedPreferences preference = context.getSharedPreferences("OnwardJourneyDetails", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preference.edit();
                    editor.putString("DropingId",dropingList.get(position).getDroppingPointId());
                    editor.putString("DropingName",dropingList.get(position).getDroppingLocation());
                    editor.putString("ArraiavalTime",dropingList.get(position).getDroppingTime());

                    editor.apply();

                    context.startActivity(i);
                }
            });
            return rowView;


        }

        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            dropingList.clear();
            if (charText.length() == 0) {
                dropingList.addAll(arraylist);



            } else {
                for (DropingPoint postDetail : arraylist) {
                    if (charText.length() != 0 && postDetail.getDroppingLocation().toLowerCase(Locale.getDefault()).contains(charText)) {
                        dropingList.add(postDetail);


                    }

                   /* else if (charText.length() != 0 && postDetail.getPostSubTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                        parkingList.add(postDetail);
                    }*/
                }
            }
            notifyDataSetChanged();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(true);
        searchView.setQueryHint("Boarding Point");
        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.curser); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }
        //*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                myAppAdapter.filter(searchQuery.trim());
                listView.invalidate();
                return true;

            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
