package com.umrahbus;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;


public class SplashActivity extends Activity {

    Boolean first=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spleash_screen);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        /*Thread timerThread = new Thread() {
            public void run() {
                try {
                    if(first==false) {

                        sleep(5000);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    SharedPreferences preference = getApplicationContext().getSharedPreferences("Sources", MODE_PRIVATE);
                    preference.edit().remove("FROMNAME").apply();
                    preference.edit().remove("FROMID").apply();
                    preference.edit().remove("TONAME").apply();
                    preference.edit().remove("TOID").apply();
                    startActivity(intent);
                    finish();
                }
            }
        };
        timerThread.start();*/

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                SharedPreferences preference = getApplicationContext().getSharedPreferences("Sources", MODE_PRIVATE);
                preference.edit().remove("FROMNAME").apply();
                preference.edit().remove("FROMID").apply();
                preference.edit().remove("TONAME").apply();
                preference.edit().remove("TOID").apply();
                startActivity(intent);
                finish();
            }
        }, 5000);
    }
}
