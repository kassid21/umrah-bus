package com.umrahbus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by jagadeesh on 7/16/2016.
 */
public class FilterActivity extends AppCompatActivity {

    Button ACButton,Non_AC_Button,Sleeper_Button,NonSleeper_Button,Travels,filter_boarding_point,filter_droping_point;
    Button Applay,ClearFilter;
    String AC,NONAC,Sleeper,NonSleeper;
    String JourneyDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        ACButton=(Button)findViewById(R.id.AC_Button);
        Non_AC_Button=(Button)findViewById(R.id.Non_AcButton);
        Sleeper_Button=(Button)findViewById(R.id.Sleeper_Button);
        NonSleeper_Button=(Button)findViewById(R.id.Non_Sleeper);
        filter_boarding_point=(Button)findViewById(R.id.filter_boarding_point);
        Travels=(Button)findViewById(R.id.filter_Travels);
        /*filter_droping_point=(Button)findViewById(R.id.filter_droping_point);*/
        Applay=(Button)findViewById(R.id.Apply);
        ClearFilter=(Button)findViewById(R.id.Clear_filter);
        Intent ip=getIntent();
        JourneyDate=ip.getStringExtra("JourneyDate");

    }
    /** Called when the activity is about to become visible. */
    @Override
    protected void onStart() {
        super.onStart();

        ACButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ACButton.setTextColor(getResources().getColor(R.color.colorPrimary));
                Non_AC_Button.setTextColor(getResources().getColor(R.color.colorBlock));
                Sleeper_Button.setTextColor(getResources().getColor(R.color.colorBlock));
                NonSleeper_Button.setTextColor(getResources().getColor(R.color.colorBlock));
                AC="true";
                NONAC="false";
                Sleeper="false";
                NonSleeper="false";
            }
        });
        Non_AC_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Non_AC_Button.setTextColor(getResources().getColor(R.color.colorPrimary));
                ACButton.setTextColor(getResources().getColor(R.color.colorBlock));
                Sleeper_Button.setTextColor(getResources().getColor(R.color.colorBlock));
                NonSleeper_Button.setTextColor(getResources().getColor(R.color.colorBlock));
                NONAC="true";
                Sleeper="false";
                AC="false";
                NonSleeper="false";
            }
        });
        Sleeper_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Sleeper_Button.setTextColor(getResources().getColor(R.color.colorPrimary));
                ACButton.setTextColor(getResources().getColor(R.color.colorBlock));
                Non_AC_Button.setTextColor(getResources().getColor(R.color.colorBlock));
                NonSleeper_Button.setTextColor(getResources().getColor(R.color.colorBlock));
                Sleeper="true";
                NONAC="false";
                AC="false";
                NonSleeper="false";
            }
        });
        NonSleeper_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NonSleeper_Button.setTextColor(getResources().getColor(R.color.colorPrimary));
                Non_AC_Button.setTextColor(getResources().getColor(R.color.colorBlock));
                Sleeper_Button.setTextColor(getResources().getColor(R.color.colorBlock));
                ACButton.setTextColor(getResources().getColor(R.color.colorBlock));
                NonSleeper="true";
                AC="false";
                NONAC="false";
                Sleeper="false";
            }
        });
        Travels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(FilterActivity.this, FilterTravels.class);
                startActivity(i);
            }
        });
        filter_boarding_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(FilterActivity.this, FilterBoardingPoint.class);
                startActivity(i);
            }
        });
     /*   filter_droping_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(FilterActivity.this, FilterDropingpoint.class);
                startActivity(i);
            }
        });*/
        ClearFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(FilterActivity.this, SearchActivity.class);
                SharedPreferences prefer = getApplicationContext().getSharedPreferences("Filter", MODE_PRIVATE);
                prefer.edit().remove("ACSELECTED").commit();
                prefer.edit().remove("NonACSELECTED").commit();
                prefer.edit().remove("SleeperSELECTED").commit();
                prefer.edit().remove("NonSleeperSELECTED").commit();
                prefer.edit().remove("Filter_OperaterId").commit();
                prefer.edit().remove("Filter_OperaterName").commit();
                prefer.edit().remove("FilterBoadingId").commit();
                prefer.edit().remove("FilterBoadingLocation").commit();
                prefer.edit().remove("FilterDropingId").commit();
                prefer.edit().remove("FilterDropinglocation").commit();
                prefer.edit().remove("FILTER").commit();
                startActivity(i);
            }
        });
        Applay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String filter="True";
                Intent i=new Intent(FilterActivity.this, SearchActivity.class);
                SharedPreferences preference = getApplicationContext().getSharedPreferences("Filter", MODE_PRIVATE);
                SharedPreferences.Editor editor = preference.edit();
                editor.putString("FILTER",filter);
                editor.putString("ACSELECTED", AC);
                editor.putString("NonACSELECTED", NONAC);
                editor.putString("SleeperSELECTED", Sleeper);
                editor.putString("NonSleeperSELECTED", NonSleeper);
                editor.putString("JourneyDate",JourneyDate);
                System.out.println("ACCLicked"+AC+NONAC+NONAC);
                editor.commit();
                startActivity(i);
            }
        });
    }
    /** Called when the activity has become visible. */
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences preference = getApplicationContext().getSharedPreferences("Filter", MODE_PRIVATE);

        String ac=preference.getString("ACSELECTED",null);
        String nonac=preference.getString("NonACSELECTED",null);
        String sleeper=preference.getString("SleeperSELECTED",null);
        String nonsleeper=preference.getString("NonSleeperSELECTED",null);
        String FilterOpeName=preference.getString("Filter_OperaterName",null);
        String filterBlocation=preference.getString("FilterBoadingLocation",null);
        String filterDlocaation=preference.getString("FilterDropinglocation",null);

        if (ac != null) {
            if(ac.equals("true")){
                ACButton.setTextColor(getResources().getColor(R.color.colorPrimary));
                AC="true";
                NONAC="false";
                Sleeper="false";
                NonSleeper="false";
            }else if (nonac != null) {
                if(nonac.equals( "true")){
                    Non_AC_Button.setTextColor(getResources().getColor(R.color.colorPrimary));
                    NONAC="true";
                    Sleeper="false";
                    AC="false";
                    NonSleeper="false";
                }else if (sleeper != null) {
                    if(sleeper.equals( "true")){
                        Sleeper_Button.setTextColor(getResources().getColor(R.color.colorPrimary));
                        Sleeper="true";
                        NONAC="false";
                        AC="false";
                        NonSleeper="false";
                    }else if (nonsleeper != null && nonsleeper.equals("true")) {
                        NonSleeper_Button.setTextColor(getResources().getColor(R.color.colorPrimary));
                        NonSleeper="true";
                        AC="false";
                        NONAC="false";
                        Sleeper="false";

                    }
                }
            }
        }

     /*   if(FilterOpeName!=null || filterBlocation!=null || filterDlocaation!=null){
            if(FilterOpeName!=null){
                Travels.setText("   "+FilterOpeName);
            }if(filterBlocation!=null){
                filter_boarding_point.setText("   "+filterBlocation);
            }if(filterDlocaation!=null){
                filter_droping_point.setText("   "+filterDlocaation);
            }

        }*/
    }
    /** Called when another activity is taking focus. */
    @Override
    protected void onPause() {
        super.onPause();

    }
    /** Called when the activity is no longer visible. */
    @Override
    protected void onStop() {
        super.onStop();

    }
    /** Called just before the activity is destroyed. */
    @Override
    public void onDestroy() {
        super.onDestroy();

    }



}
