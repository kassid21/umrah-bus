package com.umrahbus;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umrahbus.BeanClasses.AgentDetailsBean;
import com.umrahbus.ConnectionStatus.ConnectivityReceiver;
import com.umrahbus.ConnectionStatus.MyApplication;
import com.umrahbus.Enums.Months;
import com.umrahbus.ServicesClasses.ServiceHandler;
import com.umrahbus.draweritems.Aboutus;
import com.umrahbus.draweritems.CancelBooking;
import com.umrahbus.draweritems.ContactUs;
import com.umrahbus.draweritems.FaqsActivity;
import com.umrahbus.draweritems.FeedBack;
import com.umrahbus.draweritems.Offers;
import com.umrahbus.draweritems.ReportsPage;
import com.umrahbus.draweritems.TermsAndConditions;
import com.umrahbus.loginpackage.LoginActivity;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ConnectivityReceiver.ConnectivityReceiverListener {

    Button From,TO,Search,Flip;
    String ArrivalID,DestinationId;
    Boolean flip = true;
    String name;
    private int checkinyear;
    private int checkinmonth;
    private int checkinday;
    private int checkoutyear;
    private int checkoutmonth;
    private int checkoutday;
    String str;
    TextView inmonth, outmonth, inday, outday, inyear, outyear;
    TextView checkindate, checkoutdate;
    TextView currentdate,currentmonth,currentweek;
    TextView ReturnDate,ReturnMonth,ReturnWeek,Tx_ReturnJournet;
    TextView OriginError,DistinationError;
    LinearLayout date,Returndate;
    static final int DATE_PICKER_ID1 = 111;
    static final int DATE_PICKER_ID2 = 222;
    String selectedDate,dayofjourney;
    String Names;
    String selectedOnwardDate,OnwardSelectedDate;
    String Return_Date_jurney,Return_dayofjourney;
    String IsReturnSelect="No";
    String  PassengerIsReturnselected = "No";
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    String AgentId, EMAIL,Name,ClientId,AgentLOGIN,UserLOGIN="No",agentLogin;
    NavigationView navigationView;
    String fromname,toname;
    int OnwardDate,OnwardMonth,OnwardYear;
    String FROMNAME,TONAME;
    TextView clientname,clientbal;
    ImageView clientlogo;
    String data,clientid,parentid,DecriptId;
    AgentDetailsBean agent_userbean;
    ArrayList<AgentDetailsBean> beanArrayList;

    Boolean doubleBackToExitPressedOnce=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        new GetClient().execute();

        checkConnection();
        SharedPreferences prefer = getApplicationContext().getSharedPreferences("Filter", MODE_PRIVATE);
        prefer.edit().remove("ACSELECTED").apply();
        prefer.edit().remove("NonACSELECTED").apply();
        prefer.edit().remove("SleeperSELECTED").apply();
        prefer.edit().remove("NonSleeperSELECTED").apply();
        prefer.edit().remove("Filter_OperaterId").apply();
        prefer.edit().remove("Filter_OperaterName").apply();
        prefer.edit().remove("FilterBoadingId").apply();
        prefer.edit().remove("FilterBoadingLocation").apply();
        prefer.edit().remove("FilterDropingId").apply();
        prefer.edit().remove("FilterDropinglocation").apply();
        prefer.edit().remove("FILTER").apply();


        SharedPreferences preference1 = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
        preference1.edit().remove("IsReturnSelected").apply();
        preference1.edit().remove("IsReturnYes").apply();
        preference1.edit().remove("ReturnTripDetails").apply();
        preference1.edit().remove("ReturnNumberOfSeats").apply();

        SharedPreferences preferences = getSharedPreferences("OnwardJourneyDetails", MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.clear();
        edit.apply();
        SharedPreferences preferencee = getSharedPreferences("JourneyDetails", MODE_PRIVATE);
        SharedPreferences.Editor editorr = preferencee.edit();
        editorr.clear();
        editorr.apply();

        pref = getSharedPreferences("LoginPreference", 0);
        editor = pref.edit();
        Name=pref.getString("Clientname",null);
        EMAIL=pref.getString("Emailid",null);
        AgentId=pref.getString("AgentId",null);
        ClientId=pref.getString("SuperAdmin",null);
        AgentLOGIN=pref.getString("AgentLOGIN",null);
        UserLOGIN=pref.getString("UserLOGIN",null);
        hideItem();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        headerView.findViewById(R.id.sideview);
        clientname=(TextView)headerView.findViewById(R.id.clientname);
        clientname.setText("");
        clientbal=(TextView)headerView.findViewById(R.id.clientbal);
        clientbal.setText("");
        clientlogo = (ImageView)headerView.findViewById(R.id.clientlogo);

        if(EMAIL!=null) {
            if (UserLOGIN != null && UserLOGIN.equals("Yes")) {
                clientname.setText("" + EMAIL);
                clientbal.setText("" + Name);


            }else{
                new AgentDetails().execute();

            }

        }else{
            clientname.setVisibility(View.GONE);
            clientbal.setVisibility(View.GONE);
        }


        final SharedPreferences preference = getApplicationContext().getSharedPreferences("Sources", MODE_PRIVATE);
         fromname=preference.getString("FROMNAME", null);
         toname=preference.getString("TONAME", null);
        final String arrival=preference.getString("FROMID",null);
        final String distination=preference.getString("TOID",null);
        ArrivalID=arrival;
        DestinationId=distination;
        FROMNAME=fromname;
        TONAME =toname;
        Names=fromname+" To "+toname;
        System.out.println("flip_***"+ArrivalID+DestinationId);


        currentdate=(TextView)findViewById(R.id.date);
        currentmonth=(TextView)findViewById(R.id.month);
        currentweek=(TextView)findViewById(R.id.week);

        Tx_ReturnJournet=(TextView)findViewById(R.id.ReturntextView2);
        ReturnDate=(TextView)findViewById(R.id.Returndate);
        ReturnWeek=(TextView)findViewById(R.id.Returnweek);
        ReturnMonth =(TextView)findViewById(R.id.Returnmonth);

        OriginError=(TextView)findViewById(R.id.OriginCityError);
        DistinationError=(TextView)findViewById(R.id.DestinationCityError);
        From = (Button) findViewById(R.id.FROM);
        From.setText(fromname);
        From.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FromActivity.class);
                startActivity(intent);
            }
        });
        TO=(Button)findViewById(R.id.TO);
        TO.setText(toname);
        TO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ToActivity.class);
                startActivity(intent);
            }
        });
        Flip=(Button)findViewById(R.id.FLIP);
        Flip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flip==true){

                    From.setText(toname);
                    TO.setText(fromname);
                    Names=toname+" To "+fromname;
                    ArrivalID=distination;
                    DestinationId=arrival;
                    FROMNAME=toname;
                    TONAME =fromname;
                    SharedPreferences preference = getApplication().getSharedPreferences("Sources", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preference.edit();
                    editor.putString("FROMID",distination);
                    editor.putString("FROMNAME",toname);
                    editor.putString("TOID",arrival);
                    editor.putString("TONAME",fromname);
                    editor.commit();
                    System.out.println("flip_true"+ArrivalID+DestinationId);
                    flip=false;
                }
               else if(flip==false){
                    From.setText(fromname);
                    TO.setText(toname);
                    Names=fromname+" To "+toname;
                    ArrivalID=arrival;
                    FROMNAME=fromname;
                    TONAME =toname;
                    DestinationId=distination;

                    System.out.println("flip_false"+ArrivalID+DestinationId);
                    flip=true;
                }
            }
        });

        Search=(Button)findViewById(R.id.search);
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Return_Date_jurney != null) {
                    if (fromname == null || toname == null) {

                        if (fromname == null) {
                            OriginError.setVisibility(View.VISIBLE);
                            DistinationError.setVisibility(View.GONE);
                        } else if (toname == null) {
                            OriginError.setVisibility(View.GONE);
                            DistinationError.setVisibility(View.VISIBLE);
                        }
                    } else {
                        SharedPreferences preference = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preference.edit();
                        editor.putString("JourneyDate", selectedDate);
                        editor.putString("ARRIVAL_ID", ArrivalID);
                        editor.putString("DESTINATION_ID", DestinationId);
                        editor.putString("JOURNEY_DATE", selectedDate);
                        editor.putString("DAY_OF_JOURNEY", dayofjourney);
                        editor.putString("TITLE_BAR", Names);
                        editor.putString("ReturnTripTITLE_BAR", Names);
                        editor.putString("PassengerRETURN_DATE", Return_Date_jurney);

                        editor.putString("PassengerIsReturnselected", IsReturnSelect);
                        editor.putString("IsReturnYes", "No");
                        editor.putString("FROMNAME", FROMNAME);
                        editor.putString("TONAME", TONAME);

                        editor.apply();
                        Intent intent = new Intent(MainActivity.this, SearchActivity.class);
               /*     intent.putExtra("ARRIVAL_ID", ArrivalID);
                    intent.putExtra("DESTINATION_ID", DestinationId);
                    intent.putExtra("JOURNEY_DATE", selectedDate);
                    intent.putExtra("DAY_OF_JOURNEY",dayofjourney);
                    intent.putExtra("TITLE_BAR",Names);*/


                        System.out.println("date***" + selectedDate);
                        startActivity(intent);
                    }
                }else{
                   Toast.makeText(MainActivity.this,"Please Select Return Date",Toast.LENGTH_LONG).show();
                }
            }
        });

        date=(LinearLayout)findViewById(R.id.datepicker);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialog = new DatePickerFragment();
                dialog.show(getSupportFragmentManager(),"DialogDate");
                //showDialog(DATE_PICKER_ID1);
            }
        });

        Returndate=(LinearLayout)findViewById(R.id.Returndatepicker);
        Returndate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialogFragment = new DatePickerFragmentreturnonName();
                dialogFragment.show(getSupportFragmentManager(),"DialogDateNo");
                //showDialog(DATE_PICKER_ID2);
            }
        });

        setCurrentDateOnView();
    }


/////On touch motion Event ////
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {
            return false;
        }
    }
    ///****Navigation Item Hide******????
    private void hideItem()
    {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();


        agentLogin=pref.getString("AgentLOGIN",null);

        if(agentLogin!=null && agentLogin.equals("Yes")) {
            nav_Menu.findItem(R.id.nav_Offers).setVisible(false);
        }
        if(EMAIL!=null){
            nav_Menu.findItem(R.id.nav_Login).setVisible(false);
            nav_Menu.findItem(R.id.nav_logout).setVisible(true);
        }else{
            nav_Menu.findItem(R.id.nav_logout).setVisible(false);
        }

    }
    //set Current Date on chechin time

    public void setCurrentDateOnView() {


        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        checkinday = c.get(Calendar.DAY_OF_MONTH);
        checkinmonth = c.get(Calendar.MONTH);
        checkinyear = c.get(Calendar.YEAR);



        String s = formattedDate;
        StringTokenizer st = new StringTokenizer(s, "-");
        String Date = st.nextToken();
        String Month = st.nextToken();
        String Year = st.nextToken();

        System.out.println("checkin"+checkinday+checkinmonth+checkinyear+inday+inmonth+formattedDate);
        String[] days = new String[]{"SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"};

        String week = days[c.get(Calendar.DAY_OF_WEEK) - 1];


        for (Months dir : Months.values()) {
            // do what you want
            str = "" + Months.values()[checkinmonth];
        }

        currentdate.setText("" + Date);
        currentweek.setText("" + week);
        currentmonth.setText(Month+","+Year);
        int month=checkinmonth+1;
        selectedDate=Date+"-"+month+"-"+Year;
        dayofjourney=Date+" "+Month+" "+Year;
        System.out.println("date"+Date+"-"+Month+"-"+Year+"journey"+dayofjourney);


        int SelectMonth =month;
        OnwardSelectedDate=""+Date+"-"+SelectMonth+"-"+Year;
        dayofjourney=Date+" "+Month+" "+Year;
        System.out.println("date"+Date+"-"+Month+"-"+Year+"journey"+dayofjourney);
        ReturnDate.setText("" + Date);
        ReturnWeek.setText("" + week);
        ReturnMonth.setText(Month+","+Year);


    }
//    @Override
//    protected Dialog onCreateDialog(int id) {
//        switch (id) {
//            case DATE_PICKER_ID1:
//                DialogFragment dFragment = new DatePickerFragment();
//
//                // Show the date picker dialog fragment
//                dFragment.show(getFragmentManager(), "Date Picker");
//                return null;
//
//
//            case DATE_PICKER_ID2:
//                // open datepicker dialog.
//                // set date picker for year==0,month==0,day==0
//                // add pickerListener listner to date picker
//                DialogFragment dFragment1 = new DatePickerFragmentreturnonName();
//
//                // Show the date picker dialog fragment
//                dFragment1.show(getFragmentManager(), "Date Picker");
//
//        }
//        return null;
//    }


    ///Date picker Fragment with max and min date
    @SuppressWarnings("ResourceType")
    //@SuppressLint("ValidFragment")
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        MainActivity activity;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            if(activity==null){
                activity = (MainActivity) getActivity();
            }

            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog dpd = new DatePickerDialog(getActivity(),this, year, month, day);


            calendar.add(Calendar.DATE, 90);


            // Set the Calendar new date as maximum date of date picker
            dpd.getDatePicker().setMaxDate(calendar.getTimeInMillis());

            // Subtract 6 days from Calendar updated date
            calendar.add(Calendar.DATE, -90);

            // Set the Calendar new date as minimum date of date picker
            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());

            // So, now date picker selectable date range is 7 days only
            SharedPreferences preference = getActivity().getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
            activity.PassengerIsReturnselected = preference.getString("PassengerIsReturnselected", null);
            // Return the DatePickerDialog
            return dpd;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the chosen date

            if(activity==null){
                activity = (MainActivity)getActivity();
            }

            // Create a Date variable/object with user chosen date
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(year, month, day, 0, 0, 0);
            int Month = month + 1;


            Date chosenDate = cal.getTime();

            // Format the date using style and locale
        /*    DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
            Datefarmetted = df.format(chosenDate);*/
            /*SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MMM-yyyy");
            Date date = new Date(year, month, day );


            String formattedDate = simpledateformat.format(date.getTime());
            String s=formattedDate;
            //selectedOnwardDate = formattedDate;
            StringTokenizer st = new StringTokenizer(s, "-");
            String Date = st.nextToken();
            String Month1 = st.nextToken();*/

           /* SimpleDateFormat sf = new SimpleDateFormat("EEEE");
            Date dat = new Date(year, month, day-1 );
            String dayOfWeek = sf.format(dat);*/
            // Display the chosen date to app interface
            activity.currentdate.setText("" + String.format("%02d", day));
            activity.currentweek.setText("" + activity.getDayofweek(year,month,day));
            activity.currentmonth.setText(activity.getmonthname(year,month,day)+","+year);

            activity.dayofjourney=String.format("%02d", day)+"  "+activity.getmonthname(year,month,day)+"  "+year;


            activity.selectedDate = String.format("%02d", day) + "-" + Month + "-" + year;
            int SelectMonth =Month;

            activity.OnwardSelectedDate=""+String.format("%02d", day)+"-"+SelectMonth+"-"+year;

            activity.OnwardDate = day;
            activity.OnwardMonth =Month;
            activity.OnwardYear =year;

            int ret_day,ret_month,ret_year,dayofweek;

            ret_day=activity.OnwardDate;
            ret_month=activity.OnwardMonth;
            ret_year=activity.OnwardYear;

            if(activity.TO.getText().toString().matches("Makkah")||activity.TO.getText().toString().matches("Madinah")||activity.TO.getText().toString().matches("Makkah and Madinah")){
                ret_day=activity.OnwardDate+2;
            }
            if (activity.TO.getText().toString().matches("Madinah and Makkah")){
                ret_day=activity.OnwardDate+3;
            }
            Calendar ret_cal=Calendar.getInstance();
            ret_cal.set(ret_year,ret_month-1,ret_day-1,0,0,0);
            ret_cal.add(Calendar.DAY_OF_YEAR,1);

            ret_day=ret_cal.get(Calendar.DAY_OF_MONTH);
            ret_month=ret_cal.get(Calendar.MONTH);
            ret_year=ret_cal.get(Calendar.YEAR);


            System.out.println("Day-- "+ret_day+" Month -- "+ret_month+" Year -- "+ret_year);


            /*if(PassengerIsReturnselected!=null){
                if(PassengerIsReturnselected.equals("Yes")){*/
            activity.ReturnDate.setText("" + String.format("%02d", ret_day));
            activity.ReturnWeek.setText("" + activity.getDayofweek(ret_year,ret_month,ret_day));
            activity.ReturnMonth.setText(activity.getmonthname(ret_year,ret_month,ret_day)+","+year);

            activity.ReturnDate.setTextColor(getResources().getColor(R.color.colorBlock));
            activity.ReturnWeek.setTextColor(getResources().getColor(R.color.colorBlock));
            activity.ReturnMonth.setTextColor(getResources().getColor(R.color.colorBlock));
            activity.Tx_ReturnJournet.setTextColor(getResources().getColor(R.color.colorBlock));
            activity.Return_dayofjourney=String.format("%02d", ret_day)+"  "+activity.getmonthname(ret_year,ret_month,ret_day)+"  "+year;

            activity.Return_Date_jurney = String.format("%02d", ret_day) + "-" + String.format("%02d",ret_month+1) + "-" + ret_year;
            /*    }
            }*/

            System.out.println("Selected date" + activity.OnwardSelectedDate);
            //  tv.setText(Datefarmetted);
        }
    }


    //////******Return Date Date Picker in Fragment *****///////
    @SuppressWarnings("ResourceType")
    //@SuppressLint("ValidFragment")
    public static class DatePickerFragmentreturnonName extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        MainActivity activity;



        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            if(activity==null) {
                activity = (MainActivity) getActivity();
            }

            final Calendar calendar = Calendar.getInstance();

            int year = activity.OnwardYear;
            int month = activity.OnwardMonth;
            int day = activity.OnwardDate;
            if(activity.TO.getText().toString().matches("Makkah")||activity.TO.getText().toString().matches("Madinah")||activity.TO.getText().toString().matches("Makkah and Madinah")){
                day=activity.OnwardDate+2;
            }
            if (activity.TO.getText().toString().matches("Madinah and Makkah")){
                day=activity.OnwardDate+3;
            }
            int Month=month-1;

            /*
                We should use THEME_HOLO_LIGHT, THEME_HOLO_DARK or THEME_TRADITIONAL only.

                The THEME_DEVICE_DEFAULT_LIGHT and THEME_DEVICE_DEFAULT_DARK does not work
                perfectly. This two theme set disable color of disabled dates but users can
                select the disabled dates also.

                Other three themes act perfectly after defined enabled date range of date picker.
                Those theme completely hide the disable dates from date picker object.
             */
            DatePickerDialog dpd = new DatePickerDialog(getActivity(),this, year, Month, day);

            /*
                add(int field, int value)
                    Adds the given amount to a Calendar field.
             */
            // Add 3 days to Calendar
            calendar.add(Calendar.DATE, 90);

            /*
                getTimeInMillis()
                    Returns the time represented by this Calendar,
                    recomputing the time from its fields if necessary.

                getDatePicker()
                Gets the DatePicker contained in this dialog.

                setMinDate(long minDate)
                    Sets the minimal date supported by this NumberPicker
                    in milliseconds since January 1, 1970 00:00:00 in getDefault() time zone.

                setMaxDate(long maxDate)
                    Sets the maximal date supported by this DatePicker in milliseconds
                    since January 1, 1970 00:00:00 in getDefault() time zone.
             */

            // Set the Calendar new date as maximum date of date picker
            dpd.getDatePicker().setMaxDate(calendar.getTimeInMillis());

            // Subtract 6 days from Calendar updated date


            calendar.add(Calendar.DATE, -90);

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date d = null;
            try {
                d = sdf.parse(activity.OnwardSelectedDate);
                Calendar c = Calendar.getInstance();
                c.setTime(d);
                if(activity.TO.getText().toString().matches("Makkah")||activity.TO.getText().toString().matches("Madinah")||activity.TO.getText().toString().matches("Makkah and Madinah")){
                    c.add(Calendar.DATE, 2);
                }
                if (activity.TO.getText().toString().matches("Madinah and Makkah")){
                    c.add(Calendar.DATE, 3);
                }
                d = c.getTime();
                dpd.getDatePicker().setMinDate(d.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            // Set the Calendar new date as minimum date of date picker
            //   dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());
            //  dpd.getDatePicker().setMinDate(Long.parseLong(selectedOnwardDate));

            // So, now date picker selectable date range is 7 days only

            // Return the DatePickerDialog
            return dpd;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the chosen date
            // Create a Date variable/object with user chosen date
            if(activity==null) {
                activity = (MainActivity) getActivity();
            }

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(year, month, day, 0, 0, 0);
            int Month = month + 1;

            Date chosenDate = cal.getTime();

            // Format the date using style and locale
        /*    DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
            Datefarmetted = df.format(chosenDate);*/
            SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MMM-yyyy");
            Date date = new Date(year, month, day );


            String formattedDate = simpledateformat.format(date.getTime());
            String s = formattedDate;
            StringTokenizer st = new StringTokenizer(s, "-");
            String Date = st.nextToken();
            String Month1 = st.nextToken();

            SimpleDateFormat sf = new SimpleDateFormat("EEEE");
            Date dat = new Date(year, month, day-1 );
            String dayOfWeek = sf.format(dat);
            // Display the chosen date to app interface
            activity.ReturnDate.setText("" + Date);
            activity.ReturnWeek.setText("" + dayOfWeek);
            activity.ReturnMonth.setText(Month1+","+year);

            activity.ReturnDate.setTextColor(getResources().getColor(R.color.colorBlock));
            activity.ReturnWeek.setTextColor(getResources().getColor(R.color.colorBlock));
            activity.ReturnMonth.setTextColor(getResources().getColor(R.color.colorBlock));
            activity.Tx_ReturnJournet.setTextColor(getResources().getColor(R.color.colorBlock));
            activity.Return_dayofjourney=Date+"  "+Month1+"  "+year;

            activity.Return_Date_jurney = Date + "-" + Month + "-" + year;
            activity.IsReturnSelect="Yes";
            activity.Names=activity.toname+" To "+activity.fromname;
            SharedPreferences preference = activity.getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
            SharedPreferences.Editor editor = preference.edit();
            editor.putString("PassengerIsReturnselected",activity.IsReturnSelect);
            editor.commit();
            System.out.println("Return date" + activity.Return_Date_jurney);
        }
    }

//    ///Date picker Fragment with max and min date
//    @SuppressWarnings("ResourceType")
//    @SuppressLint("ValidFragment")
//    public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
//
//
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            final Calendar calendar = Calendar.getInstance();
//            int year = calendar.get(Calendar.YEAR);
//            int month = calendar.get(Calendar.MONTH);
//            int day = calendar.get(Calendar.DAY_OF_MONTH);
//
//
//            DatePickerDialog dpd = new DatePickerDialog(getActivity(),this, year, month, day);
//
//
//            calendar.add(Calendar.DATE, 90);
//
//
//            // Set the Calendar new date as maximum date of date picker
//            dpd.getDatePicker().setMaxDate(calendar.getTimeInMillis());
//
//            // Subtract 6 days from Calendar updated date
//            calendar.add(Calendar.DATE, -90);
//
//            // Set the Calendar new date as minimum date of date picker
//            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());
//
//            // So, now date picker selectable date range is 7 days only
//            SharedPreferences preference = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
//            PassengerIsReturnselected = preference.getString("PassengerIsReturnselected", null);
//            // Return the DatePickerDialog
//            return dpd;
//        }
//
//        public void onDateSet(DatePicker view, int year, int month, int day) {
//            // Do something with the chosen date
//
//
//            // Create a Date variable/object with user chosen date
//            Calendar cal = Calendar.getInstance();
//            cal.setTimeInMillis(0);
//            cal.set(year, month, day, 0, 0, 0);
//            int Month = month + 1;
//
//
//            Date chosenDate = cal.getTime();
//
//            // Format the date using style and locale
//        /*    DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
//            Datefarmetted = df.format(chosenDate);*/
//            /*SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MMM-yyyy");
//            Date date = new Date(year, month, day );
//
//
//            String formattedDate = simpledateformat.format(date.getTime());
//            String s=formattedDate;
//            //selectedOnwardDate = formattedDate;
//            StringTokenizer st = new StringTokenizer(s, "-");
//            String Date = st.nextToken();
//            String Month1 = st.nextToken();*/
//
//           /* SimpleDateFormat sf = new SimpleDateFormat("EEEE");
//            Date dat = new Date(year, month, day-1 );
//            String dayOfWeek = sf.format(dat);*/
//            // Display the chosen date to app interface
//            currentdate.setText("" + String.format("%02d", day));
//            currentweek.setText("" + getDayofweek(year,month,day));
//            currentmonth.setText(getmonthname(year,month,day)+","+year);
//
//            dayofjourney=String.format("%02d", day)+"  "+getmonthname(year,month,day)+"  "+year;
//
//
//            selectedDate = String.format("%02d", day) + "-" + Month + "-" + year;
//            int SelectMonth =Month;
//
//            OnwardSelectedDate=""+String.format("%02d", day)+"-"+SelectMonth+"-"+year;
//
//            OnwardDate = day;
//            OnwardMonth =Month;
//            OnwardYear =year;
//
//            int ret_day,ret_month,ret_year,dayofweek;
//
//            ret_day=OnwardDate;
//            ret_month=OnwardMonth;
//            ret_year=OnwardYear;
//
//            if(TO.getText().toString().matches("Makkah")||TO.getText().toString().matches("Madinah")||TO.getText().toString().matches("Makkah and Madinah")){
//                ret_day=OnwardDate+2;
//            }
//            if (TO.getText().toString().matches("Madinah and Makkah")){
//                ret_day=OnwardDate+3;
//            }
//            Calendar ret_cal=Calendar.getInstance();
//            ret_cal.set(ret_year,ret_month-1,ret_day-1,0,0,0);
//            ret_cal.add(Calendar.DAY_OF_YEAR,1);
//
//             ret_day=ret_cal.get(Calendar.DAY_OF_MONTH);
//             ret_month=ret_cal.get(Calendar.MONTH);
//             ret_year=ret_cal.get(Calendar.YEAR);
//
//
//            System.out.println("Day-- "+ret_day+" Month -- "+ret_month+" Year -- "+ret_year);
//
//
//            /*if(PassengerIsReturnselected!=null){
//                if(PassengerIsReturnselected.equals("Yes")){*/
//                    ReturnDate.setText("" + String.format("%02d", ret_day));
//                    ReturnWeek.setText("" + getDayofweek(ret_year,ret_month,ret_day));
//                    ReturnMonth.setText(getmonthname(ret_year,ret_month,ret_day)+","+year);
//
//                    ReturnDate.setTextColor(getResources().getColor(R.color.colorBlock));
//                    ReturnWeek.setTextColor(getResources().getColor(R.color.colorBlock));
//                    ReturnMonth.setTextColor(getResources().getColor(R.color.colorBlock));
//                    Tx_ReturnJournet.setTextColor(getResources().getColor(R.color.colorBlock));
//                    Return_dayofjourney=String.format("%02d", ret_day)+"  "+getmonthname(ret_year,ret_month,ret_day)+"  "+year;
//
//                    Return_Date_jurney = String.format("%02d", ret_day) + "-" + String.format("%02d",ret_month+1) + "-" + ret_year;
//            /*    }
//            }*/
//
//            System.out.println("Selected date" + OnwardSelectedDate);
//            //  tv.setText(Datefarmetted);
//        }
//    }
//
//
//    //////******Return Date Date Picker in Fragment *****///////
//    @SuppressWarnings("ResourceType")
//    @SuppressLint("ValidFragment")
//    public class DatePickerFragmentreturnonName extends DialogFragment implements DatePickerDialog.OnDateSetListener {
//
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            final Calendar calendar = Calendar.getInstance();
//
//
//
//            int year = OnwardYear;
//            int month = OnwardMonth;
//            int day = OnwardDate;
//            if(TO.getText().toString().matches("Makkah")||TO.getText().toString().matches("Madinah")||TO.getText().toString().matches("Makkah and Madinah")){
//                day=OnwardDate+2;
//            }
//            if (TO.getText().toString().matches("Madinah and Makkah")){
//                day=OnwardDate+3;
//            }
//            int Month=month-1;
//
//            /*
//                We should use THEME_HOLO_LIGHT, THEME_HOLO_DARK or THEME_TRADITIONAL only.
//
//                The THEME_DEVICE_DEFAULT_LIGHT and THEME_DEVICE_DEFAULT_DARK does not work
//                perfectly. This two theme set disable color of disabled dates but users can
//                select the disabled dates also.
//
//                Other three themes act perfectly after defined enabled date range of date picker.
//                Those theme completely hide the disable dates from date picker object.
//             */
//            DatePickerDialog dpd = new DatePickerDialog(getActivity(),this, year, Month, day);
//
//            /*
//                add(int field, int value)
//                    Adds the given amount to a Calendar field.
//             */
//            // Add 3 days to Calendar
//            calendar.add(Calendar.DATE, 90);
//
//            /*
//                getTimeInMillis()
//                    Returns the time represented by this Calendar,
//                    recomputing the time from its fields if necessary.
//
//                getDatePicker()
//                Gets the DatePicker contained in this dialog.
//
//                setMinDate(long minDate)
//                    Sets the minimal date supported by this NumberPicker
//                    in milliseconds since January 1, 1970 00:00:00 in getDefault() time zone.
//
//                setMaxDate(long maxDate)
//                    Sets the maximal date supported by this DatePicker in milliseconds
//                    since January 1, 1970 00:00:00 in getDefault() time zone.
//             */
//
//            // Set the Calendar new date as maximum date of date picker
//            dpd.getDatePicker().setMaxDate(calendar.getTimeInMillis());
//
//            // Subtract 6 days from Calendar updated date
//
//
//            calendar.add(Calendar.DATE, -90);
//
//            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//            Date d = null;
//            try {
//                d = sdf.parse(OnwardSelectedDate);
//                Calendar c = Calendar.getInstance();
//                c.setTime(d);
//                if(TO.getText().toString().matches("Makkah")||TO.getText().toString().matches("Madinah")||TO.getText().toString().matches("Makkah and Madinah")){
//                    c.add(Calendar.DATE, 2);
//                }
//                if (TO.getText().toString().matches("Madinah and Makkah")){
//                    c.add(Calendar.DATE, 3);
//                }
//                d = c.getTime();
//                dpd.getDatePicker().setMinDate(d.getTime());
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//            // Set the Calendar new date as minimum date of date picker
//            //   dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());
//            //  dpd.getDatePicker().setMinDate(Long.parseLong(selectedOnwardDate));
//
//            // So, now date picker selectable date range is 7 days only
//
//            // Return the DatePickerDialog
//            return dpd;
//        }
//
//        public void onDateSet(DatePicker view, int year, int month, int day) {
//            // Do something with the chosen date
//            // Create a Date variable/object with user chosen date
//
//            Calendar cal = Calendar.getInstance();
//            cal.setTimeInMillis(0);
//            cal.set(year, month, day, 0, 0, 0);
//            int Month = month + 1;
//
//            Date chosenDate = cal.getTime();
//
//            // Format the date using style and locale
//        /*    DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
//            Datefarmetted = df.format(chosenDate);*/
//            SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MMM-yyyy");
//            Date date = new Date(year, month, day );
//
//
//            String formattedDate = simpledateformat.format(date.getTime());
//            String s = formattedDate;
//            StringTokenizer st = new StringTokenizer(s, "-");
//            String Date = st.nextToken();
//            String Month1 = st.nextToken();
//
//            SimpleDateFormat sf = new SimpleDateFormat("EEEE");
//            Date dat = new Date(year, month, day-1 );
//            String dayOfWeek = sf.format(dat);
//            // Display the chosen date to app interface
//            ReturnDate.setText("" + Date);
//            ReturnWeek.setText("" + dayOfWeek);
//            ReturnMonth.setText(Month1+","+year);
//
//            ReturnDate.setTextColor(getResources().getColor(R.color.colorBlock));
//            ReturnWeek.setTextColor(getResources().getColor(R.color.colorBlock));
//            ReturnMonth.setTextColor(getResources().getColor(R.color.colorBlock));
//            Tx_ReturnJournet.setTextColor(getResources().getColor(R.color.colorBlock));
//            Return_dayofjourney=Date+"  "+Month1+"  "+year;
//
//            Return_Date_jurney = Date + "-" + Month + "-" + year;
//            IsReturnSelect="Yes";
//            Names=toname+" To "+fromname;
//            SharedPreferences preference = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
//            SharedPreferences.Editor editor = preference.edit();
//            editor.putString("PassengerIsReturnselected",IsReturnSelect);
//            editor.commit();
//            System.out.println("Return date" + Return_Date_jurney);
//        }
//    }

    public String getmonthname(int year,int month,int day){
        SimpleDateFormat simpledateformat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = new Date(year, month, day );


        String formattedDate = simpledateformat.format(date.getTime());
        String s=formattedDate;
        //selectedOnwardDate = formattedDate;
        StringTokenizer st = new StringTokenizer(s, "-");
        String Date = st.nextToken();
        String Month1 = st.nextToken();
        return Month1;
    }

    public String getDayofweek(int year,int month,int day){
        SimpleDateFormat sf = new SimpleDateFormat("EEEE");
        Date dat = new Date(year, month, day-1 );
        return sf.format(dat);
    }

    int back=0;
    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        Logout alertdFragment = new Logout();
        alertdFragment.show(fm, "Exit");

       /* if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press again to close "+getResources().getString(R.string.app_name), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 5000);
*/
        // mDrawerLayout.closeDrawers();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_Login) {
            Intent i=new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            // Handle the camera action
        } else if (id == R.id.nav_Booking) {
            Intent i=new Intent(MainActivity.this, MainActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_Cancelbooking) {
            Intent i=new Intent(MainActivity.this, CancelBooking.class);
            startActivity(i);
        }else if (id == R.id.nav_Reports) {
            if(EMAIL!=null){
                Intent i=new Intent(MainActivity.this, ReportsPage.class);
                startActivity(i);
            }else{
                Intent i=new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
            }

        } else if (id == R.id.nav_Offers) {
            Intent i=new Intent(MainActivity.this, Offers.class);
            startActivity(i);
        } else if (id == R.id.nav_Feedback) {
            Intent i=new Intent(MainActivity.this, FeedBack.class);
            startActivity(i);
        } else if (id == R.id.nav_Faqs) {
            Intent i=new Intent(MainActivity.this, FaqsActivity.class);
            startActivity(i);
        }else if (id == R.id.nav_Termsandconditions) {
            Intent i=new Intent(MainActivity.this, TermsAndConditions.class);
            startActivity(i);
        }else if (id == R.id.nav_About) {
            Intent i = new Intent(MainActivity.this, Aboutus.class);
            startActivity(i);
        }else if(id==R.id.nav_contuctus){
                Intent i=new Intent(MainActivity.this, ContactUs.class);
                startActivity(i);
            }else if (id == R.id.nav_logout) {

            SharedPreferences preference = getApplicationContext().getSharedPreferences("LoginPreference", MODE_PRIVATE);
            preference.edit().remove("UserLOGIN").apply();
            preference.edit().remove("AgentLOGIN").apply();
            editor.clear();
            editor.commit();
            clientname.setText("");
            clientbal.setText("");
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_logout).setVisible(false);
            nav_Menu.findItem(R.id.nav_Login).setVisible(true);
            Toast.makeText(MainActivity.this,"Logged out Successfully",Toast.LENGTH_LONG).show();
            startActivity(new Intent(this,MainActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class GetClient extends AsyncTask<String, String, String> {


        @Override
        protected String doInBackground(String... params) {
            String strurl = Constants.BUSAPI+ Constants.ClientDetails;
            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(strurl);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            try {
                data = httpClient.execute(httpGet, responseHandler);
            } catch (IOException e) {
                e.printStackTrace();
            }


            return data;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(data);


                if (!data.equals("") && !data.equals("null") && !data.contains("Message")) {

                    jsonObject.getString("IsCorporate");
                    // encryptclientid=jsonObject.getString("ClientId")
                    clientid=jsonObject.getString("ClientId");

                    editor.putString("Name", jsonObject.getString("Name"));
                    editor.putString("Domain", jsonObject.getString("Domain"));
                    editor.putString("IP", jsonObject.getString("IP"));
                    editor.putString("ConsumerKey", jsonObject.getString("ConsumerKey"));
                    editor.putString("ConsumerSecret", jsonObject.getString("ConsumerSecret"));
                    editor.putString("Email", jsonObject.getString("Email"));
                    editor.putString("BusinessType", jsonObject.getString("BusinessType"));
                    editor.putString("ClientType", jsonObject.getString("ClientType"));
                    editor.putString("Status", jsonObject.getString("Status"));
                    editor.putString("Template", jsonObject.getString("Template"));
                    editor.putString("Services", jsonObject.getString("Services"));
                    editor.putString("ContactNo", jsonObject.getString("ContactNo"));
                    editor.putString("Telephone", jsonObject.getString("Telephone"));
                    editor.putString("Address", jsonObject.getString("Address"));
                    editor.putBoolean("onetime", true);
                    editor.commit();

                } else {

                }

            } catch (Exception e) {

            }
            new Getparentid().execute();
        }
    }

    public class Getparentid extends AsyncTask<String,String,String>{

        @Override
        protected String doInBackground(String... params) {
            ServiceHandler sh=new ServiceHandler();
            String jsonstr = sh.makeServiceCall(Constants.BUSAPI+ Constants.AgentList, ServiceHandler.GET);
            if(jsonstr !=""){
                try{

                    JSONArray jarray=new JSONArray(jsonstr);
                    for(int i=0;i<jarray.length();i++){
                        JSONObject jsonObject=jarray.getJSONObject(i);
                        parentid=jsonObject.getString("UserId");
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            System.out.println("parentid"+parentid);
            editor.putString("SuperAdmin", parentid);
            editor.commit();
            new Decrypt().execute();
            //  new AgentRegistration().execute();
        }
    }

    public class Decrypt extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            ServiceHandler sh = new ServiceHandler();
            String jsonstr1 = sh.makeServiceCall(Constants.BUSAPI+ Constants.DecriptCode+parentid, ServiceHandler.GET);
            if (jsonstr1 != "") {
                try {
                    JSONObject joj = new JSONObject(jsonstr1);

                    String decryptcode = joj.getString("StatusCode");
                    DecriptId=(joj.getString("Message"));
                  /*  if (decryptcode.equals("200")) {
                        String url=Constants.logoURL+DecriptId+"/logo.png";
                        if(url==null){
                         //   clientlogo.setImageResource(R.drawable.bus);
                        }else {
                            try {
                              //  new DownloadImageTask(clientlogo).execute(url);
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                        }


                    }*/




                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return null;
        }
    }
    public class AgentDetails extends AsyncTask<String, String, String> {


        String result;

        @Override
        protected String doInBackground(String... params) {


            ServiceHandler sh = new ServiceHandler();
            String jsonstr = sh.makeServiceCall(Constants.BUSAPI+ Constants.AgentDetails+EMAIL, ServiceHandler.GET);
            if (jsonstr != "") {

                try {

                    agent_userbean = new AgentDetailsBean();
                    beanArrayList = new ArrayList<AgentDetailsBean>();
                    JSONObject jsonObject = new JSONObject(jsonstr);

                    agent_userbean.setStatus(jsonObject.getString("Status"));
                    agent_userbean.setBalance(jsonObject.getString("Balance"));
                    agent_userbean.setVirtualBalance(jsonObject.getString("VirtualBalance"));
                    agent_userbean.setBalanceAlert(jsonObject.getString("BalanceAlert"));
                    agent_userbean.setIsAgent(jsonObject.getString("IsAgent"));
                    agent_userbean.setCompanyName(jsonObject.getString("CompanyName"));
                    agent_userbean.setPAN(jsonObject.getString("PAN"));
                    agent_userbean.setClientType(jsonObject.getString("ClientType"));
                    agent_userbean.setIsSIteAdminAgent(jsonObject.getString("IsSIteAdminAgent"));
                    agent_userbean.setUserId(jsonObject.getString("UserId"));
                    agent_userbean.setClientId(jsonObject.getString("ClientId"));
                    agent_userbean.setFirstName(jsonObject.getString("FirstName"));
                    agent_userbean.setLastName(jsonObject.getString("LastName"));
                    agent_userbean.setPassword(jsonObject.getString("Password"));
                    agent_userbean.setGender(jsonObject.getString("Gender"));
                    agent_userbean.setDOB(jsonObject.getString("DOB"));
                    agent_userbean.setEmail(jsonObject.getString("Email"));
                    agent_userbean.setAlternateEmail(jsonObject.getString("AlternateEmail"));
                    agent_userbean.setTelephone(jsonObject.getString("Telephone"));
                    agent_userbean.setMobile(jsonObject.getString("Mobile"));
                    agent_userbean.setAlternateMobile(jsonObject.getString("AlternateMobile"));
                    agent_userbean.setAddress(jsonObject.getString("Address"));
                    agent_userbean.setCity(jsonObject.getString("City"));
                    agent_userbean.setState(jsonObject.getString("State"));
                    agent_userbean.setPincode(jsonObject.getString("Pincode"));
                    agent_userbean.setActivationCode(jsonObject.getString("ActivationCode"));
                    agent_userbean.setDeviceModel(jsonObject.getString("DeviceModel"));
                    agent_userbean.setDeviceOS(jsonObject.getString("DeviceOS"));
                    agent_userbean.setDeviceOSVersion(jsonObject.getString("DeviceOSVersion"));
                    agent_userbean.setDeviceToken(jsonObject.getString("DeviceToken"));
                    JSONArray jarry = jsonObject.getJSONArray("Services");
                    String[] arr = new String[jarry.length()];

                    for (int k = 0; k < jarry.length(); k++) {
                        arr[k] = jarry.getString(k);
                        agent_userbean.setServices(arr);

                    }

                    beanArrayList.add(agent_userbean);





                    // clientname.setText(pref.getString("Clientname",""));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            editor.putString("Clientname",agent_userbean.getBalance());
            editor.putString("Emailid",agent_userbean.getEmail());
            editor.commit();
            clientbal.setText(""+agent_userbean.getBalance());
            clientname.setText(""+agent_userbean.getEmail());
        }
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
            bmImage.setScaleType(ImageView.ScaleType.FIT_XY);
        }

    }

    // Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set title
            //  alertDialogBuilder.setTitle("Your Title");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Sorry! Not connected to internet")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity

                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
    //  finish();



}
