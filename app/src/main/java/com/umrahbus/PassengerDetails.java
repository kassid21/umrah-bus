package com.umrahbus;


import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.umrahbus.ConnectionStatus.ConnectivityReceiver;
import com.umrahbus.ConnectionStatus.MyApplication;
import com.umrahbus.Paymentgateways.Payfort;
import com.umrahbus.Paymentgateways.ResponseActivity;
import com.umrahbus.ServicesClasses.ServiceHandler;

import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class PassengerDetails extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    EditText contact_email, contact_mobile, passenger_fullname, passenger_nationality, /*address,*/ Idnumber, Promocode;
    EditText Co_passenger_fullname1,Co_passenger_fullname2,Co_passenger_fullname3,Co_passenger_fullname4,Co_passenger_fullname5;
    EditText Co_passenger_nationality1,Co_passenger_nationality2,Co_passenger_nationality3,Co_passenger_nationality4,Co_passenger_nationality5;
    EditText Co_passenger1_Id_proof_number,Co_passenger2_Id_proof_number,Co_passenger3_Id_proof_number,Co_passenger4_Id_proof_number,Co_passenger5_Id_proof_number;
    Button Male,Female, ContinueBooking;
    Button Co_Passenger_Female1,Co_Passenger_Female2,Co_Passenger_Female3,Co_Passenger_Female4,Co_Passenger_Female5;
    Button Co_Passenger_Male1,Co_Passenger_Male2,Co_Passenger_Male3,Co_Passenger_Male4,Co_Passenger_Male5;
    CardView PromoCodeCardView,Co_Passenger1,Co_Passenger2,Co_Passenger3,Co_Passenger4,Co_Passenger5;
    Spinner Title, TravellerId,CopassengerTitle1,CopassengerTitle2,CopassengerTitle3,CopassengerTitle4,CopassengerTitle5;
    Spinner Co_passenger1_id_proof,Co_passenger2_id_proof,Co_passenger3_id_proof,Co_passenger4_id_proof,Co_passenger5_id_proof;
    String[] Titles = {"Mr", "Mrs", "Ms"};
    String[] IdProof = {"National ID", "IQAAMA", "PASSPORT"};
    //String title, IDs;
    String CTitle1,CTitle2,CTitle3,CTitle4,CTitle5;
    String EMAIL, MOBILE, FullName, NATIONALITY, Gender, IdProofNumber/*, Address*/;
    String IdCard="0",title, IDs="IQAAMA",Copassenger1Id="IQAAMA",Copassenger2Id="IQAAMA",Copassenger3Id="IQAAMA",Copassenger4Id="IQAAMA",Copassenger5Id="IQAAMA";
    Dialog agentpaymentDialog;
    Dialog dialog;
    InputStream inputStream;
    String data, responce;
    String refno, str, status;
    ImageView wallet;
    Boolean dialogwallet=false;
    String SourceName,DestinationName,SourceId,DestinationId,JourneyDate,BoardingId,BoardingPointDetails,SelectedSeats,NumberOfSeats,BusTypeID,OnwardDisplayname
            ,OnwardHotelname;
    String Operator,BusType,departureTime,BusTypeid,NetFare,Displayname;
    String CancellationPolicy,PartialCancellationAllowed,Provider,ObiboAPIProvidrer;
    String gender="M",gender1="M",gender2="M",gender3="M",gender4="M",gender5="M";

    String Co_passenger_fullname_1,Co_passenger_fullname_2,Co_passenger_fullname_3,Co_passenger_fullname_4,Co_passenger_fullname_5;
    String Co_passenger_nationality_1,Co_passenger_nationality_2,Co_passenger_nationality_3,Co_passenger_nationality_4,Co_passenger_nationality_5;
    String Co_passenger1_id_proof_number,Co_passenger2_id_proof_number,Co_passenger3_id_proof_number,Co_passenger4_id_proof_number,Co_passenger5_id_proof_number;

    String Passenger_Full_name,Passenger_Nationality,Passenger_gender,TotalTitles;
    String ServiceTaxes,OperatorserviceCharge,Fares,ValidPromoCode;
    String RoundTripFare,OnwardTripFare;;
    String Returndate,DisplayName;
    String ReturnOperator,ReturnBusType,ReturnBusTypeid,ReturndepartureTime,ReturnCancellationPolicy,ReturnPartialCancellationAllowed;
    String ReturnProvider,ReturnObiboAPIProvidrer,ReturnJourneyDate,ReturnSelectedSeats,ReturnNumberOfSeats,ReturnBusTypeID,ReturnNetFare,
            ReturnServiceTaxes,ReturnOperatorserviceCharge,ReturnFares,ReturnBoardingId ,ReturnBoardingPointDetails;

    String IsReturnSelected;

    String Ispackage,OnwardReturnDate;


    Button  PromoCodeApplay;
    int Discountint;
    String DiscountAmount;
    String Correct_Promo = "false";
    String PROMO = "false";
    ArrayList<HashMap<String, String>> Codes = new ArrayList<>();
    LinearLayout PrompromoCodeApplyLayoutocode, promocodeAmountDetails;
    TextView PromocodeTotalPay, PromocodeDiscountAmount, PromocodeYouPay,HavepromoTV;

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String UserId, User_agentid;
    String userLogin,agentLogin;
    String HotelName;
    String OnwardReferenceNumber;


     RadioButton payment_gateway,cash_on_delivery;
     RadioGroup payment_mode_radio_grp;
     Boolean isCOD;
     //EditText landmark_et;
    String BookedBy,BookedByEmail,BookedByUserID;

    String ReturnTicketrefno,ReturnTicketstr;
    String OnwardReturnBoarding,OnwardReturnBoardingID;
    String ArravaialTime,Main_Departure_time;
    Boolean IsCod_afterclick;
    //CardView landmark_card;
    String ActualNetFare, ActualReturnNetFare;

    static final int mREQUEST_CODE = 3210;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_passenger_details);

        contact_email = (EditText) findViewById(R.id.Contact_Email);
        contact_mobile = (EditText) findViewById(R.id.Contact_Mobile);
        passenger_fullname = (EditText) findViewById(R.id.full_name);
        passenger_nationality = (EditText) findViewById(R.id.Nationality);
        ContinueBooking = (Button) findViewById(R.id.Booking);
//        address = (EditText) findViewById(R.id.Address);
        Idnumber = (EditText) findViewById(R.id.Id_proof_number);
        Male=(Button)findViewById(R.id.Male);
        Female=(Button)findViewById(R.id.Female);
        Male.setOnClickListener(this);
        Female.setOnClickListener(this);

        payment_gateway= (RadioButton) findViewById(R.id.payment_gateway);
        cash_on_delivery=(RadioButton) findViewById(R.id.cod);
        payment_mode_radio_grp= (RadioGroup) findViewById(R.id.payment_cod);
        //landmark_et= (EditText) findViewById(R.id.landmark);
        //landmark_card=findViewById(R.id.landmark_card);


        Promocode = (EditText) findViewById(R.id.Promocode);
        PromoCodeApplay = (Button) findViewById(R.id.PromoCodeApplay);
        PromoCodeCardView = (CardView) findViewById(R.id.PromoCodeCardView);
        PrompromoCodeApplyLayoutocode = (LinearLayout) findViewById(R.id.promoCodeApplyLayout);
        promocodeAmountDetails = (LinearLayout) findViewById(R.id.promocodeAmountDetails);
        PromocodeTotalPay = (TextView) findViewById(R.id.TotalPay);
        PromocodeDiscountAmount = (TextView) findViewById(R.id.DiscountAmount);
        PromocodeYouPay = (TextView) findViewById(R.id.YouPay);
        PromoCodeApplay.setOnClickListener(this);

        Co_passenger_fullname1 = (EditText) findViewById(R.id.full_name1);
        Co_passenger_fullname2 = (EditText) findViewById(R.id.full_name2);
        Co_passenger_fullname3 = (EditText) findViewById(R.id.full_name3);
        Co_passenger_fullname4 = (EditText) findViewById(R.id.full_name4);
        Co_passenger_fullname5 = (EditText) findViewById(R.id.full_name5);


        Co_passenger_nationality1 = (EditText) findViewById(R.id.Nationality1);
        Co_passenger_nationality2 = (EditText) findViewById(R.id.Nationality2);
        Co_passenger_nationality3 = (EditText) findViewById(R.id.Nationality3);
        Co_passenger_nationality4 = (EditText) findViewById(R.id.Nationality4);
        Co_passenger_nationality5 = (EditText) findViewById(R.id.Nationality5);



        Co_Passenger_Male1=(Button)findViewById(R.id.Male1);
        Co_Passenger_Female1=(Button)findViewById(R.id.Female1);
        Co_Passenger_Male2=(Button)findViewById(R.id.Male2);
        Co_Passenger_Female2=(Button)findViewById(R.id.Female2);
        Co_Passenger_Male3=(Button)findViewById(R.id.Male3);
        Co_Passenger_Female3=(Button)findViewById(R.id.Female3);
        Co_Passenger_Male4=(Button)findViewById(R.id.Male4);
        Co_Passenger_Female4=(Button)findViewById(R.id.Female4);
        Co_Passenger_Male5=(Button)findViewById(R.id.Male5);
        Co_Passenger_Female5=(Button)findViewById(R.id.Female5);

        Co_Passenger_Male1.setOnClickListener(this);
        Co_Passenger_Female1.setOnClickListener(this);
        Co_Passenger_Male2.setOnClickListener(this);
        Co_Passenger_Female2.setOnClickListener(this);
        Co_Passenger_Male3.setOnClickListener(this);
        Co_Passenger_Female3.setOnClickListener(this);
        Co_Passenger_Male4.setOnClickListener(this);
        Co_Passenger_Female4.setOnClickListener(this);
        Co_Passenger_Male5.setOnClickListener(this);
        Co_Passenger_Female5.setOnClickListener(this);

        Co_Passenger1 = (CardView) findViewById(R.id.Co_passenger1);
        Co_Passenger2 = (CardView) findViewById(R.id.Co_passenger2);
        Co_Passenger3 = (CardView) findViewById(R.id.Co_passenger3);
        Co_Passenger4 = (CardView) findViewById(R.id.Co_passenger4);
        Co_Passenger5 = (CardView) findViewById(R.id.Co_passenger5);

        Co_passenger1_Id_proof_number = (EditText) findViewById(R.id.Co_passenger1_Id_proof_number);
        Co_passenger2_Id_proof_number = (EditText) findViewById(R.id.Co_passenger2_Id_proof_number);
        Co_passenger3_Id_proof_number = (EditText) findViewById(R.id.Co_passenger3_Id_proof_number);
        Co_passenger4_Id_proof_number = (EditText) findViewById(R.id.Co_passenger4_Id_proof_number);
        Co_passenger5_Id_proof_number = (EditText) findViewById(R.id.Co_passenger5_Id_proof_number);

        SharedPreferences preference3 = getApplicationContext().getSharedPreferences("Sources", MODE_PRIVATE);
        SourceName=preference3.getString("FROMNAME", null);
        DestinationName=preference3.getString("TONAME", null);
        SourceId=preference3.getString("FROMID",null);
        DestinationId=preference3.getString("TOID",null);


        SharedPreferences preference1 = getApplicationContext().getSharedPreferences("OnwardJourneyDetails", MODE_PRIVATE);
        Operator=preference1.getString("OnwardOperator",null);
        BusType=preference1.getString("OnwardBusType",null);
        BusTypeid=preference1.getString("OnwardBusTypeID",null);
        departureTime=preference1.getString("ArraiavalTime",null);
        ArravaialTime=preference1.getString("OnwarddepartureTime",null);

        System.out.println(departureTime);
        System.out.println(ArravaialTime);

        CancellationPolicy=preference1.getString("OnwardCancellationPolicy",null);
        PartialCancellationAllowed=preference1.getString("OnwardPartialCancellationAllowed",null);
        Provider=preference1.getString("OnwardProvider",null);
        ObiboAPIProvidrer=preference1.getString("OnwardObiboAPIProvider", null);
        JourneyDate=preference1.getString("OnwardJourneyDate",null);
        SelectedSeats=preference1.getString("OnwardSelectedSeats",null);
        NumberOfSeats= preference1.getString("OnwardNumberOfSeats",null);
        BusTypeID=preference1.getString("OnwardBusTypeID", null);
        NetFare=preference1.getString("OnwardNetFare", null);
        ServiceTaxes=preference1.getString("OnwardServiceTaxes", null);
        OperatorserviceCharge=preference1.getString("OnwardOperatorserviceCharge", null);
        Fares=preference1.getString("OnwardFares", null);
        BoardingId=preference1.getString("DropingId",null);
        BoardingPointDetails=preference1.getString("DropingName",null);

        System.out.println("boarding---"+BoardingPointDetails+"-----"+BoardingId);

        OnwardDisplayname=preference1.getString("OnwardDisplayName",null);
        OnwardHotelname=preference1.getString("OnwardHotelName",null);
        OnwardReturnDate=preference1.getString("returnJourney",null);
        Ispackage=preference1.getString("Ispackage",null);
        OnwardReturnBoarding=preference1.getString("OnwardBoadingName",null);
        OnwardReturnBoardingID=preference1.getString("OnwardBoadingId",null);

        Main_Departure_time=preference1.getString("Main_Departure_time",null);
        isCOD=preference1.getBoolean("IsCOD",false);

        System.out.println("ReturnBoarding---"+OnwardReturnBoarding+"----"+OnwardReturnBoardingID);

        SharedPreferences preference = getApplicationContext().getSharedPreferences("JourneyDetails", MODE_PRIVATE);
        ReturnOperator=preference.getString("Operator",null);
        ReturnBusType=preference.getString("BusType",null);
        ReturnBusTypeid=preference.getString("BusTypeID",null);
        ReturndepartureTime=preference.getString("departureTime",null);
        ReturnCancellationPolicy=preference.getString("CancellationPolicy",null);
        ReturnPartialCancellationAllowed=preference.getString("PartialCancellationAllowed",null);
        ReturnProvider=preference.getString("Provider",null);
        ReturnObiboAPIProvidrer=preference.getString("ObiboAPIProvider", null);
        ReturnJourneyDate=preference.getString("JourneyDate",null);
        ReturnSelectedSeats=preference.getString("SelectedSeats",null);
        ReturnNumberOfSeats=preference.getString("NumberOfSeats",null);
        ReturnBusTypeID=preference.getString("BusTypeID", null);
        ReturnNetFare=preference.getString("NetFare", null);
        ReturnServiceTaxes=preference.getString("ServiceTaxes", null);
        ReturnOperatorserviceCharge=preference.getString("OperatorserviceCharge", null);
        ReturnFares=preference.getString("Fares", null);
        IsReturnSelected=preference.getString("IsReturnSelected", null);
        Returndate=preference.getString("PassengerRETURN_DATE", null);
        DisplayName=preference.getString("DisplayName", null);
        HotelName=preference.getString("HotelName",null);

        SharedPreferences preference2 = getApplicationContext().getSharedPreferences("BoardingPoint", MODE_PRIVATE);
        ReturnBoardingId=preference2.getString("BoadingId",null);
        ReturnBoardingPointDetails=preference2.getString("BoadingName",null);


        ContinueBooking.setText("Proceed to book "+NetFare+" SAR");


        ActualNetFare = NetFare;
        ActualReturnNetFare = ReturnNetFare;

        pref = getSharedPreferences("LoginPreference", MODE_PRIVATE);
        editor = pref.edit();
        userLogin= pref.getString("UserLOGIN", null);
        agentLogin=pref.getString("AgentLOGIN",null);
        if( userLogin!=null && userLogin.equals("Yes")){
            UserId="6";
            User_agentid=pref.getString("User_agentid", null);
            BookedBy=pref.getString("Clientname", null);
            BookedByEmail=pref.getString("Emailid", null);
            BookedByUserID=User_agentid;
        }else if(agentLogin!=null && agentLogin.equals("Yes")){
            UserId="4";
            User_agentid=pref.getString("User_agentid", null);
            BookedBy=pref.getString("Clientname", null);
            BookedByEmail=pref.getString("Emailid", null);
            BookedByUserID=User_agentid;
            PromoCodeCardView.setVisibility(View.GONE);
            payment_mode_radio_grp.setVisibility(View.GONE);
            //landmark_card.setVisibility(View.GONE);
        }else{
            UserId="5";
            User_agentid=pref.getString("SuperAdmin", null);
            BookedBy="Guest";
            BookedByEmail="";
            BookedByUserID="";
        }
        System.out.println("ObiboAPIProvidrer"+ObiboAPIProvidrer+"SelectedSeats"+SelectedSeats+"NumberOfSeats"+NumberOfSeats+"BusTypeID"+BusTypeID+"NetFare"+NetFare);
        if(NumberOfSeats.equals("1")){
            //address.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        }else if(NumberOfSeats.equals("2")){
            Co_Passenger1.setVisibility(View.VISIBLE);
            //Co_passenger_nationality1.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        }else if(NumberOfSeats.equals("3")){
            Co_Passenger1.setVisibility(View.VISIBLE);
            Co_Passenger2.setVisibility(View.VISIBLE);
            //Co_passenger_nationality2.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        }else if(NumberOfSeats.equals("4")){
            Co_Passenger1.setVisibility(View.VISIBLE);
            Co_Passenger2.setVisibility(View.VISIBLE);
            Co_Passenger3.setVisibility(View.VISIBLE);
            //Co_passenger_nationality3.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        }else if(NumberOfSeats.equals("5")){
            Co_Passenger1.setVisibility(View.VISIBLE);
            Co_Passenger2.setVisibility(View.VISIBLE);
            Co_Passenger3.setVisibility(View.VISIBLE);
            Co_Passenger4.setVisibility(View.VISIBLE);
            //Co_passenger_nationality4.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        }else if(NumberOfSeats.equals("6")){
            Co_Passenger1.setVisibility(View.VISIBLE);
            Co_Passenger2.setVisibility(View.VISIBLE);
            Co_Passenger3.setVisibility(View.VISIBLE);
            Co_Passenger4.setVisibility(View.VISIBLE);
            Co_Passenger5.setVisibility(View.VISIBLE);
            //Co_passenger_nationality5.setImeOptions(EditorInfo.IME_ACTION_DONE | EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        }

        Title = (Spinner) findViewById(R.id.Titles);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, Titles);
        Title.setAdapter(adapter);
        Title.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = Title.getSelectedItemPosition();
                title = Titles[+position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        CopassengerTitle1 = (Spinner) findViewById(R.id.CoPassengerTitles1);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, Titles);
        CopassengerTitle1.setAdapter(adapter1);
        CopassengerTitle1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = CopassengerTitle1.getSelectedItemPosition();
                CTitle1 = Titles[+position];


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        CopassengerTitle2 = (Spinner) findViewById(R.id.CoPassengerTitles2);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, Titles);
        CopassengerTitle2.setAdapter(adapter2);
        CopassengerTitle2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = CopassengerTitle2.getSelectedItemPosition();
                CTitle2 = Titles[+position];


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        CopassengerTitle3 = (Spinner) findViewById(R.id.CoPassengerTitles3);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, Titles);
        CopassengerTitle3.setAdapter(adapter3);
        CopassengerTitle3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = CopassengerTitle3.getSelectedItemPosition();
                CTitle3 = Titles[+position];


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        CopassengerTitle4 = (Spinner) findViewById(R.id.CoPassengerTitles4);
        ArrayAdapter<String> adapter4 = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, Titles);
        CopassengerTitle4.setAdapter(adapter4);
        CopassengerTitle4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = CopassengerTitle4.getSelectedItemPosition();
                CTitle4 = Titles[+position];


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        CopassengerTitle5 = (Spinner) findViewById(R.id.CoPassengerTitles5);
        ArrayAdapter<String> adapter5 = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, Titles);
        CopassengerTitle5.setAdapter(adapter5);
        CopassengerTitle5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = CopassengerTitle5.getSelectedItemPosition();
                CTitle5 = Titles[+position];


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        TravellerId = (Spinner) findViewById(R.id.Traveller_id_proof);
        ArrayAdapter<String> Travelleradapter = new ArrayAdapter<String>(this, R.layout.spinner_dropdown_item, IdProof){
            @Override
            public boolean isEnabled(int position) {
                if(position == 0){
                    return false;
                }else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                try {
                    TextView textView = (TextView) view;
                    if (position == 0) {
                        textView.setTextColor(ContextCompat.getColor(PassengerDetails.this, R.color.ColorDarkGray));
                    } else {
                        textView.setTextColor(ContextCompat.getColor(PassengerDetails.this, R.color.colorBlock));
                    }
                }catch (Exception e){}
                return view;
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                try {
                    TextView textView = (TextView) view;
                    if (position == 0) {
                        textView.setTextColor(ContextCompat.getColor(PassengerDetails.this, R.color.ColorDarkGray));
                    } else {
                        textView.setTextColor(ContextCompat.getColor(PassengerDetails.this, R.color.colorBlock));
                    }
                }catch (Exception e){}
                return view;
            }
        };
        TravellerId.setAdapter(Travelleradapter);
        TravellerId.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = TravellerId.getSelectedItemPosition();
                if(position>0) {
                    IDs = IdProof[+position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        Co_passenger1_id_proof = (Spinner) findViewById(R.id.Co_passenger1_id_proof);
        //ArrayAdapter<String> Co_passenger1idproof = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, IdProof);
        //Co_passenger1_id_proof.setAdapter(Co_passenger1idproof);
        Co_passenger1_id_proof.setAdapter(Travelleradapter);
        Co_passenger1_id_proof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = Co_passenger1_id_proof.getSelectedItemPosition();
                //   Copassenger1Id = IdProofID[+position];
                Copassenger1Id = Co_passenger1_id_proof.getSelectedItem().toString();
                String text = Co_passenger1_id_proof.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        Co_passenger2_id_proof = (Spinner) findViewById(R.id.Co_passenger2_id_proof);
        //ArrayAdapter<String> Co_passenger2idproof = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, IdProof);
        //Co_passenger2_id_proof.setAdapter(Co_passenger2idproof);
        Co_passenger2_id_proof.setAdapter(Travelleradapter);
        Co_passenger2_id_proof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = Co_passenger2_id_proof.getSelectedItemPosition();
                //    Copassenger2Id = IdProofID[+position];
                Copassenger2Id = Co_passenger2_id_proof.getSelectedItem().toString();
                String text = Co_passenger2_id_proof.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        Co_passenger3_id_proof = (Spinner) findViewById(R.id.Co_passenger3_id_proof);
        //ArrayAdapter<String> Co_passenger3idproof = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, IdProof);
        //Co_passenger3_id_proof.setAdapter(Co_passenger3idproof);
        Co_passenger3_id_proof.setAdapter(Travelleradapter);
        Co_passenger3_id_proof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = Co_passenger3_id_proof.getSelectedItemPosition();
                //   Copassenger3Id = IdProofID[+position];
                Copassenger3Id = Co_passenger3_id_proof.getSelectedItem().toString();
                String text = Co_passenger3_id_proof.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        Co_passenger4_id_proof = (Spinner) findViewById(R.id.Co_passenger4_id_proof);
        //ArrayAdapter<String> Co_passenger4idproof = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, IdProof);
        //Co_passenger4_id_proof.setAdapter(Co_passenger4idproof);
        Co_passenger4_id_proof.setAdapter(Travelleradapter);
        Co_passenger4_id_proof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = Co_passenger4_id_proof.getSelectedItemPosition();
                //      Copassenger4Id = IdProofID[+position];
                Copassenger4Id = Co_passenger4_id_proof.getSelectedItem().toString();
                String text = Co_passenger4_id_proof.getSelectedItem().toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        Co_passenger5_id_proof = (Spinner) findViewById(R.id.Co_passenger5_id_proof);
        //ArrayAdapter<String> Co_passenger5idproof = new ArrayAdapter<>(this, R.layout.spinner_dropdown_item, IdProof);
        //Co_passenger5_id_proof.setAdapter(Co_passenger5idproof);
        Co_passenger5_id_proof.setAdapter(Travelleradapter);
        Co_passenger5_id_proof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long ard3) {
                int position = Co_passenger5_id_proof.getSelectedItemPosition();
                //    Copassenger5Id = IdProofID[+position];
                Copassenger5Id = Co_passenger5_id_proof.getSelectedItem().toString();
                String text = Co_passenger1_id_proof.getSelectedItem().toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        if (UserId.equals("5")||UserId.equals("6")){
       /*     if (isCOD){
                cash_on_delivery.setVisibility(View.VISIBLE);
                callgetcurrenttime();
            }else {
                cash_on_delivery.setVisibility(View.INVISIBLE);
                payment_mode_radio_grp.setVisibility(View.GONE);
            }*/

            callgetcurrenttime();
        }



        payment_mode_radio_grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (cash_on_delivery.isChecked()){
                    PromoCodeCardView.setVisibility(View.GONE);
                    //landmark_card.setVisibility(View.VISIBLE);
                    Toast.makeText(PassengerDetails.this,"Enter land mark",Toast.LENGTH_SHORT).show();
                    NetFare = ActualNetFare;
                    ReturnNetFare = ActualReturnNetFare;
                    int ActualTotalFare;
                    if (IsReturnSelected.equals("Yes")) {
                        int TotalFareint = RoundOff(Double.parseDouble(ActualNetFare));
                        int ReturnTotalFareint = RoundOff(Double.parseDouble(ActualReturnNetFare));
                        ActualTotalFare = (TotalFareint + ReturnTotalFareint);
                    } else {
                        ActualTotalFare = RoundOff(Double.parseDouble(ActualNetFare));
                    }
                    ContinueBooking.setText("Proceed to book SAR " + ActualTotalFare + "");
                    ValidPromoCode="";
                }else {
                    Promocode.setText("");
                    PromoCodeCardView.setVisibility(View.VISIBLE);
                    PrompromoCodeApplyLayoutocode.setVisibility(View.VISIBLE);
                    promocodeAmountDetails.setVisibility(View.GONE);
                    //landmark_card.setVisibility(View.GONE);
                }
            }
        });

        ContinueBooking.setOnClickListener(this);
    }


    @Override
    protected void onStart() {
        super.onStart();

        passenger_nationality.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent i = new Intent(PassengerDetails.this,NationalityActivity.class);
                    i.putExtra("which","0");
                    startActivityForResult(i,mREQUEST_CODE);
                }
                return true;
            }
        });

        Co_passenger_nationality1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent i = new Intent(PassengerDetails.this,NationalityActivity.class);
                    i.putExtra("which","1");
                    startActivityForResult(i,mREQUEST_CODE);
                }
                return true;
            }
        });
        Co_passenger_nationality2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent i = new Intent(PassengerDetails.this,NationalityActivity.class);
                    i.putExtra("which","2");
                    startActivityForResult(i,mREQUEST_CODE);
                }
                return true;
            }
        });
        Co_passenger_nationality3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent i = new Intent(PassengerDetails.this,NationalityActivity.class);
                    i.putExtra("which","3");
                    startActivityForResult(i,mREQUEST_CODE);
                }
                return true;
            }
        });
        Co_passenger_nationality4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent i = new Intent(PassengerDetails.this,NationalityActivity.class);
                    i.putExtra("which","4");
                    startActivityForResult(i,mREQUEST_CODE);
                }
                return true;
            }
        });
        Co_passenger_nationality5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    Intent i = new Intent(PassengerDetails.this,NationalityActivity.class);
                    i.putExtra("which","5");
                    startActivityForResult(i,mREQUEST_CODE);
                }
                return true;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == mREQUEST_CODE && resultCode == RESULT_OK) {
            String which = data.getStringExtra("which");
            String nationality = data.getStringExtra("nationality");
            if(which.equals("0")){
                passenger_nationality.setText(nationality,TextView.BufferType.EDITABLE);
            }else if(which.equals("1")){
                Co_passenger_nationality1.setText(nationality,TextView.BufferType.EDITABLE);
            }else if(which.equals("2")){
                Co_passenger_nationality2.setText(nationality,TextView.BufferType.EDITABLE);
            }else if(which.equals("3")){
                Co_passenger_nationality3.setText(nationality,TextView.BufferType.EDITABLE);
            }else if(which.equals("4")){
                Co_passenger_nationality4.setText(nationality,TextView.BufferType.EDITABLE);
            }else if(which.equals("5")){
                Co_passenger_nationality5.setText(nationality,TextView.BufferType.EDITABLE);
            }
        }
        NationalityActivity.hideKeyboard(this);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void callgetcurrenttime(){
        if (ServiceHandler.isNetworkAvailable(PassengerDetails.this)){
            ServiceHandler.getCureenttime(this,Constants.GETCURRENTTIME);
        }
    }

    public void getcureenttime(String response) {
        if (response != null){
            String currenttime=ServiceHandler.getResponseMessage(response);
            currenttime=getdate(currenttime);
            String Journeydate_Time=JourneyDate+" "+Main_Departure_time;
            long hours=hoursBetween(getDate(Journeydate_Time),getDate(currenttime));
            System.out.println("Hours Between "+hours);
            if (hours<1){
                showalertafterdeparture();
            /*    IsCod_afterclick=false;
                payment_mode_radio_grp.setVisibility(View.GONE);
                cash_on_delivery.setVisibility(View.INVISIBLE);
                landmark_card.setVisibility(View.GONE);
                payment_gateway.setChecked(true);*/
            }else {
                IsCod_afterclick=true;
                cash_on_delivery.setVisibility(View.VISIBLE);
            }
        }
    }

    public void callgetcurrentafterclick(){
        if (ServiceHandler.isNetworkAvailable(PassengerDetails.this)){
            ServiceHandler.getcurrentafterclick(this,Constants.GETCURRENTTIME);
        }
    }

    public void getcurrentafterclick(String response) {
        if (response != null){
            String currenttime=ServiceHandler.getResponseMessage(response);
            currenttime=getdate(currenttime);
            String Journeydate_Time=JourneyDate+" "+Main_Departure_time;
            long hours=hoursBetween(getDate(Journeydate_Time),getDate(currenttime));
            System.out.println("Hours Between "+hours);
            if (hours<1){
                payment_mode_radio_grp.setVisibility(View.GONE);
                cash_on_delivery.setVisibility(View.INVISIBLE);
                //landmark_card.setVisibility(View.GONE);
                cash_on_delivery.setChecked(false);
                showalertafterdeparture();
               /* final Context context = PassengerDetails.this;
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setTitle("Cash on delivery is not available before 1 hour of departure time");
                    alertDialogBuilder
                            .setMessage("")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    IsCod_afterclick=false;
                                    Intent ip=new Intent(PassengerDetails.this,MainActivity.class);
                                    startActivity(ip);
                                }
                            });
                            *//*.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    IsCod_afterclick=false;
                                    Intent ip=new Intent(PassengerDetails.this,MainActivity.class);
                                    startActivity(ip);
                                }
                            });*//*
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();*/

            }else {
                IsCod_afterclick=true;
                cash_on_delivery.setVisibility(View.VISIBLE);
                callBlocking();
            }
        }
    }

     private void showalertafterdeparture(){
         AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PassengerDetails.this);
         alertDialogBuilder.setTitle("Cash on delivery not available for this bus");
         alertDialogBuilder
                 .setMessage("Try phone booking instead\n" +
                         "\n" +
                         "English/Urdu: 0542777292\n" +
                         "Arabic: 0591179978")
                 .setCancelable(false)
                 .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int id) {
                         dialog.cancel();
                         IsCod_afterclick=false;
                         Intent ip=new Intent(PassengerDetails.this,MainActivity.class);
                         startActivity(ip);
                     }
                 });
                            /*.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    IsCod_afterclick=false;
                                    Intent ip=new Intent(PassengerDetails.this,MainActivity.class);
                                    startActivity(ip);
                                }
                            });*/
         AlertDialog alertDialog = alertDialogBuilder.create();
         alertDialog.show();
     }


    public String getdate(String date){
        String[] items1 = date.split("/");
        String m1=items1[0];
        String d1=items1[1];
        String y1=items1[2];
        String tim[]=y1.split(" ");
        String Ye=tim[0];
        String Time=tim[1];
        String Time_Split[] =Time.split(":");
        String Time1=Time_Split[0];
        String Time2=Time_Split[1];
        String TimeAm_pm=tim[2];
       String Total_Time=converttime(Integer.parseInt(Time1))+":"+converttime(Integer.parseInt(Time2))+" "+TimeAm_pm;
       int Day = Integer.parseInt(d1);
       int Month = Integer.parseInt(m1);
       int Year = Integer.parseInt(Ye);
       return String.format("%02d", Day)+"-"+String.format("%02d", Month)+"-"+Year+" "+Total_Time;
    }

    public String converttime(int input) {
        if (input >= 10) {
            return String.valueOf(input);
        } else {
            return "0" + String.valueOf(input);
        }
    }

    private String coverttime24Hours(String _24HourTime){
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
            Date _24HourDt = _12HourSDF.parse(_24HourTime);
            _24HourTime=_24HourSDF.format(_24HourDt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _24HourTime;
    }
    private static long hoursBetween(Date one, Date two) {
        long difference = (one.getTime()-two.getTime())/3600000;
        return Math.abs(difference);
    }

    private Date getDate(String checkIn) {
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a",Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(checkIn);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }



    private JSONObject getBookedDetails(){
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("BookedBy",BookedBy);
            jsonBody.put("BookedByEmail",BookedByEmail);
            jsonBody.put("BookedByUserID",BookedByUserID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonBody;
    }

    @Override
    public void onClick(View v) {
        if (v == PromoCodeApplay) {
            ValidPromoCode = Promocode.getText().toString();
            if (!ValidPromoCode.equalsIgnoreCase("")) {
                new ValidPromoCodeAppaly().execute();

            } else {
                Promocode.setError("Enter Promo code");
            }
        }
        if(v==Male){
            Male.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Male.setTextColor(getResources().getColor(R.color.colorWhite));
            Female.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Female.setTextColor(getResources().getColor(R.color.colorPrimary));
            gender="M";
        }
        if(v==Female){
            Male.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Male.setTextColor(getResources().getColor(R.color.colorPrimary));
            Female.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Female.setTextColor(getResources().getColor(R.color.colorWhite));
            gender="F";
        }
        if(v==Co_Passenger_Male1){
            Co_Passenger_Male1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Male1.setTextColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Female1.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Female1.setTextColor(getResources().getColor(R.color.colorPrimary));
            gender1="M";
        }
        if(v==Co_Passenger_Female1){
            Co_Passenger_Male1.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Male1.setTextColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Female1.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Female1.setTextColor(getResources().getColor(R.color.colorWhite));
            gender1="F";
        }
        if(v==Co_Passenger_Male2){
            Co_Passenger_Male2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Male2.setTextColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Female2.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Female2.setTextColor(getResources().getColor(R.color.colorPrimary));
            gender2="M";
        }
        if(v==Co_Passenger_Female2){
            Co_Passenger_Male2.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Male2.setTextColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Female2.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Female2.setTextColor(getResources().getColor(R.color.colorWhite));
            gender2="F";
        }
        if(v==Co_Passenger_Male3){
            Co_Passenger_Male3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Male3.setTextColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Female3.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Female3.setTextColor(getResources().getColor(R.color.colorPrimary));
            gender3="M";
        }
        if(v==Co_Passenger_Female3){
            Co_Passenger_Male3.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Male3.setTextColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Female3.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Female3.setTextColor(getResources().getColor(R.color.colorWhite));
            gender3="F";
        }
        if(v==Co_Passenger_Male4){
            Co_Passenger_Male4.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Male4.setTextColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Female4.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Female4.setTextColor(getResources().getColor(R.color.colorPrimary));
            gender4="M";
        }
        if(v==Co_Passenger_Female4){
            Co_Passenger_Male4.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Male4.setTextColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Female4.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Female4.setTextColor(getResources().getColor(R.color.colorWhite));
            gender4="F";
        } if(v==Co_Passenger_Male5){
            Co_Passenger_Male5.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Male5.setTextColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Female5.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Female5.setTextColor(getResources().getColor(R.color.colorPrimary));
            gender5="M";
        }
        if(v==Co_Passenger_Female5){
            Co_Passenger_Male5.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            Co_Passenger_Male5.setTextColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Female5.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            Co_Passenger_Female5.setTextColor(getResources().getColor(R.color.colorWhite));
            gender5="F";
        }
        if (v == ContinueBooking) {
            if(UserId.equals("5")||UserId.equals("6")){
                //if (isCOD){
                    if (cash_on_delivery.isChecked()){
                        callgetcurrentafterclick();
                    }else {
                        IsCod_afterclick=true;
                        callBlocking();
                    }
                /*}else {
                    IsCod_afterclick=true;
                    callBlocking();
                }*/
            }else {
                IsCod_afterclick=true;
                callBlocking();
            }
        }
    }

    public void  callBlocking(){
        //if (validatelandmark()) {
            if (IsCod_afterclick){
                int length;
                FullName = passenger_fullname.getText().toString();
                EMAIL = contact_email.getText().toString();
                MOBILE = contact_mobile.getText().toString();
                NATIONALITY = passenger_nationality.getText().toString();
                //Address = address.getText().toString();
                IdProofNumber = Idnumber.getText().toString();

                Co_passenger_fullname_1 = Co_passenger_fullname1.getText().toString();
                Co_passenger_fullname_2 = Co_passenger_fullname2.getText().toString();
                Co_passenger_fullname_3 = Co_passenger_fullname3.getText().toString();
                Co_passenger_fullname_4 = Co_passenger_fullname4.getText().toString();
                Co_passenger_fullname_5 = Co_passenger_fullname5.getText().toString();

                Co_passenger_nationality_1 = Co_passenger_nationality1.getText().toString();
                Co_passenger_nationality_2 = Co_passenger_nationality2.getText().toString();
                Co_passenger_nationality_3 = Co_passenger_nationality3.getText().toString();
                Co_passenger_nationality_4 = Co_passenger_nationality4.getText().toString();
                Co_passenger_nationality_5 = Co_passenger_nationality5.getText().toString();

                Co_passenger1_id_proof_number = Co_passenger1_Id_proof_number.getText().toString();
                Co_passenger2_id_proof_number = Co_passenger2_Id_proof_number.getText().toString();
                Co_passenger3_id_proof_number = Co_passenger3_Id_proof_number.getText().toString();
                Co_passenger4_id_proof_number = Co_passenger4_Id_proof_number.getText().toString();
                Co_passenger5_id_proof_number = Co_passenger5_Id_proof_number.getText().toString();


                length = MOBILE.length();
                if (!FullName.equals("") && !NATIONALITY.equals("") && /*!Address.equals("") && */!IdProofNumber.equals("") && !MOBILE.equals("") && (length == 9) && !EMAIL.equals("") && EMAIL.matches(Constants.emailPattern)) {
                    // new BlockTicket().execute();
                    if (NumberOfSeats.equals("1")) {
                        Passenger_Full_name = "" + FullName;
                        Passenger_Nationality = "" + NATIONALITY;
                        Passenger_gender = "" + gender;
                        TotalTitles = "" + title;
                        IdCard = "" + IDs;
                        checkConnection();
                    }
                    if (NumberOfSeats.equals("2")) {
                        length = Co_passenger_fullname_1.length();
                        if (!Co_passenger_fullname_1.equals("") && !Co_passenger_nationality_1.equals("") && !Co_passenger1_id_proof_number.equals("")) {
                            Passenger_Full_name = "" + FullName + "~" + Co_passenger_fullname_1;
                            Passenger_Nationality = "" + NATIONALITY + "~" + Co_passenger_nationality_1;
                            Passenger_gender = "" + gender + "~" + gender1;
                            TotalTitles = "" + title + "~" + CTitle1;
                            IdCard = "" + IDs + "~" + Copassenger1Id;
                            IdProofNumber = "" + IdProofNumber + "~" + Co_passenger1_id_proof_number;
                            System.out.println("Number of seats 2" + FullName + NATIONALITY);
                            checkConnection();

                        } else {
                            if (Co_passenger_fullname_1.equals("") || Co_passenger_nationality_1.equals("") || Co_passenger1_id_proof_number.equals("")) {
                                if (Co_passenger_fullname_1.equals("")) {
                                    Co_passenger_fullname1.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_1.equals("")) {
                                    //Co_passenger_nationality1.setError("Enter Valid Age");
                                }
                                if (Co_passenger1_id_proof_number.equals("")) {
                                    Co_passenger1_Id_proof_number.requestFocus();
                                    Co_passenger1_Id_proof_number.setError("Enter Valid Id Number");
                                }
                            }

                        }
                    } else if (NumberOfSeats.equals("3")) {
                        length = Co_passenger_fullname_1.length();
                        if (!Co_passenger_fullname_1.equals("") && !Co_passenger_nationality_1.equals("") && !Co_passenger1_id_proof_number.equals("") && !Co_passenger_fullname_2.equals("") && !Co_passenger_nationality_2.equals("") && !Co_passenger2_id_proof_number.equals("")) {
                            Passenger_Full_name = "" + FullName + "~" + Co_passenger_fullname_1 + "~" + Co_passenger_fullname_2;
                            Passenger_Nationality = "" + NATIONALITY + "~" + Co_passenger_nationality_1 + "~" + Co_passenger_nationality_2;
                            Passenger_gender = "" + gender + "~" + gender1 + "~" + gender2;
                            IdCard = "" + IDs + "~" + Copassenger1Id + "~" + Copassenger2Id;
                            IdProofNumber = "" + IdProofNumber + "~" + Co_passenger1_id_proof_number + "~" + Co_passenger2_id_proof_number;
                            TotalTitles = "" + title + "~" + CTitle1 + "~" + CTitle2;
                            System.out.println("Number of seats 2" + FullName + NATIONALITY);
                            checkConnection();

                        } else {
                            if (Co_passenger_fullname_1.equals("") || Co_passenger_nationality_1.equals("") || Co_passenger1_id_proof_number.equals("") || Co_passenger_fullname_2.equals("") || Co_passenger_nationality_2.equals("") || Co_passenger2_id_proof_number.equals("")) {
                                if (Co_passenger_fullname_1.equals("")) {
                                    Co_passenger_fullname1.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_1.equals("")) {
                                    //Co_passenger_nationality1.setError("Enter Valid Age");
                                }
                                if (Co_passenger1_id_proof_number.equals("")) {
                                    Co_passenger1_Id_proof_number.setError("Enter Valid Id Number");
                                }
                                if (Co_passenger_fullname_2.equals("")) {
                                    Co_passenger_fullname2.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_2.equals("")) {
                                    //Co_passenger_nationality2.setError("Enter Valid Age");
                                }
                                if (Co_passenger2_id_proof_number.equals("")) {
                                    Co_passenger2_Id_proof_number.requestFocus();
                                    Co_passenger2_Id_proof_number.setError("Enter Valid Id Number");
                                }
                            }

                        }
                    } else if (NumberOfSeats.equals("4")) {
                        length = Co_passenger_fullname_1.length();
                        if (!Co_passenger_fullname_1.equals("") && !Co_passenger_nationality_1.equals("") && !Co_passenger1_id_proof_number.equals("") && !Co_passenger_fullname_2.equals("") && !Co_passenger_nationality_2.equals("") && !Co_passenger2_id_proof_number.equals("") && !Co_passenger_fullname_3.equals("") && !Co_passenger_nationality_3.equals("") && !Co_passenger3_id_proof_number.equals("")) {
                            Passenger_Full_name = "" + FullName + "~" + Co_passenger_fullname_1 + "~" + Co_passenger_fullname_2 + "~" + Co_passenger_fullname_3;
                            Passenger_Nationality = "" + NATIONALITY + "~" + Co_passenger_nationality_1 + "~" + Co_passenger_nationality_2 + "~" + Co_passenger_nationality_3;
                            Passenger_gender = "" + gender + "~" + gender1 + "~" + gender2 + "~" + gender3;
                            IdCard = "" + IDs + "~" + Copassenger1Id + "~" + Copassenger2Id + "~" + Copassenger3Id;
                            IdProofNumber = "" + IdProofNumber + "~" + Co_passenger1_id_proof_number + "~" + Co_passenger2_id_proof_number + "~" + Co_passenger3_id_proof_number;
                            TotalTitles = "" + title + "~" + CTitle1 + "~" + CTitle2 + "~" + CTitle3;
                            checkConnection();

                        } else {
                            if (Co_passenger_fullname_1.equals("") || Co_passenger_nationality_1.equals("") || Co_passenger1_id_proof_number.equals("") || Co_passenger_fullname_2.equals("") || Co_passenger_nationality_2.equals("") || Co_passenger2_id_proof_number.equals("") || Co_passenger_fullname_3.equals("") || Co_passenger_nationality_3.equals("") || Co_passenger3_id_proof_number.equals("")) {
                                if (Co_passenger_fullname_1.equals("")) {
                                    Co_passenger_fullname1.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_1.equals("")) {
                                    //Co_passenger_nationality1.setError("Enter Valid Age");
                                }
                                if (Co_passenger_fullname_2.equals("")) {
                                    Co_passenger_fullname2.setError("Enter Name");
                                }
                                if (Co_passenger1_id_proof_number.equals("")) {
                                    Co_passenger1_Id_proof_number.setError("Enter Valid Id Number");
                                }
                                if (Co_passenger_nationality_2.equals("")) {
                                    //Co_passenger_nationality2.setError("Enter Valid Age");
                                }
                                if (Co_passenger2_id_proof_number.equals("")) {
                                    Co_passenger2_Id_proof_number.setError("Enter Valid Id Number");
                                }
                                if (Co_passenger_fullname_3.equals("")) {
                                    Co_passenger_fullname3.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_3.equals("")) {
                                    //Co_passenger_nationality3.setError("Enter Valid Age");
                                }
                                if (Co_passenger3_id_proof_number.equals("")) {
                                    Co_passenger3_Id_proof_number.requestFocus();
                                    Co_passenger3_Id_proof_number.setError("Enter Valid Id Number");
                                }
                            }

                        }
                    } else if (NumberOfSeats.equals("5")) {
                        if (!Co_passenger_fullname_1.equals("") && !Co_passenger_nationality_1.equals("") && !Co_passenger1_id_proof_number.equals("") && !Co_passenger_fullname_2.equals("") && !Co_passenger_nationality_2.equals("") && !Co_passenger2_id_proof_number.equals("") && !Co_passenger_fullname_3.equals("") && !Co_passenger_nationality_3.equals("") && !Co_passenger3_id_proof_number.equals("")
                                && !Co_passenger_fullname_4.equals("") && !Co_passenger_nationality_4.equals("") && !Co_passenger4_id_proof_number.equals("")) {
                            Passenger_Full_name = "" + FullName + "~" + Co_passenger_fullname_1 + "~" + Co_passenger_fullname_2 + "~" + Co_passenger_fullname_3 + "~" + Co_passenger_fullname_4;
                            Passenger_Nationality = "" + NATIONALITY + "~" + Co_passenger_nationality_1 + "~" + Co_passenger_nationality_2 + "~" + Co_passenger_nationality_3 + "~" + Co_passenger_nationality_4;
                            Passenger_gender = "" + gender + "~" + gender1 + "~" + gender2 + "~" + gender3 + "~" + gender4;
                            TotalTitles = "" + title + "~" + CTitle1 + "~" + CTitle2 + "~" + CTitle3 + "~" + CTitle4;
                            IdCard = "" + IDs + "~" + Copassenger1Id + "~" + Copassenger2Id + "~" + Copassenger3Id + "~" + Copassenger4Id;
                            IdProofNumber = "" + IdProofNumber + "~" + Co_passenger1_id_proof_number + "~" + Co_passenger2_id_proof_number + "~" + Co_passenger3_id_proof_number + "~" + Co_passenger4_id_proof_number;
                            checkConnection();

                        } else {
                            if (Co_passenger_fullname_1.equals("") || Co_passenger_nationality_1.equals("") || Co_passenger1_id_proof_number.equals("") || Co_passenger_fullname_2.equals("") || Co_passenger_nationality_2.equals("") || Co_passenger2_id_proof_number.equals("") || Co_passenger_fullname_3.equals("") || Co_passenger_nationality_3.equals("") || Co_passenger1_id_proof_number.equals("") || Co_passenger_fullname_4.equals("") || Co_passenger_nationality_4.equals("") || Co_passenger4_id_proof_number.equals("")) {
                                if (Co_passenger_fullname_1.equals("")) {
                                    Co_passenger_fullname1.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_1.equals("")) {
                                    //Co_passenger_nationality1.setError("Enter Valid Age");
                                }
                                if (Co_passenger_fullname_2.equals("")) {
                                    Co_passenger_fullname2.setError("Enter Name");
                                }
                                if (Co_passenger1_id_proof_number.equals("")) {
                                    Co_passenger1_Id_proof_number.setError("Enter Valid Id Number");
                                }
                                if (Co_passenger_nationality_2.equals("")) {
                                    //Co_passenger_nationality2.setError("Enter Valid Age");
                                }
                                if (Co_passenger2_id_proof_number.equals("")) {
                                    Co_passenger2_Id_proof_number.setError("Enter Valid Id Number");
                                }
                                if (Co_passenger_fullname_3.equals("")) {
                                    Co_passenger_fullname3.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_3.equals("")) {
                                    //Co_passenger_nationality3.setError("Enter Valid Age");
                                }
                                if (Co_passenger3_id_proof_number.equals("")) {
                                    Co_passenger3_Id_proof_number.setError("Enter Valid Id Number");
                                }
                                if (Co_passenger_fullname_4.equals("")) {
                                    Co_passenger_fullname4.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_4.equals("")) {
                                    //Co_passenger_nationality4.setError("Enter Valid Age");
                                }
                                if (Co_passenger4_id_proof_number.equals("")) {
                                    Co_passenger4_Id_proof_number.requestFocus();
                                    Co_passenger4_Id_proof_number.setError("Enter Valid Id Number");
                                }


                            }

                        }
                    } else if (NumberOfSeats.equals("6")) {
                        if (!Co_passenger_fullname_1.equals("") && !Co_passenger_nationality_1.equals("") && !Co_passenger1_id_proof_number.equals("") && !Co_passenger_fullname_2.equals("") && !Co_passenger_nationality_2.equals("") && !Co_passenger2_id_proof_number.equals("") && !Co_passenger_fullname_3.equals("") && !Co_passenger_nationality_3.equals("") && !Co_passenger3_id_proof_number.equals("")
                                && !Co_passenger_fullname_4.equals("") && !Co_passenger_nationality_4.equals("") && !Co_passenger4_id_proof_number.equals("") && !Co_passenger_fullname_5.equals("") && !Co_passenger_nationality_5.equals("") && !Co_passenger5_id_proof_number.equals("")) {
                            Passenger_Full_name = "" + FullName + "~" + Co_passenger_fullname_1 + "~" + Co_passenger_fullname_2 + "~" + Co_passenger_fullname_3 + "~" + Co_passenger_fullname_4 + "~" + Co_passenger_fullname_5;
                            Passenger_Nationality = "" + NATIONALITY + "~" + Co_passenger_nationality_1 + "~" + Co_passenger_nationality_2 + "~" + Co_passenger_nationality_3 + "~" + Co_passenger_nationality_4 + "~" + Co_passenger_nationality_5;
                            Passenger_gender = "" + gender + "~" + gender1 + "~" + gender2 + "~" + gender3 + "~" + gender4 + "~" + gender5;
                            TotalTitles = "" + title + "~" + CTitle1 + "~" + CTitle2 + "~" + CTitle3 + "~" + CTitle4 + "~" + CTitle5;
                            IdProofNumber = "" + IdProofNumber + "~" + Co_passenger1_id_proof_number + "~" + Co_passenger2_id_proof_number + "~" + Co_passenger3_id_proof_number + "~" + Co_passenger4_id_proof_number + "~" + Co_passenger5_id_proof_number;
                            IdCard = "" + IDs + "~" + Copassenger1Id + "~" + Copassenger2Id + "~" + Copassenger3Id + "~" + Copassenger4Id + "~" + Copassenger5Id;
                            checkConnection();

                        } else {
                            if (Co_passenger_fullname_1.equals("") || Co_passenger_nationality_1.equals("") || Co_passenger1_id_proof_number.equals("") || Co_passenger_fullname_2.equals("") || Co_passenger_nationality_2.equals("") || Co_passenger2_id_proof_number.equals("") || Co_passenger_fullname_3.equals("") || Co_passenger_nationality_3.equals("") || Co_passenger1_id_proof_number.equals("") || Co_passenger_fullname_4.equals("") || Co_passenger_nationality_4.equals("") || Co_passenger4_id_proof_number.equals("") ||
                                    Co_passenger_fullname_5.equals("") || Co_passenger_nationality_5.equals("") || Co_passenger5_id_proof_number.equals("")) {
                                if (Co_passenger_fullname_1.equals("")) {
                                    Co_passenger_fullname1.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_1.equals("")) {
                                    //Co_passenger_nationality1.setError("Enter Valid Age");
                                }
                                if (Co_passenger_fullname_2.equals("")) {
                                    Co_passenger_fullname2.setError("Enter Name");
                                }
                                if (Co_passenger1_id_proof_number.equals("")) {
                                    Co_passenger1_Id_proof_number.setError("Enter Valid Id Number");
                                }
                                if (Co_passenger_nationality_2.equals("")) {
                                    //Co_passenger_nationality2.setError("Enter Valid Age");
                                }
                                if (Co_passenger2_id_proof_number.equals("")) {
                                    Co_passenger2_Id_proof_number.setError("Enter Valid Id Number");
                                }
                                if (Co_passenger_fullname_3.equals("")) {
                                    Co_passenger_fullname3.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_3.equals("")) {
                                    //Co_passenger_nationality3.setError("Enter Valid Age");
                                }
                                if (Co_passenger3_id_proof_number.equals("")) {
                                    Co_passenger3_Id_proof_number.setError("Enter Valid Id Number");
                                }
                                if (Co_passenger_fullname_4.equals("")) {
                                    Co_passenger_fullname4.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_4.equals("")) {
                                    //Co_passenger_nationality4.setError("Enter Valid Age");
                                }
                                if (Co_passenger4_id_proof_number.equals("")) {
                                    Co_passenger4_Id_proof_number.setError("Enter Valid Id Number");
                                }
                                if (Co_passenger_fullname_5.equals("")) {
                                    Co_passenger_fullname5.setError("Enter Name");
                                }
                                if (Co_passenger_nationality_5.equals("")) {
                                    //Co_passenger_nationality5.setError("Enter Valid Age");
                                }
                                if (Co_passenger5_id_proof_number.equals("")) {
                                    Co_passenger5_Id_proof_number.requestFocus();
                                    Co_passenger5_Id_proof_number.setError("Enter Valid Id Number");
                                }


                            }

                        }
                    }
                } else {
                    if (FullName.equals("") || NATIONALITY.equals("") || NATIONALITY.equals("0") || /*Address.equals("") || */IdProofNumber.equals("") || MOBILE.equals("") || length < 10 || EMAIL.equals("") || !EMAIL.matches(Constants.emailPattern)) {
                        if (FullName.equals("")) {
                            passenger_fullname.setError("Enter Name");
                        }
                        if (NATIONALITY.equals("")) {
                            //passenger_nationality.setError("Enter Valid Age");
                        }
                        if (NATIONALITY.equals("0")) {
                            //passenger_nationality.setError("Enter Valid Age");
                        }
                        /*if (Address.equals("")) {
                            address.setError("Enter Address");
                        }*/
                        if (IdProofNumber.equals("")) {
                            Idnumber.setError("Enter Id Proof Number");
                        }
                        if (MOBILE.length() < 9) {
                            contact_mobile.setError("Enter Mobile No");
                        }
                        if (!EMAIL.matches(Constants.emailPattern)) {
                            contact_email.setError("Enter Valid Email");
                        }
                        if (EMAIL.equals("")) {
                            contact_email.setError("Enter Email");
                        }
                        if (length > 15) {
                            passenger_fullname.setError("Name Should not Exeeced 15 Characters");
                        }
                    }

                }


            }else {
                Toast.makeText(this,"Cash on delivery is available before 5 hours of departure time",Toast.LENGTH_LONG).show();
            }
        //}
    }


//    private boolean validatelandmark() {
//        if (cash_on_delivery.isChecked()){
//            if (UserId.equals("5")|| UserId.equals("6")){
//                if(!Constants.checkField(landmark_et)) {
//                    landmark_et.setError("Enter land mark");
//                    return false;
//                }
//            }
//
//        }
//        return true;
//    }



    ///////****** TO CHECKING PROMO CODE VALID OR NOT ITS VALID PROMOCODE TO CHECK DISSCOUNT AMOUNT *******////////
    public class ValidPromoCodeAppaly extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(PassengerDetails.this, "", "Please Wait...\n Validating Promo Code");
        }

        @Override
        protected String doInBackground(String... params) {
            String strurl = Constants.GETPROMOCODES;
            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(strurl);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpGet.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            httpGet.setHeader("Content-Type", "application/json");

            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            try {
                data = httpClient.execute(httpGet, responseHandler);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            try {

                if (!data.equals("") && !data.equals("null") && !data.contains("Message")) {
                    JSONArray PromocodeArray = new JSONArray(data);

                    int len = PromocodeArray.length();
                    for (int k = 0; k < len; k++) {
                        JSONObject object = PromocodeArray.getJSONObject(k);
                        HashMap<String, String> Ma = new HashMap<String, String>();
                        String Servicetype = object.getString("ServiceType");
                        if (Servicetype.equals("1")) {
                            Ma.put("Code", object.getString("Code"));
                            String Code=object.getString("Code");
                            if (Code.equals(ValidPromoCode)) {
                                Correct_Promo = "true";
                                String VaildFromDate = PromocodeArray.getJSONObject(k).getString("ValidFrom");
                                String VaildTillDate = PromocodeArray.getJSONObject(k).getString("ValidTill");
//                                String From=PromocodeArray.getJSONObject(k).getString("FromAmount");
//                                String To= PromocodeArray.getJSONObject(k).getString("ToAmount");
//                                int FromAmount=(int) Math.round(Double.parseDouble(From));
//                                int ToAmount=(int) Math.round(Double.parseDouble(To));
                                int Total = (int) Math.round(Double.parseDouble(NetFare));

                                SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
                                try {
                                    Date PromoCodeValidFromDate = output.parse(VaildFromDate);
                                    Date PromoCodeValidTillDate = output.parse(VaildTillDate);

                                    Calendar c = Calendar.getInstance();
                                    String CurrentDate = output.format((c.getTime()));
                                    Date todayDate = output.parse(CurrentDate);

                                    if (PromoCodeValidTillDate.after(todayDate) || todayDate.equals(PromoCodeValidTillDate) && PromoCodeValidFromDate.before(todayDate) || todayDate.equals(PromoCodeValidFromDate)) {
//                                        if (Total>=FromAmount&&Total<=ToAmount) {
                                            // In between
                                            String DiscountType = PromocodeArray.getJSONObject(k).getString("DiscountType");
                                            String Discount = PromocodeArray.getJSONObject(k).getString("Discount");
                                            Double d = Double.parseDouble(String.valueOf(Discount));
                                            Discountint = (int) Math.round(d);
                                            PROMO = "true";
                                            if (IsReturnSelected.equals("Yes")) {
                                                if (DiscountType.equals("0")) {
                                                        int TotalFareint =  RoundOff(Double.parseDouble(NetFare));
                                                    int ReturnTotalFareint = RoundOff(Double.parseDouble(ReturnNetFare));
                                                    int Fare = (TotalFareint + ReturnTotalFareint);
                                                    double DiscountAmonut = (Fare / 100.0f) * Discountint;
                                                    int DisC = RoundOff(DiscountAmonut);
                                                    DiscountAmount = String.valueOf(DisC);
                                                    double TotalFAre = Fare - DisC;
                                                    int Amount = (int) TotalFAre;
                                                    ReturnNetFare = String.valueOf(Amount);
                                                    int TotalShowingFare = (TotalFareint + ReturnTotalFareint);
                                                    PrompromoCodeApplyLayoutocode.setVisibility(View.GONE);
                                                    promocodeAmountDetails.setVisibility(View.VISIBLE);
                                                    PromocodeTotalPay.setText("" + TotalShowingFare + "");
                                                    PromocodeDiscountAmount.setText("" + DisC + "");
                                                    PromocodeYouPay.setText("" + ReturnNetFare + "");
                                                    ContinueBooking.setText("Proceed to book "+ReturnNetFare+" SAR");

                                                } else {
                                                    int TotalFareint =  RoundOff(Double.parseDouble(NetFare));
                                                    int ReturnTotalFareint = RoundOff(Double.parseDouble(ReturnNetFare));
                                                    int Fare = (TotalFareint + ReturnTotalFareint) - Discountint;
                                                    DiscountAmount = String.valueOf(Discountint);
                                                    ReturnNetFare = String.valueOf(Fare);
                                                    int TotalShowingFare = (TotalFareint + ReturnTotalFareint);
                                                    PrompromoCodeApplyLayoutocode.setVisibility(View.GONE);
                                                    promocodeAmountDetails.setVisibility(View.VISIBLE);
                                                    PromocodeTotalPay.setText("" + TotalShowingFare + "");
                                                    PromocodeDiscountAmount.setText("" + Discountint + "");
                                                    PromocodeYouPay.setText("" + ReturnNetFare + "");
                                                    ContinueBooking.setText("Proceed to book "+ReturnNetFare+" SAR");
                                                }

                                            } else {
                                                if (DiscountType.equals("0")) {
                                                    int TotalFareint = RoundOff(Double.parseDouble(NetFare));
                                                    double Disconut = (TotalFareint / 100.0f) * Discountint;
                                                    int DisC = RoundOff(Disconut);
                                                    DiscountAmount = String.valueOf(DisC);
                                                    double DiscountAmont = TotalFareint - DisC;
                                                    int Amount = (int) DiscountAmont;
                                                    NetFare = String.valueOf(Amount);
                                                    PrompromoCodeApplyLayoutocode.setVisibility(View.GONE);
                                                    promocodeAmountDetails.setVisibility(View.VISIBLE);
                                                    PromocodeTotalPay.setText("" + TotalFareint + "");
                                                    PromocodeDiscountAmount.setText("" + DisC + "");
                                                    PromocodeYouPay.setText("" + NetFare + "");
                                                    ContinueBooking.setText("Proceed to book "+NetFare+" SAR");
                                                } else {
                                                    int TotalFareint =  RoundOff(Double.parseDouble(NetFare));
                                                    int Fare = (TotalFareint) - Discountint;
                                                    DiscountAmount = String.valueOf(Discountint);
                                                    NetFare = String.valueOf(Fare);
                                                    PrompromoCodeApplyLayoutocode.setVisibility(View.GONE);
                                                    promocodeAmountDetails.setVisibility(View.VISIBLE);
                                                    PromocodeTotalPay.setText("" + TotalFareint + "");
                                                    PromocodeDiscountAmount.setText("" + Discountint + "");
                                                    PromocodeYouPay.setText("" + NetFare + "");
                                                    ContinueBooking.setText("Proceed to book "+NetFare+" SAR");
                                                }

                                            }


                                            getWindow().setSoftInputMode(
                                                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                                            );

                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PassengerDetails.this);
                                            alertDialogBuilder.setTitle("");
                                            alertDialogBuilder
                                                    .setMessage("Promo Code Applied Successfully")
                                                    .setCancelable(false)
                                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();


                                                        }
                                                    });
                                            AlertDialog alertDialog = alertDialogBuilder.create();
                                            alertDialog.show();


                                            Toast.makeText(PassengerDetails.this, "Promo Code Applied Successfully", Toast.LENGTH_SHORT).show();


                                            break;
                                        }/*else {
                                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PassengerDetails.this);

                                            // set title
                                            alertDialogBuilder.setTitle("");

                                            // set dialog message
                                            alertDialogBuilder
                                                    .setMessage("Promo Code is Valid only when \nTotal Amount is in between\n"+FromAmount +" and "+ToAmount)
                                                    .setCancelable(false)
                                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();


                                                        }
                                                    });

                                            // create alert dialog
                                            AlertDialog alertDialog = alertDialogBuilder.create();

                                            // show it
                                            alertDialog.show();
                                            break;
                                        }

                                    }*/ else {
                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PassengerDetails.this);
                                        alertDialogBuilder.setTitle("");
                                        alertDialogBuilder
                                                .setMessage("Invalid PromoCode !")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();


                                                    }
                                                });
                                        AlertDialog alertDialog = alertDialogBuilder.create();
                                        alertDialog.show();
                                        break;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                    if (Correct_Promo.equals("false")) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(PassengerDetails.this);
                        alertDialogBuilder.setTitle("");
                        alertDialogBuilder
                                .setMessage("Invalid PromoCode !")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();


                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public class BlockTicket extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(PassengerDetails.this, "", "Please Wait..\nGood things take time :)");
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {


            JSONObject bcparam = new JSONObject();
            // List<NameValuePair> parm=new ArrayList<NameValuePair>();
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Constants.BUSAPI + Constants.BlockBusTicket);
            httpPost.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpPost.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            httpPost.setHeader("Content-type", "application/json");
            try {


                bcparam.put("Names", Passenger_Full_name);
                bcparam.put("User", User_agentid);
                bcparam.put("UserType", UserId);
                bcparam.put("Address", "null");
                bcparam.put("MobileNo", MOBILE);
                bcparam.put("TripType", "2");
                bcparam.put("Servicetax", ServiceTaxes);
                bcparam.put("ServiceCharge", OperatorserviceCharge);
                bcparam.put("IdCardNo", IdProofNumber);
                bcparam.put("EmailId", EMAIL);
                bcparam.put("Ages", "null");
                bcparam.put("Nationalities",Passenger_Nationality); //New Line Added
                bcparam.put("PostalCode", "500082");
                bcparam.put("PartialCancellationAllowed", PartialCancellationAllowed);
                bcparam.put("IsOfflineBooking", "false");
                bcparam.put("NoofSeats", NumberOfSeats);
                bcparam.put("TripId", BusTypeID);
                bcparam.put("DestinationId", DestinationId);
                bcparam.put("CancellationPolicy", CancellationPolicy);
                bcparam.put("EmergencyMobileNo", "9160664166");
                bcparam.put("IdCardType", IdCard);
                bcparam.put("DeviceModel",getDeviceName());
                bcparam.put("DeviceToken", Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                bcparam.put("DeviceOSVersion", Build.VERSION.RELEASE);
                bcparam.put("BoardingId", BoardingId);
                bcparam.put("BoardingPointDetails", BoardingPointDetails );
                bcparam.put("DropingPointDetails",OnwardReturnBoarding);
                bcparam.put("DroppingId",OnwardReturnBoardingID);
                bcparam.put("SeatNos", SelectedSeats);
                bcparam.put("BookedDetails", getBookedDetails());
                bcparam.put("BusType", BusTypeid);
                bcparam.put("DisplayName",OnwardDisplayname);
                bcparam.put("Operator",Operator);
                bcparam.put("Genders", Passenger_gender);
                bcparam.put("JourneyDate", JourneyDate);
                bcparam.put("DestinationName", DestinationName);
                bcparam.put("SourceId", SourceId);
                bcparam.put("DeviceOS", "android");
                bcparam.put("Provider", Provider);
                bcparam.put("Fares", Fares);
                bcparam.put("Titles", TotalTitles);
                bcparam.put("SourceName", SourceName);
                bcparam.put("DepartureTime", departureTime);
                bcparam.put("ArrivalTime",ArravaialTime);
                bcparam.put("BusTypeName", BusType);
                bcparam.put("ConvenienceFee", "0");
                bcparam.put("IdCardIssuedBy", "AP");
                bcparam.put("HotelName",OnwardHotelname);
                bcparam.put("obiboProvider", ObiboAPIProvidrer);
                bcparam.put("IsPackage",Ispackage);
                bcparam.put("ReturnDate", OnwardReturnDate);
                bcparam.put("PromoCode", ValidPromoCode);
                bcparam.put("PayMode",pay_mode_ischecked());
                bcparam.put("BillingEmail","null");
                bcparam.put("BillingMobile","null");
                bcparam.put("BillingAddress","null");
                bcparam.put("BillingName","null");
                bcparam.put("PassangerLandMark","null");

                // bcparam.put("IsAgentPaymentGateway", false);


                // httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                // parm.add(new BasicNameValuePair("res",bcparam.toString()));
                try {
                    StringEntity seEntity;
                    // String data1 = null;
                /*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                    seEntity = new StringEntity(bcparam.toString(), "UTF_8");
                    //    seEntity.setContentType("application/json");
                    httpPost.setEntity(seEntity);
                    HttpResponse httpResponse;

                    httpResponse = httpClient.execute(httpPost);
                    Log.v("jsonresponce", "" + bcparam);
                    inputStream = httpResponse.getEntity().getContent();


                } catch (Exception e) {
                    e.printStackTrace();
                }
                //json=bcparam.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream));
            String line = "";
            data = "";
            try {
                while ((line = bufferedReader.readLine()) != null)
                    data += line;
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                inputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.d("TAG_LOCATIONS", bcparam.toString());

            //   Log.v("rsponse", "" + JsonWriter.formatJson(data.toString()).toString());
            System.out.println("BookingStatus" + data);
            responce = "" + bcparam;
            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(data);
                jsonObject.getString("BlockingReferenceNo");
                refno =jsonObject.getString("BookingReferenceNo");
                str = jsonObject.getString("Message");
                jsonObject.getString("BookingStatus");
                String Res=jsonObject.getString("ResponseStatus");


                if(str.equals("SUCCESS")&& Res.equals("200")){

                    if(IsReturnSelected.equals("Yes")){
                        new ReturnTicketBlockTicket().execute();
                    }else {
                        if (UserId.equals("4")) {
                            agentpaymentDialog = new Dialog(PassengerDetails.this);
                            agentpaymentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            agentpaymentDialog.setContentView(R.layout.agent_dialog);


                            //  pglogo = (ImageView) agentpaymentDialog.findViewById(R.id.payulogoimg);
                            wallet = (ImageView) agentpaymentDialog.findViewById(R.id.walletimg);

                            wallet.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialogwallet = false;
                                    wallet.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                    pay();
                                }
                            });
                            agentpaymentDialog.show();

                        } else {
                            if (payment_gateway.isChecked()){
                                final Context context = PassengerDetails.this;
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                                alertDialogBuilder.setTitle("Payment Conformation");
                                alertDialogBuilder
                                        .setMessage("Click yes to proceed!")
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                Intent i = new Intent(PassengerDetails.this, Payfort.class);
                                                i.putExtra("name", FullName);
                                                i.putExtra("email", String.valueOf(EMAIL));
                                                i.putExtra("phone", MOBILE);
                                                i.putExtra("amount", NetFare);
                                                i.putExtra("referencenumber", refno);
                                                System.out.println("referenceNo" + refno);
                                                i.putExtra("isFromOrder", true);
                                                startActivity(i);
                                                //  }
                                            }
                                            //  }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }else if (cash_on_delivery.isChecked()){
                                Intent i = new Intent(PassengerDetails.this, ResponseActivity.class);
                                i.putExtra("name", FullName);
                                i.putExtra("email", String.valueOf(EMAIL));
                                i.putExtra("phone", MOBILE);
                                i.putExtra("amount", NetFare);
                                i.putExtra("Pnrnumber", "");
                                i.putExtra("msg", "Your Booking Request is received by Site Admin - Your Booking Reference Number = ");
                                i.putExtra("Onwardrefno", refno);
                                i.putExtra("status","5");
                                i.putExtra("isFromOrder", true);
                                startActivity(i);
                            }
                        }
                    }
            }else{

                final Context context = PassengerDetails.this;
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setTitle(" ");
                alertDialogBuilder
                        .setMessage("" + str)
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(PassengerDetails.this, SearchActivity.class);

                                startActivity(i);
                            }
                            //  }
                        });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
            }catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
    public class ReturnTicketBlockTicket extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(PassengerDetails.this, "", "Please Wait..\nGood things take time :)");
        }

        @Override
        protected String doInBackground(String... params) {


            JSONObject bcparam = new JSONObject();
            // List<NameValuePair> parm=new ArrayList<NameValuePair>();
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Constants.BUSAPI + Constants.BlockBusTicket);
            httpPost.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpPost.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            httpPost.setHeader("Content-type", "application/json");
            try {

                bcparam.put("Names", Passenger_Full_name);
                bcparam.put("User", User_agentid);
                bcparam.put("UserType", UserId);
                bcparam.put("Address", "null");
                bcparam.put("MobileNo", MOBILE);
                bcparam.put("TripType", "2");
                bcparam.put("Servicetax", ReturnServiceTaxes);
                bcparam.put("ServiceCharge", ReturnOperatorserviceCharge);
                bcparam.put("IdCardNo", IdProofNumber);
                bcparam.put("EmailId", EMAIL);
                bcparam.put("Ages", "null");
                bcparam.put("Nationalities",Passenger_Nationality); //New Line Added
                bcparam.put("PostalCode", "500082");
                bcparam.put("PartialCancellationAllowed", ReturnPartialCancellationAllowed);
                bcparam.put("IsOfflineBooking", "false");
                bcparam.put("TripId", ReturnBusTypeid);
                bcparam.put("DestinationId", SourceId);
                bcparam.put("CancellationPolicy", ReturnCancellationPolicy);
                bcparam.put("EmergencyMobileNo", "9160664166");
                bcparam.put("IdCardType", IdCard);
                bcparam.put("BookedDetails", getBookedDetails());
                bcparam.put("DeviceModel", getDeviceName());
                bcparam.put("DeviceToken", Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID));
                bcparam.put("DeviceOSVersion",Build.VERSION.RELEASE);
                bcparam.put("BoardingId", ReturnBoardingId);
                bcparam.put("BoardingPointDetails", ReturnBoardingPointDetails);
                bcparam.put("BusType", ReturnBusType);
                bcparam.put("DisplayName",DisplayName);
                bcparam.put("Operator",ReturnOperator);
                bcparam.put("Genders", Passenger_gender);
                bcparam.put("JourneyDate", Returndate);
                bcparam.put("DestinationName", SourceName);
                bcparam.put("SourceId", DestinationId);
                bcparam.put("DeviceOS", "android");
                bcparam.put("Provider", Provider);
                bcparam.put("Fares", ReturnFares);
                bcparam.put("Titles", TotalTitles);
                bcparam.put("SourceName", DestinationName);
                bcparam.put("DepartureTime", ReturndepartureTime);
                bcparam.put("BusTypeName", ReturnBusType);
                bcparam.put("ConvenienceFee", "0");
                bcparam.put("IdCardIssuedBy", "AP");
                bcparam.put("returnDate", Returndate);
                bcparam.put("HotelName",HotelName);
                bcparam.put("NoofSeats",  ReturnNumberOfSeats);
                bcparam.put("SeatNos",  ReturnSelectedSeats);
                bcparam.put("obiboProvider", ObiboAPIProvidrer);
                bcparam.put("PromoCode", ValidPromoCode);
                bcparam.put("PayMode",pay_mode_ischecked());
                bcparam.put("BillingEmail","null");
                bcparam.put("BillingMobile","null");
                bcparam.put("BillingAddress","null");
                bcparam.put("BillingName","null");
                bcparam.put("PassangerLandMark","null");

                // bcparam.put("IsAgentPaymentGateway", false);


                // httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                // parm.add(new BasicNameValuePair("res",bcparam.toString()));
                try {
                    StringEntity seEntity;
                    // String data1 = null;
                /*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                    seEntity = new StringEntity(bcparam.toString(), "UTF_8");
                    //seEntity.setContentType("application/json");
                    httpPost.setEntity(seEntity);
                    HttpResponse httpResponse;

                    httpResponse = httpClient.execute(httpPost);
                    Log.v("REturn ticket Data", "" + bcparam);
                    inputStream = httpResponse.getEntity().getContent();


                } catch (Exception e) {
                    e.printStackTrace();
                }
                //json=bcparam.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream));
            String line = "";
            data = "";
            try {
                while ((line = bufferedReader.readLine()) != null)
                    data += line;
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                inputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            Log.d("TAG_LOCATIONS", bcparam.toString());

            //   Log.v("rsponse", "" + JsonWriter.formatJson(data.toString()).toString());
            System.out.println("BookingStatus" + data);
            responce = "" + bcparam;
            return data;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            try {
                JSONObject jsonObject = new JSONObject(data);
                jsonObject.getString("BlockingReferenceNo");
                ReturnTicketrefno =jsonObject.getString("BookingReferenceNo");
                ReturnTicketstr = jsonObject.getString("Message");
                jsonObject.getString("BookingStatus");
                String Res=jsonObject.getString("ResponseStatus");


                if(ReturnTicketstr.equals("SUCCESS")&& Res.equals("200")){
                    if(UserId.equals("4")){
                        agentpaymentDialog = new Dialog(PassengerDetails.this);
                        agentpaymentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        agentpaymentDialog.setContentView(R.layout.agent_dialog);


                        //  pglogo = (ImageView) agentpaymentDialog.findViewById(R.id.payulogoimg);
                        wallet = (ImageView) agentpaymentDialog.findViewById(R.id.walletimg);
                       /* pglogo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogwallet = true;
                                pglogo.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                wallet.setBackgroundColor(Color.TRANSPARENT);
                                // pglayout.setVisibility(View.VISIBLE);
                                int GrandFare= Integer.parseInt(NetFare)+Integer.parseInt(ReturnNetFare);
                                RoundTripFare= String.valueOf(GrandFare);
                                Intent i = new Intent(PassengerDetails.this, Monetbil_paymentgateway.class);
                                i.putExtra("name" ,FullName);
                                i.putExtra("email", String.valueOf(EMAIL));
                                i.putExtra("phone", MOBILE);
                                i.putExtra("amount", RoundTripFare);
                                i.putExtra("referencenumber", refno);
                                i.putExtra("ReturnTicketrReferenceNumber", ReturnTicketrefno);
                                System.out.println("referenceNo"+ReturnTicketrefno);
                                i.putExtra("isFromOrder", true);
                                startActivity(i);
                            }
                        });*/

                        wallet.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogwallet = false;
                                wallet.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                //  pglogo.setBackgroundColor(Color.TRANSPARENT);

                                pay();
                            }
                        });
                        agentpaymentDialog.show();
                        //  agentpaymentDialog.setCancelable(false);
                    }else {
                        if (payment_gateway.isChecked()) {
                            final Context context = PassengerDetails.this;
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                            // set title
                            alertDialogBuilder.setTitle("Payment Confirmation ");

                            // set dialog message
                            alertDialogBuilder
                                    .setMessage("Click Yes to proceed!")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // if this button is clicked, close
                                       /* Float Grandfare=Float.parseFloat(NetFare)+Float.parseFloat(OperatorserviceCharge)+Float.parseFloat(ServiceTaxes);
                                        Float GrandFare1=Grandfare+Float.parseFloat(ReturnFares)+Float.parseFloat(ReturnServiceTaxes)+Float.parseFloat(ReturnOperatorserviceCharge); ;
                                        RoundTripFare= String.valueOf(GrandFare1);*/
                                            Float Grandfare = Float.parseFloat(NetFare) + Float.parseFloat(ReturnNetFare);
                                            RoundTripFare = String.valueOf(Grandfare);
                                            Intent i = new Intent(PassengerDetails.this, Payfort.class);
                                            i.putExtra("name", FullName);
                                            i.putExtra("email", String.valueOf(EMAIL));
                                            i.putExtra("phone", MOBILE);
                                            i.putExtra("amount", RoundTripFare);
                                            i.putExtra("referencenumber", refno);
                                            i.putExtra("ReturnTicketrReferenceNumber", ReturnTicketrefno);
                                            System.out.println("referenceNo" + ReturnTicketrefno);
                                            i.putExtra("isFromOrder", true);
                                            startActivity(i);
                                            //  }
                                        }
                                        //  }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            // if this button is clicked, just close
                                            // the dialog box and do nothing
                                            dialog.cancel();
                                        }
                                    });

                            // create alert dialog
                            AlertDialog alertDialog = alertDialogBuilder.create();

                            // show it
                            alertDialog.show();
                        } else if (cash_on_delivery.isChecked()){
                            Intent i = new Intent(PassengerDetails.this, ResponseActivity.class);
                            i.putExtra("name", FullName);
                            i.putExtra("email", String.valueOf(EMAIL));
                            i.putExtra("phone", MOBILE);
                            i.putExtra("amount", NetFare);
                            i.putExtra("Pnrnumber", "");
                            i.putExtra("msg", "Your Booking Request is received by Site Admin - Your Booking Reference Number = ");
                            i.putExtra("Onwardrefno", refno);
                            i.putExtra("status","5");
                            i.putExtra("isFromOrder", true);
                            startActivity(i);
                        }

                    }
                }else{

                    final Context context = PassengerDetails.this;
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setTitle(" ");
                    alertDialogBuilder
                            .setMessage("" +ReturnTicketstr)
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = new Intent(PassengerDetails.this, SearchActivity.class);

                                    startActivity(i);
                                }
                            });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private String pay_mode_ischecked() {
        String paymode;
        if (cash_on_delivery.isChecked()){
            paymode="0";
        }else {
            paymode="1";
        }

        return paymode;
    }


    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }


    public void pay(){
        new Busbooking().execute();
    }

    public  class Busbooking extends AsyncTask<String,String,String> {

        ProgressDialog dialog;
        String res;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog=ProgressDialog.show(PassengerDetails.this,"","Please Wait...");
        }

        @Override
        protected String doInBackground(String... params) {

            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(Constants.BUSAPI+ Constants.BookBusTicket+refno);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpGet.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            try {
                res = httpClient.execute(httpGet, responseHandler);
            } catch (IOException e) {

                e.printStackTrace();
            }

            Log.v("Busbooking", "" + res);
            return res;


        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            String sts,msg,ref,PNRNUMBER;
            JSONObject jsonObject = null;
            try {



                if ((!res.equals("")) && (!res.equals(null)) ) {
                    //  Toast.makeText(getApplicationContext(),"hiii welcome to splash Screen",2000).show();
                    jsonObject = new JSONObject(res);
                    // encryptclientid=jsonObject.getString("ClientId")
                    PNRNUMBER=jsonObject.getString("OperatorPNR");
                    OnwardReferenceNumber=jsonObject.getString("ReferenceNo");
                    jsonObject.getString("APIReferenceNo");
                    msg= jsonObject.getString("Message");
                    sts=  jsonObject.getString("BookingStatus");
                    jsonObject.getString("ResponseStatus");
                    jsonObject.getString("RefundResponse");
                    jsonObject.getString("Provider");



                    //    Toast.makeText(Paymentinfo.this,""+data,3000).show();

                    if(ReturnTicketrefno!=null) {
                        new PassengerDetails.ReturnTicketBusbooking().execute();
                    }else if(sts.equals("1")){
                        Intent i = new Intent(PassengerDetails.this, ResponseActivity.class);
                        i.putExtra("name", FullName);
                        i.putExtra("email", String.valueOf(EMAIL));
                        i.putExtra("phone", MOBILE);
                        i.putExtra("amount", NetFare);
                        i.putExtra("Pnrnumber", PNRNUMBER);
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", refno);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);
                    } else if(sts.equals("4")){
                        Intent i = new Intent(PassengerDetails.this, ResponseActivity.class);
                        i.putExtra("name", FullName);
                        i.putExtra("email", String.valueOf(EMAIL));
                        i.putExtra("phone", MOBILE);
                        i.putExtra("amount", NetFare);
                        i.putExtra("Pnrnumber", PNRNUMBER);
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", refno);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);

                    }else if(sts.equals("3")){
                        Intent i = new Intent(PassengerDetails.this, ResponseActivity.class);
                        i.putExtra("name", FullName);
                        i.putExtra("email", String.valueOf(EMAIL));
                        i.putExtra("phone", MOBILE);
                        i.putExtra("amount", NetFare);
                        i.putExtra("Pnrnumber", PNRNUMBER);
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", refno);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);

                    }
                    else {

                        Toast.makeText(PassengerDetails.this,"Some Problem Occured.\n Please Check your credentials",Toast.LENGTH_LONG).show();
                    }
                }else {

                    Toast.makeText(PassengerDetails.this,"Some Problem Occured with Provider\n Please Try later",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                Toast.makeText(PassengerDetails.this,"Some Problem Occured with Provider\n Please Try later",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
           /* Intent intent = new Intent(PayUmoney.this, MainActivity.class);
            startActivity(intent);*/

        }

    }


    public  class ReturnTicketBusbooking extends AsyncTask<String,String,String> {

        ProgressDialog dialog;
        String res;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog=ProgressDialog.show(PassengerDetails.this,"","Please Wait..\nGood things take time :)");
        }

        @Override
        protected String doInBackground(String... params) {

            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(Constants.BUSAPI+ Constants.BookBusTicket+ReturnTicketrefno);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpGet.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            try {
                res = httpClient.execute(httpGet, responseHandler);
            } catch (IOException e) {

                e.printStackTrace();
            }

            Log.v("Busbooking", "" + res);
            return res;


        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();
            String sts,msg,ref;
            JSONObject jsonObject = null;
            try {



                if ((!res.equals("")) && (!res.equals(null)) ) {
                    //  Toast.makeText(getApplicationContext(),"hiii welcome to splash Screen",2000).show();
                    jsonObject = new JSONObject(res);
                    // encryptclientid=jsonObject.getString("ClientId")
                    jsonObject.getString("OperatorPNR");
                    ref=jsonObject.getString("ReferenceNo");
                    jsonObject.getString("APIReferenceNo");
                    msg= jsonObject.getString("Message");
                    sts=  jsonObject.getString("BookingStatus");
                    jsonObject.getString("ResponseStatus");
                    jsonObject.getString("RefundResponse");
                    jsonObject.getString("Provider");



                    //    Toast.makeText(Paymentinfo.this,""+data,3000).show();
                    if (sts.equals("1")) {


                        Intent i = new Intent(PassengerDetails.this, ResponseActivity.class);
                        i.putExtra("name", FullName);
                        i.putExtra("email", String.valueOf(EMAIL));
                        i.putExtra("phone", MOBILE);
                        i.putExtra("amount", RoundTripFare);
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", OnwardReferenceNumber);
                        i.putExtra("Returnrefno", ref);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);

                    } else if(sts.equals("4")){
                        Intent i = new Intent(PassengerDetails.this, ResponseActivity.class);
                        i.putExtra("name", FullName);
                        i.putExtra("email", String.valueOf(EMAIL));
                        i.putExtra("phone", MOBILE);
                        i.putExtra("amount", RoundTripFare);
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", OnwardReferenceNumber);
                        i.putExtra("Returnrefno", ref);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);

                    }else if(sts.equals("3")){
                        Intent i = new Intent(PassengerDetails.this, ResponseActivity.class);
                        i.putExtra("name", FullName);
                        i.putExtra("email", String.valueOf(EMAIL));
                        i.putExtra("phone", MOBILE);
                        i.putExtra("amount", RoundTripFare);
                        i.putExtra("msg", msg);
                        i.putExtra("Onwardrefno", OnwardReferenceNumber);
                        i.putExtra("Returnrefno", ref);
                        i.putExtra("status",sts);
                        i.putExtra("isFromOrder", true);
                        startActivity(i);

                    }
                    else {

                        Toast.makeText(PassengerDetails.this,"Some Problem Occured.\n Please Check your credentials",Toast.LENGTH_LONG).show();
                    }
                }else {

                    Toast.makeText(PassengerDetails.this,"Some Problem Occured with Provider\n Please Try later",Toast.LENGTH_LONG).show();
                }

            } catch (Exception e) {

                Toast.makeText(PassengerDetails.this,"Some Problem Occured with Provider\n Please Try later",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
           /* Intent intent = new Intent(PayUmoney.this, MainActivity.class);
            startActivity(intent);*/

        }

    }




    // Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
           new BlockTicket().execute();
        } else {


            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set title
            //  alertDialogBuilder.setTitle("Your Title");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Sorry! Not connected to internet")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity

                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }

    public Integer RoundOff(double TotalRate){
        double dAbs = Math.abs(TotalRate);
        int amt2 = (int) dAbs;
        double result = dAbs - (double) amt2;
        int finalrate;
        if (result<0.5){
            finalrate= (int) Math.floor(TotalRate);
        }else {
            finalrate= (int) Math.ceil(TotalRate);
        }

        return finalrate;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
    //  finish();
}
