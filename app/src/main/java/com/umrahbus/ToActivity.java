package com.umrahbus;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.umrahbus.AdapterClasses.TotopcitiesAdapter;
import com.umrahbus.BeanClasses.Sources;
import com.umrahbus.BeanClasses.TopCities;
import com.umrahbus.ConnectionStatus.ConnectivityReceiver;
import com.umrahbus.ConnectionStatus.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ToActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {


    private ListView listView,list;
    private MyAppAdapter myAppAdapter;
    private ArrayList<Sources> sourceArrayList;


    ProgressDialog pDialog;
    Sources roomDetailsBean;
    JSONObject jsonobject;
    String Responce;
    static final String[] TopCities = new String[] { "Makkah", "Makkah and Madinah", "Madinah","Madinah and Makkah"};
    static final String[] TopCitiesId=new String[]{"141","122","140","142"};

    ArrayList<com.umrahbus.BeanClasses.TopCities> arraylist = new ArrayList<>();
    TotopcitiesAdapter listadapter;

    SharedPreferences preference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        listView = (ListView) findViewById(R.id.listView);
      list = (ListView) findViewById(R.id.listView1);
        sourceArrayList = new ArrayList<>();
        checkConnection();
         preference = getApplicationContext().getSharedPreferences("Sources", MODE_PRIVATE);
         Responce=preference.getString("JSONSTR", null);
//        if(Responce==null){
//            new FromAvailableSources().execute();
//        }else{
//
//            new ToSources().execute();
//
//
//        }

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header, list,
                false);
        list.addHeaderView(header, null, false);
       // ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.availablelist_item,R.id.location, TopCities);
        for (int i = 0; i < TopCities.length; i++)
        {
            com.umrahbus.BeanClasses.TopCities wp = new TopCities(TopCities[i], TopCitiesId[i] );
            // Binds all strings into an array
            arraylist.add(wp);
        }
        // Pass results to FromtopcitiesAdapter Class
        listadapter = new TotopcitiesAdapter(this, arraylist);

        list.setAdapter(listadapter);


        list.setTextFilterEnabled(true);


        //new ToSources().execute();
       // new FromAvailableSources().execute();
    }

    public class ToSources extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {


            pDialog = new ProgressDialog(ToActivity.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }



        @Override
        protected String doInBackground(String... params) {
            Responce=preference.getString("JSONSTR", null);
            JSONObject jsonObject = null;
            try {
                //    jsonObject = new JSONObject(data);
                JSONArray jsonarray = new JSONArray(Responce);



                for (int i = 0; i < jsonarray.length(); i++) {
                    roomDetailsBean = new Sources();


                    jsonobject = jsonarray.getJSONObject(i);
                    // Retrive JSON Objects
                    roomDetailsBean.setId(jsonobject.getString("Id"));
                    roomDetailsBean.setName(jsonobject.getString("Name"));


                    // Set the JSON Objects into the array
                    sourceArrayList.add(roomDetailsBean);
                   /* myAppAdapter = new MyAppAdapter(sourceArrayList, FromActivity.this);
                    listView.setAdapter(myAppAdapter);
                    listView.setVisibility(View.GONE);*/
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            pDialog.dismiss();
            super.onPostExecute(s);
            myAppAdapter = new MyAppAdapter(sourceArrayList, ToActivity.this);
            listView.setAdapter(myAppAdapter);
            listView.setVisibility(View.VISIBLE);


        }
    }

    public class MyAppAdapter extends BaseAdapter {

        public class ViewHolder {
            TextView txtTitle;


        }

        public List<Sources> parkingList;

        public Context context;
        ArrayList<Sources> arraylist;

        private MyAppAdapter(List<Sources> apps, Context context) {
            this.parkingList = apps;
            this.context = context;
            arraylist = new ArrayList<>();
            arraylist.addAll(parkingList);

        }

        @Override
        public int getCount() {
            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;
            ViewHolder viewHolder;

            if (rowView == null) {
                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.availablelist_item, null);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.txtTitle = (TextView) rowView.findViewById(R.id.location);
                //   viewHolder.txtSubTitle = (TextView) rowView.findViewById(R.id.subtitle);
                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.txtTitle.setText(parkingList.get(position).getName() + "");
            //  viewHolder.txtSubTitle.setText(parkingList.get(position).getPostSubTitle() + "");

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int postin =position;

                    Intent i = new Intent(context, MainActivity.class);


                    SharedPreferences preference = context.getSharedPreferences("Sources", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preference.edit();
                    editor.putString("TOID",parkingList.get(position).getId());
                    editor.putString("TONAME",parkingList.get(position).getName());

                    editor.commit();

                    context.startActivity(i);
                    finish();
                }
            });
            return rowView;


        }

        /*public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());

            parkingList.clear();
            if (charText.length() == 0) {
                parkingList.addAll(arraylist);
                listView.setVisibility(View.GONE);
                list.setVisibility(View.VISIBLE);

            } else {
                for (Sources postDetail : arraylist) {
                    if (charText.length() != 0 && postDetail.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        parkingList.add(postDetail);
                       // listView.setVisibility(View.VISIBLE);
                       // list.setVisibility(View.GONE);
                    }

                   *//* else if (charText.length() != 0 && postDetail.getPostSubTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                        parkingList.add(postDetail);
                    }*//*
                }
            }
            notifyDataSetChanged();
        }*/
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setIconifiedByDefault(false);
        searchView.setSubmitButtonEnabled(true);
        searchView.setQueryHint("To");
        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.curser); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
        }
        /*//*** setOnQueryTextFocusChangeListener ***
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchQuery) {
                myAppAdapter.filter(searchQuery.trim());
                listView.invalidate();
                return true;
            }
        });

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Do something when collapsed
                return true;  // Return true to collapse action view
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Do something when expanded
                return true;  // Return true to expand action view
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

   /* public class CheckAvailability extends AsyncTask<String, String, String> {






        @Override
        protected void onPreExecute() {

            pDialog = new ProgressDialog(ToActivity.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }

        String url = Constants.BUSAPI+Constants.Sources;

        @Override
        protected String doInBackground(String... params) {

            System.out.println("url" + url);
            ServiceHandler sh = new ServiceHandler();
            String jsonstr = sh.makeServiceCall(url, ServiceHandler.GET);
            if (jsonstr != null) {

                postArrayList.clear();

                try {
                    // Locate the array name in JSON
                    JSONArray jsonarray = new JSONArray(jsonstr);


                    for (int i = 0; i < jsonarray.length(); i++) {
                        roomDetailsBean = new Sources();


                        jsonobject = jsonarray.getJSONObject(i);
                        // Retrive JSON Objects
                        roomDetailsBean.setId(jsonobject.getString("Id"));
                        roomDetailsBean.setName(jsonobject.getString("Name"));


                        // Set the JSON Objects into the array
                        postArrayList.add(roomDetailsBean);
                        SharedPreferences preference = getApplicationContext().getSharedPreferences("Sources", MODE_PRIVATE);
                        SharedPreferences.Editor editor = preference.edit();
                        editor.putString("JSONSOURCES", jsonstr);
                        editor.commit();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            return null;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            myAppAdapter = new MyAppAdapter(postArrayList, ToActivity.this);
            listView.setAdapter(myAppAdapter);
            listView.setVisibility(View.GONE);
            pDialog.dismiss();

        }
    }*/

    /*public class FromAvailableSources extends AsyncTask<String, String, String> {

        String data;
        @Override
        protected void onPreExecute() {

            pDialog = new ProgressDialog(ToActivity.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();

        }
        @Override
        protected String doInBackground(String... params) {
            String strurl = Constants.BUSAPI+Constants.Sources;
            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(strurl);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpGet.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            try {
                data = httpClient.execute(httpGet, responseHandler);
            } catch (IOException e) {
                e.printStackTrace();
            }


            return data;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            JSONObject jsonObject = null;
            try {
                //    jsonObject = new JSONObject(data);
                JSONArray jsonarray = new JSONArray(data);

                if (!data.equals("") && !data.equals("null") && !data.contains("Message")) {

                   *//* try {
                        // Locate the array name in JSON
                        //  JSONArray jsonarray = new JSONArray(jsonObject);


                        for (int i = 0; i < jsonarray.length(); i++) {
                            roomDetailsBean = new Sources();


                            jsonobject = jsonarray.getJSONObject(i);
                            // Retrive JSON Objects
                            roomDetailsBean.setId(jsonobject.getString("Id"));
                            roomDetailsBean.setName(jsonobject.getString("Name"));


                            // Set the JSON Objects into the array
                            sourceArrayList.add(roomDetailsBean);
                            myAppAdapter = new MyAppAdapter(sourceArrayList, ToActivity.this);
                            listView.setAdapter(myAppAdapter);
                            listView.setVisibility(View.GONE);
                            pDialog.dismiss();*//*
                            SharedPreferences preference = getApplicationContext().getSharedPreferences("Sources", MODE_PRIVATE);
                            SharedPreferences.Editor editor = preference.edit();
                            editor.putString("JSONSTR", String.valueOf(jsonarray));
                            editor.commit();
                    new ToSources().execute();
                    pDialog.dismiss();
                       *//* }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*//*
                } else {

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }*/

    // Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    this);

            // set title
            //  alertDialogBuilder.setTitle("Your Title");

            // set dialog message
            alertDialogBuilder
                    .setMessage("Sorry! Not connected to internet")
                    .setCancelable(false)
                    .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, close
                            // current activity

                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
    //  finish();
}
