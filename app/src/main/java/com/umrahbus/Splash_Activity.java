package com.umrahbus;

/**
 * Created by Nagarjuna on 5/10/2018.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;
import com.umrahbus.ServicesClasses.ServiceClasses;
import com.umrahbus.pojo.GetClientDetails;
import com.umrahbus.pojo.Version_DTO;




import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Nagarjuna on 8/14/2017.
 */

public class Splash_Activity extends AppCompatActivity {

    RelativeLayout parentLinear;
    Boolean first=false;
    String Services;
    String data;
    GetClientDetails ClientDetailsDTO;
    String C_Response;
    String currentVersion;
    String online_version = null;
    Boolean NewUpdateAvailable=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.spleash_screen);
        parentLinear = (RelativeLayout) findViewById(R.id.parentLinear);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);



        //Uncomment this in future after migrating to new server
        //callVersionCodeCheck();

        callClientDetails();
    }

    private void callVersionCodeCheck() {

        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (Util.isNetworkAvailable(this)) {
            ServiceClasses.GETVERSIONCODE(this, Constants.VERSIONCODE+"202");
        } else {
            Util.showMessage(this, "No Internet connection");
        }
    }

    public void getversioncoderesponse(String response) {
        if (response != null) {
            InputStream stream = new ByteArrayInputStream(response.getBytes());
            Gson gson = new Gson();
            Reader reader = new InputStreamReader(stream);
            Version_DTO version_dto = gson.fromJson(reader, Version_DTO.class);

            if (version_dto.getVersionDetails().size() != 0) {
                for (int p = 0; p < version_dto.getVersionDetails().size(); p++) {
                    if (version_dto.getVersionDetails().get(p).getClientID() == 202) {
                        online_version = version_dto.getVersionDetails().get(p).getAndroidVersion();
                    }
                }
                String currentversion = currentVersion;
                if (currentversion != null && online_version != null) {
                    if (Float.valueOf(currentversion) < Float.valueOf(online_version)) {
                        NewUpdateAvailable=true;
                    }
                }
            }

        }
    }

    private void callClientDetails() {
        if(Util.isNetworkAvailable(getApplicationContext())) {
            ServiceClasses.getClientDetails(Splash_Activity.this,Constants.BUSAPI+Constants.ClientDetails);

        }else{
            Snackbar snackbar = Snackbar
                    .make(parentLinear, "No internet connection", Snackbar.LENGTH_INDEFINITE)
                    .setActionTextColor(getResources().getColor(R.color.colorWhite))
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            callClientDetails();
                        }
                    });

            snackbar.show();
        }
    }


    public void getClientDetailsResponse(String response) {
        GetClientDetails getClientDetailsDTO =null;
        if(response!=null){

            C_Response=response;


            InputStream stream = new ByteArrayInputStream(response.getBytes());
            Gson gson = new Gson();
            Reader reader = new InputStreamReader(stream);
            getClientDetailsDTO = gson.fromJson(reader, GetClientDetails.class);
            if(getClientDetailsDTO!=null && getClientDetailsDTO.getServices()!=null && getClientDetailsDTO.getServices().size()>0){
                ClientDetailsDTO = getClientDetailsDTO;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        if(NewUpdateAvailable){
                            Intent i=new Intent(Splash_Activity.this,Version_updater_Activity.class);
                            i.putExtra("NewVesionCode",online_version);
                            finish();
                            startActivity(i);
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }else {
                            Intent i = new Intent(Splash_Activity.this, MainActivity.class);
                            SharedPreferences preference = getApplicationContext().getSharedPreferences("Sources", MODE_PRIVATE);
                            preference.edit().remove("FROMNAME").apply();
                            preference.edit().remove("FROMID").apply();
                            preference.edit().remove("TONAME").apply();
                            preference.edit().remove("TOID").apply();
                            startActivity(i);
                            finish();

                        }
                    }
                }, 3000);
            }else{
                Util.showMessage(this,"Something went wrong!");
            }
        }else {
            Util.showMessage(this, "Something went wrong!");
        }

    }

    public void errorresponse (String response){

        Snackbar snackbar = Snackbar
                .make(parentLinear, "Couldn't create connection", Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(getResources().getColor(R.color.colorWhite))
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callClientDetails();
                    }
                });

        snackbar.show();
    }
}

