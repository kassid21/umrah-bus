package com.umrahbus.draweritems;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.umrahbus.Constants;
import com.umrahbus.ServicesClasses.ServiceHandler;
import com.umrahbus.R;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jagadeesh on 7/30/2016.
 */
public class CancelTicketActivity extends AppCompatActivity{

    TextView activelfare,Reference_no,journeyname,cencellation,partailCancellation,Returndetails;
    Button Ticket_Cancel;
    CheckBox Ch1,Ch2,Ch3,Ch4,Ch5,Ch6;
    String cancalpolicy="";
    String polcy;
    String Refernce_No,EMAIL_ID;
    String NoofSeats,SeatNos,Partial;
    String data;
    int code;
    ArrayList<String> selectedStrings = new ArrayList<String>();
    String SelectedSeats;
    protected void onCreate(Bundle savedInstaceState) {
        super.onCreate(savedInstaceState);
        setContentView(R.layout.activity_cancel_ticket_details);

        Reference_no=(TextView)findViewById(R.id.Enter_reference_no) ;
        journeyname=(TextView)findViewById(R.id.Journey_Details) ;
        activelfare=(TextView)findViewById(R.id.Total_fare) ;
        cencellation=(TextView)findViewById(R.id.Cancellation_policy) ;
        partailCancellation=(TextView)findViewById(R.id.Partial) ;
        Ticket_Cancel=(Button)findViewById(R.id.Ticket_Cancel);
        Returndetails= (TextView) findViewById(R.id.Journey_Details1);

        Ch1=(CheckBox)findViewById(R.id.checkBox);
        Ch2=(CheckBox)findViewById(R.id.checkBox1);
        Ch3=(CheckBox)findViewById(R.id.checkBox2);
        Ch4=(CheckBox)findViewById(R.id.checkBox4);
        Ch5=(CheckBox)findViewById(R.id.checkBox5);
        Ch6=(CheckBox)findViewById(R.id.checkBox6);

        Bundle bundle = getIntent().getExtras();
        String ActualFare = bundle.getString("ActualFare");
        String Convinecefee=bundle.getString("ConvenienceFee");
        String SourceName = bundle.getString("SourceName");
        String DestinationName = bundle.getString("DestinationName");
        String JourneyDate = bundle.getString("JourneyDate");
        String OperatorName = bundle.getString("OperatorName");
        String BusTypeName = bundle.getString("BusTypeName");
        String Returndate=bundle.getString("Returndate");
        NoofSeats = bundle.getString("NoofSeats");
        SeatNos = bundle.getString("SeatNos");
        String CancellationPolicy = bundle.getString("CancellationPolicy");
        Partial=bundle.getString("PartialCancellationAllowed");
        Refernce_No = bundle.getString("Refernce_No");
        EMAIL_ID = bundle.getString("EMAIL_ID");

        int Fare=  (int) Math.round(Double.parseDouble(ActualFare));
        int Convinec=  (int) Math.round(Double.parseDouble(Convinecefee));

        int Total=Fare-Convinec;

        Reference_no.setText(""+Refernce_No);
        journeyname.setText(""+SourceName+"  To  "+DestinationName+"  On "+JourneyDate+" "+OperatorName+" - "+BusTypeName);
        activelfare.setText("Rs."+Total);

        Returndetails.setText(""+DestinationName+"  To  "+SourceName+"  On "+Returndate+" "+OperatorName+" - "+BusTypeName);


        String seatCount[]=SeatNos.split("~");
        System.out.println("seatCount"+seatCount[0]);
        Ch1.setText(seatCount[0]);

        String cancelArray[] = CancellationPolicy.split(";");
        System.out.println("split"+cancelArray[0]);

        for (int i=0; i<cancelArray.length; i++) {
            String fromCancelArray[]=cancelArray[i].split(":");
            if (fromCancelArray.length>1) {
//
                String cancelTimeStr;
                if (fromCancelArray[1].equals("-1")) {
                    cancelTimeStr=(fromCancelArray[0]+"   hours before journey time          ");
                }else{
                    cancelTimeStr=(fromCancelArray[1]+"   hours before journey time           "+fromCancelArray[0]);
                }

                String percentSybole="%";
                String percentStr=(fromCancelArray[2]+".0 "+percentSybole);



                polcy=cancelTimeStr+percentStr;



                System.out.println("data"+cancelTimeStr+percentStr);
            }

            cancalpolicy=""+cancalpolicy+""+polcy+"\n";
        }
        if (CancellationPolicy.equals("")){
            cencellation.setText("No Cancellation policy found");
        }else {
            cencellation.setText(""+cancalpolicy);
        }
        if (Partial.equals("true")){
            partailCancellation.setText("Partial Cancellation is Allowed");
        }else {
            partailCancellation.setText("Partial Cancellation is not Allowed");
        }


        if(NoofSeats.equals("2")){
            Ch2.setVisibility(View.VISIBLE);
            Ch2.setText(seatCount[1]);
        }else if(NoofSeats.equals("3")){
            Ch2.setVisibility(View.VISIBLE);
            Ch3.setVisibility(View.VISIBLE);
            Ch2.setText(seatCount[1]);
            Ch3.setText(seatCount[2]);
        }else if(NoofSeats.equals("4")){
            Ch2.setVisibility(View.VISIBLE);
            Ch3.setVisibility(View.VISIBLE);
            Ch4.setVisibility(View.VISIBLE);
            Ch2.setText(seatCount[1]);
            Ch3.setText(seatCount[2]);
            Ch4.setText(seatCount[3]);
        }else if(NoofSeats.equals("5")){
            Ch2.setVisibility(View.VISIBLE);
            Ch3.setVisibility(View.VISIBLE);
            Ch4.setVisibility(View.VISIBLE);
            Ch5.setVisibility(View.VISIBLE);
            Ch2.setText(seatCount[1]);
            Ch3.setText(seatCount[2]);
            Ch4.setText(seatCount[3]);
            Ch5.setText(seatCount[4]);
        }else if(NoofSeats.equals("6")){
            Ch2.setVisibility(View.VISIBLE);
            Ch3.setVisibility(View.VISIBLE);
            Ch4.setVisibility(View.VISIBLE);
            Ch5.setVisibility(View.VISIBLE);
            Ch6.setVisibility(View.VISIBLE);
            Ch2.setText(seatCount[1]);
            Ch3.setText(seatCount[2]);
            Ch4.setText(seatCount[3]);
            Ch5.setText(seatCount[4]);
            Ch6.setText(seatCount[5]);
        }
        Ch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedStrings.add(Ch1.getText().toString());
                }else{
                    selectedStrings.remove(Ch1.getText().toString());
                }

            }
        });
        Ch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedStrings.add(Ch2.getText().toString());
                }else{
                    selectedStrings.remove(Ch2.getText().toString());
                }

            }
        });
        Ch3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedStrings.add(Ch3.getText().toString());
                }else{
                    selectedStrings.remove(Ch3.getText().toString());
                }

            }
        });
        Ch4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedStrings.add(Ch4.getText().toString());
                }else{
                    selectedStrings.remove(Ch4.getText().toString());
                }

            }
        });
        Ch5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedStrings.add(Ch5.getText().toString());
                }else{
                    selectedStrings.remove(Ch5.getText().toString());
                }

            }
        });
        Ch6.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    selectedStrings.add(Ch6.getText().toString());
                }else{
                    selectedStrings.remove(Ch6.getText().toString());
                }

            }
        });

        Ticket_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Ch1.isChecked()|| Ch2.isChecked()|| Ch3.isChecked()|| Ch4.isChecked()|| Ch5.isChecked()|| Ch6.isChecked()){
                    System.out.println("Checked"+selectedStrings);

                    SelectedSeats= TextUtils.join(",",selectedStrings);
                    new CancelTicket().execute();
                }
                else{
                    Toast.makeText(CancelTicketActivity.this,
                            "Please Select at least one seat", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    public class CancelTicket extends AsyncTask<String, String, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(CancelTicketActivity.this, "", "Please Wait  under Process.....");
        }


        @Override
        protected String doInBackground(String... params) {
            String strurl = Constants.BUSAPI + Constants.CancelBusTicket + "referenceNo=" + Refernce_No + "&seatNos="+SelectedSeats+"&emailId="+EMAIL_ID;
            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(strurl);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpGet.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();


            ServiceHandler sh = new ServiceHandler();
            data = sh.makeServiceCall(strurl, ServiceHandler.GET);

            System.out.println("JSON LENGTH" + strurl);
            /*try {
                HttpResponse response = httpClient.execute(httpGet);
                code = response.getStatusLine().getStatusCode();
                data = httpClient.execute(httpGet, responseHandler);
            } catch (IOException e) {
                e.printStackTrace();
            }*/


            return data;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //  JSONObject jsonObject = null;
            try {


                System.out.println("BookingDetails" + data);

                dialog.dismiss();

                if(code==403){
                    Toast.makeText(CancelTicketActivity.this,
                            "Error occurred", Toast.LENGTH_LONG).show();
                    // Error_Invalid_Reference.setVisibility(View.VISIBLE);
                }else{
                    JSONObject jsonObject = new JSONObject(data);


                    String Status = jsonObject.getString("BookingStatus");
                    String TicketNumber = jsonObject.getString("TicketNumber");
                    String TotalTicketFare = jsonObject.optString("TotalTicketFare");
                    String TotalRefundAmount = jsonObject.optString("TotalRefundAmount");
                    String Message = jsonObject.optString("Message");
                    String CancellationCharges = jsonObject.optString("CancellationCharges");
                    String RefundType = jsonObject.optString("RefundType");
                    String isSeatCancellable = jsonObject.optString("isSeatCancellable");
                    String PartialCancellationAllowed = jsonObject.optString("PartialCancellationAllowed");

                    if(Status.equals("5")){
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CancelTicketActivity.this);

                        // set title
                        alertDialogBuilder.setTitle("Cancel Your Booking");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Ticket Cancelled Successfully")
                                .setCancelable(false)
                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // if this button is clicked, close
                                        // current activity

                                        dialog.cancel();
                                        Intent i = new Intent(CancelTicketActivity.this, CancelBooking.class);
                                        startActivity(i);
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();

                    }else if(Status.equals("6")){
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CancelTicketActivity.this);

                        // set title
                        alertDialogBuilder.setTitle("Cancel Your Booking");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Partial cancellation is not allowed for this ticket")
                                .setCancelable(false)
                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // if this button is clicked, close
                                        // current activity

                                        dialog.cancel();

                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();

                    }else{
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CancelTicketActivity.this);

                        // set title
                        alertDialogBuilder.setTitle("Cancel Your Booking");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage(""+Message)
                                .setCancelable(false)
                                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // if this button is clicked, close
                                        // current activity

                                        dialog.cancel();
                                        Intent i = new Intent(CancelTicketActivity.this, CancelBooking.class);
                                        startActivity(i);
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();

                    }





                }



            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
}
