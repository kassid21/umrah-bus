package com.umrahbus.draweritems;

/**
 * Created by jagadeesh on 3/16/2017.
 */

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.umrahbus.Constants;
import com.umrahbus.MainActivity;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.umrahbus.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class TicketFormatActivity extends AppCompatActivity implements View.OnClickListener {

    View success, failed, pending;
    String newString, stringname, stringmessage, stringReturnReferencenumber;
    TextView referenceno, message, name;
    Button clickhere;
    ProgressDialog dialog;
    String data, refno;
    InputStream inputStream;
    int code;
    TextView journeydate, FromName, DisnationName, Boards, BoardingPoint, TRavel, Seat, Passengers, BoardingpointDetails, TicketNumber, Total, PNRNumber, HotelName;
    TextView Returnjourneydate, ReturnFromName, ReturnDisnationName, ReturnBoards, ReturnBoardingPoint, ReturnTRavel, ReturnSeat, ReturnPassengers, ReturnBoardingpointDetails, ReturnTicketNumber, ReturnTotal, ReturnPNRNumber, ReturnHotelName;
    CardView ReturnticketDetails, ReturnticketPNR;

    TextView RetFromname, RetToname, Ret_Boading_time, Ret_Boadingname, Ret_Boading_adderesss;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    TextView Status_tv;
    protected void onCreate(Bundle savedInstanstate) {
        super.onCreate(savedInstanstate);
        setContentView(R.layout.booking);

        if (savedInstanstate == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                newString = null;
                refno = null;
                stringmessage = null;
                stringname = null;
            } else {
                newString = extras.getString("status");
                refno = extras.getString("Onwardrefno");
                stringname = extras.getString("name");
                stringmessage = extras.getString("msg");
                stringReturnReferencenumber = extras.getString("Returnrefno");

            }
        } else {
            newString = (String) savedInstanstate.getSerializable("status");
            refno = (String) savedInstanstate.getSerializable("Onwardrefno");
            stringname = (String) savedInstanstate.getSerializable("name");
            stringmessage = (String) savedInstanstate.getSerializable("msg");
            stringReturnReferencenumber = (String) savedInstanstate.getSerializable("Returnrefno");
        }
        journeydate = (TextView) findViewById(R.id.journeydate);
        FromName = (TextView) findViewById(R.id.FromAddress);
        DisnationName = (TextView) findViewById(R.id.ToAddress);
        Boards = (TextView) findViewById(R.id.Boards);
        BoardingPoint = (TextView) findViewById(R.id.BoardingPoint);
        TRavel = (TextView) findViewById(R.id.TRavel);
        Seat = (TextView) findViewById(R.id.Seat);
        Passengers = (TextView) findViewById(R.id.Passengers);
        BoardingpointDetails = (TextView) findViewById(R.id.BoardingpointDetails);
        TicketNumber = (TextView) findViewById(R.id.TicketNumber);
        Total = (TextView) findViewById(R.id.Total);
        PNRNumber = (TextView) findViewById(R.id.PNRNumber);
        HotelName = (TextView) findViewById(R.id.HotelName);
        Status_tv= (TextView) findViewById(R.id.status_tv);

        ReturnticketDetails = (CardView) findViewById(R.id.ReturnticketDetails);
        ReturnticketPNR = (CardView) findViewById(R.id.ReturnticketPnrDetails);
        Returnjourneydate = (TextView) findViewById(R.id.Retuenjourneydate);
        ReturnFromName = (TextView) findViewById(R.id.ReturnFromAddress);
        ReturnDisnationName = (TextView) findViewById(R.id.ReturnToAddress);
        ReturnBoards = (TextView) findViewById(R.id.ReturnBoards);
        ReturnBoardingPoint = (TextView) findViewById(R.id.ReturnBoardingPoint);
        ReturnTRavel = (TextView) findViewById(R.id.ReturnTRavel);
        ReturnSeat = (TextView) findViewById(R.id.ReturnSeat);
        ReturnPassengers = (TextView) findViewById(R.id.ReturnPassengers);
        ReturnBoardingpointDetails = (TextView) findViewById(R.id.ReturnBoardingpointDetails);
        ReturnTicketNumber = (TextView) findViewById(R.id.ReturnTicketNumber);
        ReturnTotal = (TextView) findViewById(R.id.ReturnTotal);
        ReturnPNRNumber = (TextView) findViewById(R.id.ReturnPNRNumber);
        ReturnHotelName = (TextView) findViewById(R.id.ReturnHotelName);


        RetFromname = (TextView) findViewById(R.id.From_return);
        RetToname= (TextView) findViewById(R.id.To_return);
        Ret_Boading_time= (TextView) findViewById(R.id.Return_Boards);
        Ret_Boadingname= (TextView) findViewById(R.id.Return_BoardingPoint);
        Ret_Boading_adderesss= (TextView) findViewById(R.id.ReturnBoadingdetails);


        if (newString.equals("5")){
            Status_tv.setText("Not Booked");
        }

        new OnwardTicketDetails().execute();


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onClick(View v) {
        if (v == clickhere) {
            if (newString.equals("1") || newString.equals("4") || newString.equals("3")) {
                Intent i = new Intent(TicketFormatActivity.this, MainActivity.class);
                startActivity(i);
            }
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("TicketFormat Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }


    public class OnwardTicketDetails extends AsyncTask<String, String, String> {

        String Time,Time1;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(TicketFormatActivity.this, "", "Please Wait  under Process.....");
        }


        @Override
        protected String doInBackground(String... params) {
            String strurl = Constants.BUSAPI + Constants.BookingDetails + "referenceNo=" + refno + "&type=2";
            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(strurl);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpGet.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();


            try {

                HttpResponse response = httpClient.execute(httpGet);
                code = response.getStatusLine().getStatusCode();
                data = httpClient.execute(httpGet, responseHandler);

            } catch (IOException e) {
                e.printStackTrace();
            }


            return data;


        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            //  JSONObject jsonObject = null;
            try {


                System.out.println("BookingDetails" + code);


                    JSONObject jsonObject = new JSONObject(data);


                    String BookingDate = jsonObject.getString("BookingDate");
                    String APIRefNo = jsonObject.getString("APIRefNo");
                    String ActualFare = jsonObject.getString("ActualFare");
                    String SourceName = jsonObject.optString("SourceName");
                    String DestinationName = jsonObject.optString("DestinationName");
                    String JourneyDate = jsonObject.optString("JourneyDate");
                    String ReturnDate=jsonObject.getString("ReturnDate");
                    String OperatorName = jsonObject.optString("Operator");
                    String BusTypeName = jsonObject.optString("BusTypeName");
                    String PassengerName = jsonObject.optString("PassengerName");
                    String SeatNos = jsonObject.optString("SeatNos");
                    String CancellationPolicy = jsonObject.optString("CancellationPolicy");
                    JSONObject BoardingDroppingDetails = jsonObject.getJSONObject("BoardingDroppingDetails");
                    String Address = BoardingDroppingDetails.optString("Address");
                    String Landmark = BoardingDroppingDetails.optString("Landmark");
                    String Location = BoardingDroppingDetails.optString("Location");
                    String Hotel = jsonObject.getString("HotelName");


                    JSONObject ReturnBoardingDetails = jsonObject.getJSONObject("DroppingDetails");

                    String ReturnAddress = ReturnBoardingDetails.optString("Address");
                    String ReturnLandmark = ReturnBoardingDetails.optString("Landmark");
                    String ReturnLocation = ReturnBoardingDetails.optString("Location");


                    String Names = PassengerName.replace("~", " ,");
                    String SEATS = SeatNos.replace("~", " ,");
                    int time = Integer.parseInt(BoardingDroppingDetails.getString("Time"));
                    int ret_time=Integer.parseInt(ReturnBoardingDetails.getString("Time"));

                    double time2 = time / 60;
                    DecimalFormat df = new DecimalFormat("##.#");
                    int minuts = time % 60;
                    String timestring = df.format(time2) + ":" + minuts;
                    final String Btime = timestring;

                    try {
                        String _24HourTime = Btime;
                        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
                        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                        Date _24HourDt = _24HourSDF.parse(_24HourTime);
                        System.out.println(_24HourDt);
                        Time = _12HourSDF.format(_24HourDt);

                        System.out.println(Time);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    double time3 = ret_time / 60;
                    DecimalFormat df1 = new DecimalFormat("##.#");
                    int minuts1 = ret_time % 60;
                    String timestring1 = df1.format(time3) + ":" + minuts1;
                    final String Btime1 = timestring1;

                    try {
                        String _24HourTime = Btime1;
                        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
                        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                        Date _24HourDt = _24HourSDF.parse(_24HourTime);
                        System.out.println(_24HourDt);
                        Time1 = _12HourSDF.format(_24HourDt);

                        System.out.println(Time1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date myDate = null;
                    Date retdate=null;
                    try {
                        myDate = dateFormat.parse(JourneyDate);
                        retdate=dateFormat.parse(ReturnDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM,yyyy");
                    String finalDate = timeFormat.format(myDate);

                    SimpleDateFormat dateFormat1=new SimpleDateFormat("dd MMM,yyyy");
                    String ret_date_final=dateFormat1.format(retdate);

                    System.out.println("DATE FORMAT :" + finalDate);
                    Boards.setText("" + finalDate + " ," + Time);
                    FromName.setText("" + SourceName);
                    DisnationName.setText("" + DestinationName);
                    journeydate.setText("" + BookingDate);
                    BoardingPoint.setText("" + Location);
                    TRavel.setText("" + OperatorName + "\n" + BusTypeName);
                    Seat.setText("" + SEATS);
                    Passengers.setText("" + Names);
                    BoardingpointDetails.setText("" + Address + "\n" + "" + Landmark + "\n" + Location);
                    TicketNumber.setText("" + refno);
                    Total.setText("" + ActualFare);
                    PNRNumber.setText("" + APIRefNo);
                    HotelName.setText("" + Hotel);

                    RetFromname.setText(""+DestinationName);
                    RetToname.setText(""+SourceName);
                    Ret_Boadingname.setText(""+ReturnLocation);
                    Ret_Boading_time.setText(""+ret_date_final+","+Time1);
                    Ret_Boading_adderesss.setText("" + ReturnAddress + "\n" + "" + ReturnLandmark + "\n" + ReturnLocation);




                    if (stringReturnReferencenumber != null) {
                        new ReturnticketDetails().execute();
                    } else {
                        dialog.dismiss();
                    }



            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public class ReturnticketDetails extends AsyncTask<String, String, String> {

        String Time;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(TicketFormatActivity.this, "", "Please Wait  under Process.....");
        }


        @Override
        protected String doInBackground(String... params) {
            String strurl = Constants.BUSAPI + Constants.BookingDetails + "referenceNo=" + stringReturnReferencenumber + "&type=2";
            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(strurl);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpGet.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();


            try {

                HttpResponse response = httpClient.execute(httpGet);
                code = response.getStatusLine().getStatusCode();
                data = httpClient.execute(httpGet, responseHandler);

            } catch (IOException e) {
                e.printStackTrace();
            }


            return data;


        }

        @SuppressLint("SetTextI18n")
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //  JSONObject jsonObject = null;
            try {


                System.out.println("BookingDetails" + code);
                dialog.dismiss();
                if (code == 406) {
                    //  Error_Invalid_Reference.setVisibility(View.VISIBLE);
                } else {
                    JSONObject jsonObject = new JSONObject(data);


                    String BookingDate = jsonObject.getString("BookingDate");
                    String APIRefNo = jsonObject.getString("APIRefNo");
                    String ActualFare = jsonObject.getString("ActualFare");
                    String SourceName = jsonObject.optString("SourceName");
                    String DestinationName = jsonObject.optString("DestinationName");
                    String JourneyDate = jsonObject.optString("JourneyDate");
                    String OperatorName = jsonObject.optString("Operator");
                    String BusTypeName = jsonObject.optString("BusTypeName");
                    String PassengerName = jsonObject.optString("PassengerName");
                    String SeatNos = jsonObject.optString("SeatNos");
                    String CancellationPolicy = jsonObject.optString("CancellationPolicy");
                    JSONObject BoardingDroppingDetails = jsonObject.getJSONObject("BoardingDroppingDetails");
                    String Address = BoardingDroppingDetails.optString("Address");
                    String Landmark = BoardingDroppingDetails.optString("Landmark");
                    String Location = BoardingDroppingDetails.optString("Location");
                    String Hotel = jsonObject.getString("HotelName");

                    String Names = PassengerName.replace("~", " ,");
                    String SEATS = SeatNos.replace("~", " ,");
                    int time = Integer.parseInt(BoardingDroppingDetails.getString("Time"));
                    double time2 = time / 60;
                    DecimalFormat df = new DecimalFormat("##.#");
                    int minuts = time % 60;
                    String timestring = df.format(time2) + ":" + minuts;
                    final String Btime = timestring;

                    try {
                        String _24HourTime = Btime;
                        SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
                        SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
                        Date _24HourDt = _24HourSDF.parse(_24HourTime);
                        System.out.println(_24HourDt);
                        Time = _12HourSDF.format(_24HourDt);

                        System.out.println(Time);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date myDate = null;
                    try {
                        myDate = dateFormat.parse(JourneyDate);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    SimpleDateFormat timeFormat = new SimpleDateFormat("dd MMM,yyyy");
                    String finalDate = timeFormat.format(myDate);

                    ReturnBoards.setText("" + finalDate + " ," + Time);
                    ReturnFromName.setText("" + SourceName);
                    ReturnDisnationName.setText("" + DestinationName);
                    Returnjourneydate.setText("" + BookingDate);
                    ReturnBoardingPoint.setText("" + Location);
                    ReturnTRavel.setText("" + OperatorName + "\n" + BusTypeName);
                    ReturnSeat.setText("" + SEATS);
                    ReturnPassengers.setText("" + Names);
                    ReturnBoardingpointDetails.setText("" + Address + "\n" + "" + Landmark + "\n" + Location);
                    ReturnTicketNumber.setText("" + stringReturnReferencenumber);
                    ReturnTotal.setText("" + ActualFare);
                    ReturnPNRNumber.setText("" + APIRefNo);
                    ReturnHotelName.setText("" + Hotel);


                    ReturnticketDetails.setVisibility(View.VISIBLE);
                    ReturnticketPNR.setVisibility(View.VISIBLE);
                }

            } catch (Exception e) {

            }

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        startActivity(new Intent(this,MainActivity.class));
    }
}
