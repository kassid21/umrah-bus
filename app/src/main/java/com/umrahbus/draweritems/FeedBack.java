package com.umrahbus.draweritems;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.umrahbus.Constants;
import com.umrahbus.MainActivity;
import com.umrahbus.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.UUID;

/**
 * Created by jagadeesh on 6/24/2016.
 */
public class FeedBack extends AppCompatActivity {
    EditText email,description,mobile,name;
    Button submit;
    String mail,descript,number,data,refid,sname;
    InputStream inputStream;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    protected void onCreate(Bundle savedInstaceState){
        super.onCreate(savedInstaceState);
        setContentView(R.layout.activity_feedback);
        pref = getSharedPreferences("I2space", 0);
        editor = pref.edit();
        email=(EditText)findViewById(R.id.feedbackmail);

        if(pref.getBoolean("ISLOGIN",false)==true){
            email.setText(pref.getString("username",""));
        }else {
            email.setHint("please enter Email Id");
        }
        description=(EditText)findViewById(R.id.feedbacktext);
        mobile=(EditText)findViewById(R.id.feedbackmobile);
        name=(EditText)findViewById(R.id.feedbackname);
        submit=(Button)findViewById(R.id.feedbacksubmit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mail = email.getText().toString();
                descript = description.getText().toString();
                number = mobile.getText().toString();
                sname = name.getText().toString();
                try {
                   int length = number.length();
                    if ((!sname.equals("")&&!mail.equals("")&& mail.matches(Constants.emailPattern)&& length==9 && (!descript.equals("")) && (!number.equals("")))) {
                        new Feedbacktask().execute();
                    } else {
                        if ((sname.equals("")||mail.equals("") || descript.equals("") || number.equals("")|| length < 9  || !mail.matches(Constants.emailPattern))) {
                            if (sname.equals("")){
                                name.setError("Enter Your Name");
                            }
                            if (mail.equals("")) {
                                email.setError("Enter Email");
                            }
                            if (descript.equals("")) {
                                description.setError("Enter Your Feedback");
                            }
                            if (number.equals("")) {
                                mobile.setError("Enter Mobile Number");
                            }
                            if (!mail.matches(Constants.emailPattern)) {
                                email.setError("Enter Valid Email");
                            }
                            if (length < 9) {
                                mobile.setError("Enter Valid Mobile No");
                            }
                        } else {
                            new Feedbacktask().execute();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });


    }


    public class Feedbacktask extends AsyncTask<String,String,String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog= ProgressDialog.show(FeedBack.this,"","\t \tPlease Wait\nlet us submit your feedback");
        }

        @Override
        protected String doInBackground(String... params) {

            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            refid=uuid.substring(0,6);
            // List<NameValuePair> bcparam=new ArrayList<NameValuePair>();
            JSONObject bcparam = new JSONObject();
            // List<NameValuePair> parm=new ArrayList<NameValuePair>();
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(Constants.BUSAPI+ Constants.feedbackurl);
            httpPost.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpPost.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            try {
                bcparam.put("Catagory", "Feedback");
                bcparam.put("Name", sname);
                bcparam.put("EmailId", mail);

                bcparam.put("Mobile", number);
                bcparam.put("Description", descript);
                bcparam.put("ReferenceNumber",refid);


                // httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                // parm.add(new BasicNameValuePair("res",bcparam.toString()));
                try {
                    StringEntity seEntity;
                    // String data1 = null;
				/*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                    seEntity = new StringEntity(bcparam.toString(), "UTF_8");
                    seEntity.setContentType("application/json");
                    httpPost.setEntity(seEntity);
                    HttpResponse httpResponse;

                    httpResponse = httpClient.execute(httpPost);

                    inputStream = httpResponse.getEntity().getContent();

                } catch (Exception e) {
                    e.printStackTrace();
                }
                //json=bcparam.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream));
            String line = "";
            data = "";
            try {
                while ((line = bufferedReader.readLine()) != null)
                    data += line;
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                inputStream.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            Log.v("rsponse", "" + data);


            return data;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            dialog.dismiss();

            try{
                JSONObject object=new JSONObject(data);
                String str=object.getString("Message");
                if(str.equals("Success")){
                    feedbacksubmit();
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void  feedbacksubmit(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(" Your Feedback Id is  "+refid+" \nThanks for your valuable feedback :)")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent i = new Intent(FeedBack.this, MainActivity.class);
                        startActivity(i);
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
