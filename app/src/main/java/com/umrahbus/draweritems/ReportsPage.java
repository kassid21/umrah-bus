package com.umrahbus.draweritems;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.umrahbus.BeanClasses.Getreports;
import com.umrahbus.Constants;
import com.umrahbus.MainActivity;
import com.umrahbus.ServicesClasses.ServiceHandler;
import com.umrahbus.loginpackage.LoginActivity;
import com.umrahbus.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by jagadeesh on 8/11/2016.
 */
public class ReportsPage extends AppCompatActivity {

    SharedPreferences.Editor editor;
    SharedPreferences pref;

    String AgentId;
    private ListView listView;
    String userLogin, agentLogin, SuperAdmin;
    String UserId, User_agentid,Role,ClientId,ID;

    ProgressDialog progressDialog;
    InputStream inputStream;
    String data;
    Getreports BusDetailsBean;
    private ArrayList<Getreports> postArrayList = new ArrayList<>();
    private ReportsAdapter Repotrtsadapter;
    String Agentid;
    /* servicesArray = [[NSArray alloc] initWithObjects:@"Blocked",@"BlockingFailed",@"Booked",@"Cancelled",@"CancellationFailed",@"PartiallyCancelled",@"PartialCancellationFailed",@"Failed",@"Visited",nil];
     servicesCodeArray = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",nil];*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_getreports);

        pref = getSharedPreferences("LoginPreference", 0);
        editor = pref.edit();
        AgentId=pref.getString("Clientname",null);

        pref = getSharedPreferences("LoginPreference", MODE_PRIVATE);
        editor = pref.edit();
        userLogin = pref.getString("UserLOGIN", null);
        agentLogin = pref.getString("AgentLOGIN", null);
        ClientId=pref.getString("ClientId",null);
        Agentid = pref.getString("AgentId", null);
        User_agentid = pref.getString("User_agentid", null);

        new Decrypt().execute();

        listView = (ListView) findViewById(R.id.reports_listview);

    }

    public class Decrypt extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ReportsPage.this, "", "Please Wait...");
        }

        @Override
        protected String doInBackground(String... params) {
            ServiceHandler sh = new ServiceHandler();
            String jsonstr1 = sh.makeServiceCall(Constants.BUSAPI+ Constants.DecriptCode+User_agentid, ServiceHandler.GET);
            if (jsonstr1 != "") {
                try {
                    JSONObject joj = new JSONObject(jsonstr1);

                    String decryptcode = joj.getString("StatusCode");

                    if (decryptcode.equals("200")) {
                        ID=joj.getString("Message");


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if (userLogin != null && userLogin.equals("Yes")) {
                UserId = ID;
                Role = "6";
                User_agentid = pref.getString("User_agentid", null);
                new GetReportsData().execute();
            } else if (agentLogin != null && agentLogin.equals("Yes")) {
                UserId = ID;
                Role = "4";
                User_agentid = pref.getString("User_agentid", null);
                new GetReportsData().execute();
            } else {
                Intent i=new Intent(ReportsPage.this, LoginActivity.class);
                startActivity(i);
            }

        }
    }
    ////User Login Functionality////////////////
    public class GetReportsData extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(ReportsPage.this, "", "Please Wait..\nGood things take time :)");
        }

        @Override
        protected String doInBackground(String... params) {
            {


                JSONObject bcparam = new JSONObject();
                // List<NameValuePair> parm=new ArrayList<NameValuePair>();
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(Constants.BUSAPI + Constants.Reports);
                httpPost.setHeader("ConsumerKey", Constants.ConsumerKey);
                httpPost.setHeader("ConsumerSecret", Constants.ConsumerSecret);
                try {
                    bcparam.put("AgentId", UserId);
                    bcparam.put("BookingStatus", "");
                    bcparam.put("Role", Role);
                    bcparam.put("Service", "1");
                    bcparam.put("Fromdate", "");
                    bcparam.put("ToDate", "");
                    bcparam.put("BookingRefNo", "");
                    bcparam.put("APIReferenceNo", "");
                    bcparam.put("RechargeType", "");
                    bcparam.put("RechargeNumber", "");
                    bcparam.put("ClientId", "");



                    System.out.println("Json Object :" + bcparam);
                    try {
                        StringEntity seEntity;
                        // String data1 = null;
				/*
				 * try { data1 = URLEncoder.encode(jsonObjectrows, "UTF-8"); }
				 * catch (UnsupportedEncodingException e2) { // TODO
				 * Auto-generated catch block e2.printStackTrace(); }
				 */
                        seEntity = new StringEntity(bcparam.toString(), "UTF_8");
                        seEntity.setContentType("application/json");
                        httpPost.setEntity(seEntity);
                        HttpResponse httpResponse;

                        httpResponse = httpClient.execute(httpPost);

                        inputStream = httpResponse.getEntity().getContent();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream));
                String line = "";
                data = "";
                try {
                    while ((line = bufferedReader.readLine()) != null)
                        data += line;
                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    inputStream.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                    System.out.println("Responce :" + data);

                }
                return data;
            }
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            JSONObject jsonObj = null;
            String Status = null;

            try {

                if (data.equals("null")) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ReportsPage.this);

                    // set title
                    alertDialogBuilder.setTitle("My Trips");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("No Trips Available")
                            .setCancelable(false)
                            .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, close
                                    // current activity

                                    dialog.cancel();
                                    Intent i = new Intent(ReportsPage.this, MainActivity.class);
                                    startActivity(i);
                                }
                            });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();
                }else {

                    JSONArray JsonArray = new JSONArray(data);
                    for (int i = 0; i < JsonArray.length(); i++) {
                        BusDetailsBean = new Getreports();

                        JSONObject jobj = JsonArray.getJSONObject(i);

                        BusDetailsBean.setBookingDate(jobj.getString("BookingDate"));
                        BusDetailsBean.setBookingRefNo(jobj.getString("BookingRefNo"));
                        BusDetailsBean.setHotelName(jobj.getString("HotelName"));
                        BusDetailsBean.setEmailID(jobj.getString("EmailId"));
                        BusDetailsBean.setOperatorNamey(jobj.getString("OperatorName"));
                        BusDetailsBean.setPassengerName(jobj.getString("PassengerName"));
                        BusDetailsBean.setSourceName(jobj.getString("SourceName"));
                        BusDetailsBean.setDestinationName(jobj.getString("DestinationName"));
                        BusDetailsBean.setJourneyDate(jobj.getString("JourneyDate"));
                        BusDetailsBean.setNoofSeats(jobj.getString("NoofSeats"));
                        BusDetailsBean.setBookingStatus(jobj.getString("BookingStatus"));
                        BusDetailsBean.setFares(jobj.getString("CollectedFare"));
                        BusDetailsBean.setServicetax(jobj.getString("Servicetax"));
                        BusDetailsBean.setServiceCharge(jobj.getString("ServiceCharge"));
                        BusDetailsBean.setDepartureTime(jobj.getString("DepartureTime"));
                        BusDetailsBean.setReturndate(jobj.getString("ReturnDate"));
                        String Returndate=jobj.getString("ReturnDate");
                        System.out.println(Returndate);
                        BusDetailsBean.setArravialTime(jobj.getString("ArrivalTime"));


                        postArrayList.add(BusDetailsBean);
                    }
                    Repotrtsadapter = new ReportsAdapter(postArrayList, ReportsPage.this);
                    // setListAdapter(myAppAdapter);
                    listView.setAdapter(Repotrtsadapter);

                }
                progressDialog.dismiss();
            } catch (JSONException e) {
                e.printStackTrace();
            }



        }
    }

    public class ReportsAdapter extends BaseAdapter {

        public class ViewHolder {
            TextView transation, travele ,bookingstatus, journeydate,numberofseats,amount,operater,pax,ref_number,Hotelname,
                    return_date,return_jourmey,PaxEmail;


        }

        public ArrayList<Getreports> ReportsList;

        public Context context;
        ArrayList<Getreports> arraylist;

        private ReportsAdapter(ArrayList<Getreports> apps, Context context) {
            this.ReportsList = apps;
            this.context = context;
            arraylist = new ArrayList<>();
            arraylist.addAll(ReportsList);

        }

        @Override
        public int getCount() {
            return ReportsList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;
            ViewHolder viewHolder;

            if (rowView == null) {
                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.activity_getreports_item, null);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.transation = (TextView) rowView.findViewById(R.id.transaction_status);
                viewHolder.travele = (TextView) rowView.findViewById(R.id.traveler);
                viewHolder.bookingstatus = (TextView) rowView.findViewById(R.id.Booking_status);
                viewHolder.journeydate = (TextView) rowView.findViewById(R.id.journey_date);
                viewHolder.numberofseats = (TextView) rowView.findViewById(R.id.Number_of_seats);
                viewHolder.amount = (TextView) rowView.findViewById(R.id.Amount);
                viewHolder.operater = (TextView) rowView.findViewById(R.id.Operater);
                viewHolder.pax = (TextView) rowView.findViewById(R.id.Pax);
                viewHolder.ref_number = (TextView) rowView.findViewById(R.id.ref_number);
                viewHolder.Hotelname= (TextView) rowView.findViewById(R.id.HotelName);
                viewHolder.return_date= (TextView) rowView.findViewById(R.id.ret_journey_date);
                viewHolder.return_jourmey = (TextView) rowView.findViewById(R.id.traveler2);
                viewHolder.PaxEmail = (TextView) rowView.findViewById(R.id.PaxEmail);

                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.transation.setText(ReportsList.get(position).getBookingDate() + "");
            viewHolder.travele.setText(ReportsList.get(position).getSourceName() + " To "+ReportsList.get(position).getDestinationName());

            viewHolder.journeydate.setText(ReportsList.get(position).getJourneyDate() + " , "+ReportsList.get(position).getDepartureTime());
            viewHolder.numberofseats.setText(ReportsList.get(position).getNoofSeats() + "");

            viewHolder.amount.setText(ReportsList.get(position).getFares() + "");
            viewHolder.operater.setText(ReportsList.get(position).getOperatorNamey() + "");
            viewHolder.pax.setText(ReportsList.get(position).getPassengerName() + "");
            viewHolder.ref_number.setText(ReportsList.get(position).getBookingRefNo() + "");
            viewHolder.ref_number.setTextColor(getResources().getColor(R.color.colorPrimary));
            viewHolder.Hotelname.setText(ReportsList.get(position).getHotelName()+"");
            viewHolder.PaxEmail.setText(ReportsList.get(position).getEmailID()+"");
            viewHolder.return_date.setText(ReportsList.get(position).getReturndate()+ " , "+ReportsList.get(position).getArravialTime());

            viewHolder.return_jourmey.setText(ReportsList.get(position).getDestinationName() + " To "+ReportsList.get(position).getSourceName());


            if(ReportsList.get(position).getBookingStatus().equals("1")){
                viewHolder.bookingstatus.setText( "Blocked");
            }else  if(ReportsList.get(position).getBookingStatus().equals("2")){
                viewHolder.bookingstatus.setText( "BlockingFailed");
            }else  if(ReportsList.get(position).getBookingStatus().equals("3")){
                viewHolder.bookingstatus.setText( "Booked");
            }else  if(ReportsList.get(position).getBookingStatus().equals("4")){
                viewHolder.bookingstatus.setText( "BookingFailed");
            }else  if(ReportsList.get(position).getBookingStatus().equals("5")){
                viewHolder.bookingstatus.setText( "Cancelled");
            }else  if(ReportsList.get(position).getBookingStatus().equals("6")){
                viewHolder.bookingstatus.setText( "CancellationFailed");
            }else  if(ReportsList.get(position).getBookingStatus().equals("7")){
                viewHolder.bookingstatus.setText( "PartiallyCancelled");
            }else  if(ReportsList.get(position).getBookingStatus().equals("8")){
                viewHolder.bookingstatus.setText( "PartialCancellationFailed");
            }else  if(ReportsList.get(position).getBookingStatus().equals("9")){
                viewHolder.bookingstatus.setText( "Failed");
            }else  if(ReportsList.get(position).getBookingStatus().equals("10")){
                viewHolder.bookingstatus.setText( "Visited");
            }


            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(ReportsList.get(position).getBookingStatus().equals("3")){
                        Intent i = new Intent(context, TicketFormatActivity.class);
                        i.putExtra("Onwardrefno", ReportsList.get(position).getBookingRefNo());
                        i.putExtra("status","4");
                        context.startActivity(i);
                    }else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ReportsPage.this);
                        builder.setMessage("Ticket details not found !")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        //do things
                                        dialog.dismiss();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }


                }
            });
            return rowView;


        }
    }

}

