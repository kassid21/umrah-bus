package com.umrahbus.draweritems;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.umrahbus.R;

/**
 * Created by Administrator on 17-05-2016.
 */
public class FaqsActivity extends AppCompatActivity implements View.OnClickListener {

    TextView qtn1,ans1,qtn2,ans2,qtn3,ans3,qtn4,ans4,qtn5,ans5,qtn6,ans6,qtn7,ans7,qtn8,ans8,qtn9,ans9,qtn10,ans10;
    protected void  onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);
        qtn1=(TextView)findViewById(R.id.qstn1);
        ans1=(TextView)findViewById(R.id.ans1);
        qtn2=(TextView)findViewById(R.id.qstn2);
        ans2=(TextView)findViewById(R.id.ans2);
        qtn3=(TextView)findViewById(R.id.qstn3);
        ans3=(TextView)findViewById(R.id.ans3);
        qtn4=(TextView)findViewById(R.id.qstn4);
        ans4=(TextView)findViewById(R.id.ans4);
       /* qtn5=(TextView)findViewById(R.id.qstn5);
        ans5=(TextView)findViewById(R.id.ans5);
        qtn6=(TextView)findViewById(R.id.qstn6);
        ans6=(TextView)findViewById(R.id.ans6);
        qtn7=(TextView)findViewById(R.id.qstn7);
        ans7=(TextView)findViewById(R.id.ans7);
        qtn8=(TextView)findViewById(R.id.qstn8);
        ans8=(TextView)findViewById(R.id.ans8);
        qtn9=(TextView)findViewById(R.id.qstn9);
        ans9=(TextView)findViewById(R.id.ans9);
        qtn10=(TextView)findViewById(R.id.qstn10);
        ans10=(TextView)findViewById(R.id.ans10);
*/
       /* qtn1.setOnClickListener(this);
        qtn2.setOnClickListener(this);
        qtn3.setOnClickListener(this);
        qtn4.setOnClickListener(this);
        qtn5.setOnClickListener(this);
        qtn6.setOnClickListener(this);
        qtn7.setOnClickListener(this);
        qtn8.setOnClickListener(this);
        qtn9.setOnClickListener(this);
        qtn10.setOnClickListener(this);*/


    }

    @Override
    public void onClick(View v) {



        if(v==qtn1){
            ans1.setVisibility(View.VISIBLE);
            ans2.setVisibility(View.GONE);
            ans3.setVisibility(View.GONE);
            ans4.setVisibility(View.GONE);
            ans5.setVisibility(View.GONE);
            ans6.setVisibility(View.GONE);
            ans7.setVisibility(View.GONE);
            ans8.setVisibility(View.GONE);
            ans9.setVisibility(View.GONE);
            ans10.setVisibility(View.GONE);

        }


        if(v==qtn2){
            ans1.setVisibility(View.GONE);
            ans2.setVisibility(View.VISIBLE);
            ans3.setVisibility(View.GONE);
            ans4.setVisibility(View.GONE);
            ans5.setVisibility(View.GONE);
            ans6.setVisibility(View.GONE);
            ans7.setVisibility(View.GONE);
            ans8.setVisibility(View.GONE);
            ans9.setVisibility(View.GONE);
            ans10.setVisibility(View.GONE);

        }
        if(v==qtn3){
            ans1.setVisibility(View.GONE);
            ans2.setVisibility(View.GONE);
            ans3.setVisibility(View.VISIBLE);
            ans4.setVisibility(View.GONE);
            ans5.setVisibility(View.GONE);
            ans6.setVisibility(View.GONE);
            ans7.setVisibility(View.GONE);
            ans8.setVisibility(View.GONE);
            ans9.setVisibility(View.GONE);
            ans10.setVisibility(View.GONE);

        }
        if(v==qtn4){
            ans1.setVisibility(View.GONE);
            ans2.setVisibility(View.GONE);
            ans3.setVisibility(View.GONE);
            ans4.setVisibility(View.VISIBLE);
            ans5.setVisibility(View.GONE);
            ans6.setVisibility(View.GONE);
            ans7.setVisibility(View.GONE);
            ans8.setVisibility(View.GONE);
            ans9.setVisibility(View.GONE);
            ans10.setVisibility(View.GONE);

        }
        if(v==qtn5){
            ans1.setVisibility(View.GONE);
            ans2.setVisibility(View.GONE);
            ans3.setVisibility(View.GONE);
            ans4.setVisibility(View.GONE);
            ans5.setVisibility(View.VISIBLE);
            ans6.setVisibility(View.GONE);
            ans7.setVisibility(View.GONE);
            ans8.setVisibility(View.GONE);
            ans9.setVisibility(View.GONE);
            ans10.setVisibility(View.GONE);

        }
        if(v==qtn6){
            ans1.setVisibility(View.GONE);
            ans2.setVisibility(View.GONE);
            ans3.setVisibility(View.GONE);
            ans4.setVisibility(View.GONE);
            ans5.setVisibility(View.GONE);
            ans6.setVisibility(View.VISIBLE);
            ans7.setVisibility(View.GONE);
            ans8.setVisibility(View.GONE);
            ans9.setVisibility(View.GONE);
            ans10.setVisibility(View.GONE);

        }
        if(v==qtn7){
            ans1.setVisibility(View.GONE);
            ans2.setVisibility(View.GONE);
            ans3.setVisibility(View.GONE);
            ans4.setVisibility(View.GONE);
            ans5.setVisibility(View.GONE);
            ans6.setVisibility(View.GONE);
            ans7.setVisibility(View.VISIBLE);
            ans8.setVisibility(View.GONE);
            ans9.setVisibility(View.GONE);
            ans10.setVisibility(View.GONE);

        }
        if(v==qtn8){
            ans1.setVisibility(View.GONE);
            ans2.setVisibility(View.GONE);
            ans3.setVisibility(View.GONE);
            ans4.setVisibility(View.GONE);
            ans5.setVisibility(View.GONE);
            ans6.setVisibility(View.GONE);
            ans7.setVisibility(View.GONE);
            ans8.setVisibility(View.VISIBLE);
            ans9.setVisibility(View.GONE);
            ans10.setVisibility(View.GONE);

        }
        if(v==qtn9){
            ans1.setVisibility(View.GONE);
            ans2.setVisibility(View.GONE);
            ans3.setVisibility(View.GONE);
            ans4.setVisibility(View.GONE);
            ans5.setVisibility(View.GONE);
            ans6.setVisibility(View.GONE);
            ans7.setVisibility(View.GONE);
            ans8.setVisibility(View.GONE);
            ans9.setVisibility(View.VISIBLE);
            ans10.setVisibility(View.GONE);

        }
        if(v==qtn10){
            ans1.setVisibility(View.GONE);
            ans2.setVisibility(View.GONE);
            ans3.setVisibility(View.GONE);
            ans4.setVisibility(View.GONE);
            ans5.setVisibility(View.GONE);
            ans6.setVisibility(View.GONE);
            ans7.setVisibility(View.GONE);
            ans8.setVisibility(View.GONE);
            ans9.setVisibility(View.GONE);
            ans10.setVisibility(View.VISIBLE);

        }
    }
}
