package com.umrahbus.draweritems;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.umrahbus.R;

/**
 * Created by jagadeesh on 6/24/2016.
 */
public class TermsAndConditions extends AppCompatActivity {


    protected void onCreate(Bundle savedInstaceState) {
        super.onCreate(savedInstaceState);
        setContentView(R.layout.activity_terms_and_conditions);
        TextView t = (TextView)findViewById(R.id.Terms);
        TextView paymentTerms = (TextView)findViewById(R.id.payment_termsandConditions);
        TextView Text3 = (TextView)findViewById(R.id.text3);
        TextView Text4 = (TextView)findViewById(R.id.text4);
        TextView Text5 = (TextView)findViewById(R.id.text5);
        TextView Text6 = (TextView)findViewById(R.id.text6);
        TextView Text7 = (TextView)findViewById(R.id.text7);
        TextView Text8 = (TextView)findViewById(R.id.text8);
        TextView Text9 = (TextView)findViewById(R.id.text9);
        TextView Text10 = (TextView)findViewById(R.id.text10);
        TextView Text11 = (TextView)findViewById(R.id.text11);
        t.setText("Welcome to our website. If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern relationship with you in relation to this website.  ");

        paymentTerms.setText("The use of this website is subject to the following terms of use: " );

        Text3.setText("The content of the pages of this website is for your general information and use only. It is subject to change without notice. \n" +
                "Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.\n" +
                "Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.\n" +
                "This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.\n" +
                "Unauthorized use of this website may give rise to a claim for damages and/or be a criminal offence.");

        Text4.setText("Payment Terms & Conditions:  ");

        Text5.setText("We ensure that every transaction you conduct online is in a safe and secure environment. We do not take or store any credit card details or other card details on our servers. User will be directed to Payment Gateway website to enter his/her card details. This site is Master Secure & VBV compliant. \n" +
                "We reserve the right to refuse or cancel any order placed for a product that is listed at an incorrect price. This shall be regardless of whether the order has been confirmed and/or payment been levied via credit card. In the event the payment has been processed, the same shall be credited to your account and duly notified to you by email.\n" +
                "We shall not be liable for any credit card fraud. The liability to use a card fraudulently will be on the user and the onus to 'prove otherwise' shall be exclusively on the user.\n" +
                "If you have any additional queries or concerns, please contact us on the details provided in 'Contact Us' page of the website.\n" +
                "While compiling this information, we have endeavored to ensure that all information is correct. However, no guarantee or representation is made to the accuracy or completeness of the information contained here. This information is subject to changes without notice.");

        Text6.setText("Reservations, Bookings, & Purchases: ");

        Text7.setText("You agree to abide by the terms and conditions of purchase imposed by any supplier with whom you opt to deal.\n" +
                "Booking can be made online by filling up the booking form provided here. Payment can be made through any of the credit/debit cards listed here. Booking can be made for any person other than card holder also. For that you need to fill up details of card holder for payment and details of user for booking. The use of credit card is totally safe, we use EBS payment gateway for payment. EBS has a 128 bits SSL encryption technology compliant solution certified by VeriSign. Your card details are not shared.");


        Text8.setText("Bus Booking Terms:");
        Text9.setText("Apart from the general terms and conditions the below mentioned are the specific bus booking terms. \n" +
                "Our responsibilities include:\n" +
                "Issuing a valid ticket (a ticket that will be accepted by the bus operator) for its’ network of bus operators\n" +
                "Providing refund and support in the event of cancellation\n" +
                "Providing customer support and information in case of any delays / inconvenience\n" +
                "Our responsibilities do NOT include:\n" +
                "The bus operator’s bus not departing / reaching on time\n" +
                "The bus operator’s employees being rude the bus operator’s bus seats etc not being up to the customer’s expectation\n" +
                "The bus operator canceling the trip due to unavoidable reasons\n" +
                "The baggage of the customer getting lost / stolen / damaged\n" +
                "The bus operator changing a customer’s seat at the last minute to accommodate a lady / child\n" +
                "The customer waiting at the wrong boarding point (please call the bus operator to find out the exact boarding point if you are not a regular traveler on that particular bus)\n" +
                "The bus operator changing the boarding point and/or using a pick-up vehicle at the boarding point to take customers to the bus departure point\n"  );

        Text10.setText("Passengers are required to furnish the following at the time of boarding the bus: ");
        Text11.setText("A copy of the ticket (A print out of the ticket or the print out of the ticket e-mail). - Identity proof (Passport and IQAAMA).");
    }
}