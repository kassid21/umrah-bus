package com.umrahbus.draweritems;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.umrahbus.R;

/**
 * Created by Administrator on 17-05-2016.
 */
public class Aboutus extends AppCompatActivity {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);

        TextView t = (TextView)findViewById(R.id.AboutUs);
        TextView text1 = (TextView)findViewById(R.id.text1);
        TextView text2 = (TextView)findViewById(R.id.text2);
        TextView text3 = (TextView)findViewById(R.id.text3);
        TextView text4 = (TextView)findViewById(R.id.text4);
        TextView text7 = (TextView)findViewById(R.id.text7);
        t.setText("Umrah Bus is a complete user friendly customer app to book or buy bus tickets.\n" +
                "Easy to use, Fast, Reliable, Simple and the most Secure way to book your bus tickets in Cameroon.\n" +
                "\n" +
                "Now you can book your bus tickets on the move with Umrah Bus application. The app allows you to search and book bus tickets for all bus routes and also choose from all bus operators.\n" +
                "\n" +
                "Umrah Bus is the preferred choice of booking bus seats in when it comes to bus travel. Choose from seater, sleeper, semi-sleeper, A/C, non A/C buses.\n" +
                "\n" +
                "Download Now and avail exciting offers on bus tickets to destinations." );

        text1.setText("Key Features of the android app:");

        text2.setText("- Find buses & route information: Use the app to check availability and price of buses & operators \n" +
                "- 3 easy steps to book - find bus, select seat and pay online\n" +
                "- Real time bus ticket booking: Choose buses from all major bus operators for your route. Compare buses and see available seat positions instantly\n" +
                "- Search buses based on time, fare, ratings and seats availability\n" +
                "- Select Boarding points: Choose your boarding point on a map and get directions \n" +
                "- One touch Secure Payment: Save your card details & book tickets with a single tap\n" +
                "- Call support: Call our support team directly from the application\n" +
                "- Save ticket details: Download previously booked tickets and save them on the app\n" +
                "- Ratings: Get ratings of buses based on fellow customers feedback for each operator\n" +
                "- Amenities: Check out amenities available in the bus\n" +
                "- Exciting deals on all app bookings" );

        text3.setText("Payment options:" );

        text4.setText("You can now transact on the Umrah Bus app using: \n" +
                "- Credit cards\n" +
                "- Debit cards\n" +
                "- Phone booking options\n" );


        text7.setText("Additional Information\n" +
                "\n" +
                "Reserve seats & check fares from several Bus Operators \n" +
                "Book your bus tickets from leading operators in one mobile app.\n" +

                "********\n" +
                "To get more info about latest offers on bus tickets:\n" +
                "- Visit our official site at https://umrahbus.com/SearchBuses \n" +
                "- Follow us on Twitter at https://twitter.com/Umrah Bus\n" +
                "- Like us on Facebook at https://www.facebook.com/Umrah Bus\n" +
                "- Follow us on Google+ at https://plus.google.com/115805420708386010036\n" +
                "- Follow us on Linkedin at https://www.linkedin.com/company/Umrah Bus\n" +
                "- Subscribe on our Youtube Channel at https://www.youtube.com/channel/UCMrOXtTbIi-z4wlupIPUGKg");

    }

}
