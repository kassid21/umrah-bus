package com.umrahbus.draweritems;

/**
 * Created by Nagarjuna on 8/18/2017.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.umrahbus.AdapterClasses.OffersAdapter;
import com.umrahbus.BeanClasses.GetPromoCodesDTO;
import com.umrahbus.Constants;
import com.umrahbus.ServicesClasses.ServiceHandler;
import com.umrahbus.ServicesClasses.Utils;
import com.google.gson.Gson;
import com.umrahbus.R;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class Offers extends AppCompatActivity {

    RecyclerView offer_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offers);
        offer_view= (RecyclerView) findViewById(R.id.recycler_view);
        init();
    }

    private void init() {

        callPromoCodes();

    }

    private void callPromoCodes() {
        if(Utils.isNetworkAvailable(getApplicationContext())) {
            ServiceHandler.getPromocodes(Offers.this, Constants.GETPROMOCODES);
        }else{
            Toast.makeText(this, "No Internet ", Toast.LENGTH_SHORT).show();
        }
    }


    public void getPromoCodesResponse(String response) {
        System.out.println("Offers---"+response);
        //Util.showMessage(this,response);
        if(response!=null) {
            InputStream stream = new ByteArrayInputStream(response.getBytes());
            Gson gson = new Gson();
            Reader reader = new InputStreamReader(stream);
            GetPromoCodesDTO[] getPromoCodesDTOs = gson.fromJson(reader, GetPromoCodesDTO[].class);
            if(getPromoCodesDTOs!=null){
                OffersAdapter mAdapter = new OffersAdapter(getPromoCodesDTOs,this);
                LinearLayoutManager mLayoutManager = new LinearLayoutManager(Offers.this);
                offer_view.setLayoutManager(mLayoutManager);
                offer_view.setAdapter(mAdapter);

            }
        }
    }
}

