package com.umrahbus.draweritems;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.umrahbus.Constants;
import com.umrahbus.MainActivity;
import com.umrahbus.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jagadeesh on 6/24/2016.
 */
public class CancelBooking extends AppCompatActivity {

    EditText referenceno, Email;
    TextView Error_Reference_Number, Error_Email, Error_message, Error_Invalid_Reference;
    Button Cancel;
    String Refernce_No, EMAIL;
    String data, refno;
    InputStream inputStream;
    int code;

    TextView Cancel_BUS,Cancel_Flight,Cancel_Hotel;
    String ArrivalTime,DisptTime,Cancelticket;
    int TotalFare;

    protected void onCreate(Bundle savedInstaceState) {
        super.onCreate(savedInstaceState);
        setContentView(R.layout.activity_cancel_booking);

        referenceno = (EditText) findViewById(R.id.Reference_No);
        Email = (EditText) findViewById(R.id.email_cancel);
        Error_Reference_Number = (TextView) findViewById(R.id.error_Reference_no);
        Error_Email = (TextView) findViewById(R.id.error_Email_id);
        Error_message = (TextView) findViewById(R.id.error_cancel_ticket);
        Error_Invalid_Reference = (TextView) findViewById(R.id.Invalid_Reference);
        Cancelticket="BUS";
        /*Cancel_BUS               = (TextView) findViewById(R.id.Cancel_BUS);
        Cancel_Flight            = (TextView)findViewById(R.id.Cancel_Flight);
        Cancel_Hotel             = (TextView)findViewById(R.id.Cancel_Hotel);


        Cancel_BUS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
         *//*   specialfareButton.setVisibility(View.GONE);*//*
                Cancel_BUS.setBackgroundResource(R.drawable.general);
                Cancel_BUS.setTextColor(Color.WHITE);

                Cancel_Flight.setBackgroundResource(R.drawable.buttonborder);
                Cancel_Flight.setText("Flight");
                Cancel_Flight.setTextColor(Color.parseColor("#233E7D"));

                Cancel_Hotel.setBackgroundResource(R.drawable.buttonborder);
                Cancel_Hotel.setText("Hotel");
                Cancel_Hotel.setTextColor(Color.parseColor("#233E7D"));

                Cancelticket="BUS";

            }
        });


        Cancel_Flight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               *//* specialfareButton.setVisibility(View.VISIBLE);*//*
                Cancel_Flight.setBackgroundResource(R.drawable.general);
                Cancel_Flight.setTextColor(Color.WHITE);

                Cancel_BUS.setBackgroundResource(R.drawable.buttonborder);
                Cancel_BUS.setText("Bus");
                Cancel_BUS.setTextColor(Color.parseColor("#233E7D"));

                Cancel_Hotel.setBackgroundResource(R.drawable.buttonborder);
                Cancel_Hotel.setText("Hotel");
                Cancel_Hotel.setTextColor(Color.parseColor("#233E7D"));

                Cancelticket="FLIGHT";

            }
        });
        Cancel_Hotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Cancel_Hotel.setBackgroundResource(R.drawable.general);
                Cancel_Hotel.setTextColor(Color.WHITE);

                Cancel_BUS.setBackgroundResource(R.drawable.buttonborder);
                Cancel_BUS.setText("Bus");
                Cancel_BUS.setTextColor(Color.parseColor("#233E7D"));

                Cancel_Flight.setBackgroundResource(R.drawable.buttonborder);
                Cancel_Flight.setText("Flight");
                Cancel_Flight.setTextColor(Color.parseColor("#233E7D"));

                Cancelticket="HOTEL";
            }
        });*/


        Cancel = (Button) findViewById(R.id.Cancel_Ticket);
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Refernce_No = referenceno.getText().toString();
                EMAIL = Email.getText().toString();

                if (TextUtils.isEmpty(Refernce_No) || Refernce_No.length() > 0 && Refernce_No.startsWith(" ")) {
                    referenceno.setError("Please enter reference number");
                    return;
                }
                if (TextUtils.isEmpty(EMAIL) || EMAIL.length() > 0 && EMAIL.startsWith(" ")) {
                    Email.setError("Please enter email");

                    return;
                }

                if (emailValidator(Email.getText().toString())) {

                } else {
                    Email.setError("Please enter valid email address");
                    return;
                }

                if (Cancelticket.equalsIgnoreCase("BUS")) {


                    final Context context = CancelBooking.this;
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                    // set title
                    alertDialogBuilder.setTitle("Comfirmation");

                    // set dialog message
                    alertDialogBuilder
                            .setMessage("Are you sure you want to cancel this BUS booking ?")
                            .setCancelable(false)
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    new BusTicketCancel().execute();
                                    //  }
                                }
                                //  }
                            });
                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });


                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();

                    // show it
                    alertDialog.show();

                }

            }
            private boolean emailValidator(final String mailAddress) {
                Pattern pattern;
                Matcher matcher;

                final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

                pattern = Pattern.compile(EMAIL_PATTERN);
                matcher = pattern.matcher(mailAddress);
                return matcher.matches();
            }
        });
    }

    public class BusTicketCancel extends AsyncTask<String, String, String> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = ProgressDialog.show(CancelBooking.this, "", "Please Wait  under Process.....");
        }


        @Override
        protected String doInBackground(String... params) {
            String strurl = Constants.BUSAPI + Constants.BookingDetails + "referenceNo=" + Refernce_No + "&type=2";
            DefaultHttpClient httpClient = new DefaultHttpClient();


            HttpGet httpGet = new HttpGet(strurl);
            httpGet.setHeader("ConsumerKey", Constants.ConsumerKey);
            httpGet.setHeader("ConsumerSecret", Constants.ConsumerSecret);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();


            try {

                HttpResponse response = httpClient.execute(httpGet);
                code = response.getStatusLine().getStatusCode();
                data = httpClient.execute(httpGet, responseHandler);

            } catch (IOException e) {
                e.printStackTrace();
            }


            return data;


        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //  JSONObject jsonObject = null;
            try {


                System.out.println("BookingDetails" + data);
                dialog.dismiss();
                if (code == 406) {
                    Error_Email.setVisibility(View.GONE);
                    Error_Reference_Number.setVisibility(View.GONE);
                    Error_Invalid_Reference.setVisibility(View.VISIBLE);
                } else {
                    JSONObject jsonObject = new JSONObject(data);
                    String BookingStatus = jsonObject.getString("BookingStatus");
                    if (BookingStatus.equals("Cancelled")) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CancelBooking.this);

                        // set title
                        //   alertDialogBuilder.setTitle(" ");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Your ticket has been already cancelled.")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        // if this button is clicked, close
                                        // current activity

                                        dialog.cancel();
                                        Intent i = new Intent(CancelBooking.this, MainActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);

                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();

                    } else {
                        String EmailId = jsonObject.getString("EmailId");
                        String ActualFare = jsonObject.getString("ActualFare");
                        String SourceName = jsonObject.optString("SourceName");
                        String DestinationName = jsonObject.optString("DestinationName");
                        String JourneyDate = jsonObject.optString("JourneyDate");
                        String OperatorName = jsonObject.optString("OperatorName");
                        String BusTypeName = jsonObject.optString("BusTypeName");
                        String NoofSeats = jsonObject.optString("NoofSeats");
                        String SeatNos = jsonObject.optString("SeatNos");
                        String CancellationPolicy = jsonObject.optString("CancellationPolicy");
                        String PartailCancellation = jsonObject.optString("PartialCancellationAllowed");
                        String ConvineceFee=jsonObject.getString("ConvenienceFee");
                        String ReturnDate=jsonObject.getString("ReturnDate");

                        if (EMAIL.equals(EmailId)) {

                            Intent i = new Intent(CancelBooking.this, CancelTicketActivity.class);
                            i.putExtra("ActualFare", ActualFare);
                            i.putExtra("SourceName", SourceName);
                            i.putExtra("DestinationName", DestinationName);
                            i.putExtra("JourneyDate", JourneyDate);
                            i.putExtra("OperatorName", OperatorName);
                            i.putExtra("BusTypeName", BusTypeName);
                            i.putExtra("NoofSeats", NoofSeats);
                            i.putExtra("SeatNos", SeatNos);
                            i.putExtra("CancellationPolicy", CancellationPolicy);
                            i.putExtra("Refernce_No", Refernce_No);
                            i.putExtra("EMAIL_ID", EMAIL);
                            i.putExtra("ConvenienceFee",ConvineceFee);
                            i.putExtra("PartialCancellationAllowed", PartailCancellation);
                            i.putExtra("Returndate",ReturnDate);

                            startActivity(i);
                        } else {
                            Error_message.setVisibility(View.VISIBLE);
                            Error_Email.setVisibility(View.GONE);
                            Error_Reference_Number.setVisibility(View.GONE);
                        }


                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
